package cucumbersManagers;

import java.util.concurrent.TimeUnit;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import cucumberEnums.DriverType;
import cucumberEnums.EnvironmentType;
import io.cucumber.core.api.Scenario;

public class WebDriverManager {

	public static WebDriver driver;
	private static DriverType driverType;
	private static EnvironmentType environmentType;
	private static final String CHROME_DRIVER_PROPERTY = "webdriver.chrome.driver";
	private static final String FIREFOX_DRIVER_PROPERTY = "webdriver.gecko.driver";

	public WebDriverManager() {
		driverType = FileReaderManager.getInstance().getConfigReader().getBrowser();
		environmentType = FileReaderManager.getInstance().getConfigReader().getEnvironment();
	}

	public WebDriver getDriverr() {
		return driver;
	}

	public WebDriver getDriver(Scenario scenario) {
		if (driver == null)
			driver = createDriver(scenario);
		return driver;
	}

	private WebDriver createDriver(Scenario scenario) {
		switch (environmentType) {
		case LOCAL:
			driver = createLocalDriver(scenario);
			break;
		case REMOTE:
			driver = createRemoteDriver(scenario);
			break;
		}
		return driver;
	}

	private WebDriver createRemoteDriver(Scenario scenario) {
		throw new RuntimeException("RemoteWebDriver is not yet implemented");
	}

	private WebDriver createLocalDriver(Scenario scenario) {
		switch (driverType) {
		case FIREFOX:
			System.setProperty(FIREFOX_DRIVER_PROPERTY,
					System.getProperty("user.dir") + "\\browserDriver\\geckodriver.exe");

			driver = new FirefoxDriver();
			break;
		case CHROME:
			System.setProperty(CHROME_DRIVER_PROPERTY,
					System.getProperty("user.dir") + "\\browserDriver\\chromedriver.exe");
			/*String downloadFilepath = System.getProperty("user.dir") + "\\output";

			HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
			// chromePrefs.put("profile.default_content_settings.popups", 0);
			chromePrefs.put("download.default_directory", downloadFilepath);
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--headless", "--disable-gpu", "--no-sandbox");
			options.setExperimentalOption("prefs", chromePrefs);
			// options.addArguments("--test-type");
			// options.addArguments("--disable-extensions"); // to disable browser extension
			// popup
			driver = new ChromeDriver(options);*/
			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("useAutomationExtension", false);
			driver = new ChromeDriver(options);
			break;
		case INTERNETEXPLORER:
			driver = new InternetExplorerDriver();
			break;
		}
		if (FileReaderManager.getInstance().getConfigReader().getBrowserWindowSize())
			driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(FileReaderManager.getInstance().getConfigReader().getImplicitlyWait(),
				TimeUnit.SECONDS);
			driver.get(FileReaderManager.getInstance().getConfigReader().getApplicationUrl(scenario));

		return driver;
	}

	public void closeDriver() {
		if (driver != null) {
			driver.quit();
			driver = null;
		}

	}

}
