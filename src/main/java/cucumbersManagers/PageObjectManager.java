package cucumbersManagers;

import org.openqa.selenium.WebDriver;

import pageObjects.MAClaimAdjudicateChildcarePage;
import pageObjects.MAClaimAdjudicateDentalPage;
import pageObjects.MAClaimAdjudicateFamilyPage;
import pageObjects.MAClaimAdjudicateHealthPage;
import pageObjects.MAClaimAdjudicateMedicalPage;
import pageObjects.EESubmitClaimChildCarePage;
import pageObjects.EESubmitClaimDentalExpencesPage;
import pageObjects.EESubmitClaimFamilyCarePage;
import pageObjects.EESubmitClaimHealthScreeningPage;
import pageObjects.EESubmitClaimMedicalExpencesPage;
import pageObjects.EnrolmentCompletePage;
import pageObjects.HRClaimSearchPage;
import pageObjects.HRDependentReportPage;
import pageObjects.HREmployeeReportPage;
import pageObjects.HRFlexReportPage;
import pageObjects.HRPage;
import pageObjects.HRSoeSearchPage;
import pageObjects.ImplementationPage;
import pageObjects.InitializationPage;
import pageObjects.LoginEEPage;
import pageObjects.LoginPage;
import pageObjects.MAAddNewChildcareBatchPage;
import pageObjects.MAAddNewDentalBatchPage;
import pageObjects.MAAddNewFamilyBatchPage;
import pageObjects.MAAddNewHealthBatchPage;
import pageObjects.MAAddNewMedicalBatchPage;
import pageObjects.MABenefitSelectionReportPage;
import pageObjects.MADependentProfileReportPage;
import pageObjects.MAEmployeeProfileReportPage;
import pageObjects.MAFlexPointsAllocationReportPage;
import pageObjects.MAPanelClaimListingReportPage;
import pageObjects.MAPanelClaimUploadPage;
import pageObjects.MASoaPage;
import pageObjects.MAClaimInitializationPage;
import pageObjects.MAClaimPaymentPage;
import pageObjects.MAClaimReviewCommonPage;
import pageObjects.UploadEmpDepDataPage;
import pageObjects.PortalConfigurationsPage;
import pageObjects.HREmployeeSearchPage;
import pageObjects.EnrolmentCompletePage;

public class PageObjectManager {

	private WebDriver driver;

	private LoginPage loginPage;

	private ImplementationPage implementationPage;

	private InitializationPage initializationPage;

	private UploadEmpDepDataPage uploadEmpDepDataPage;

	private MAEmployeeProfileReportPage maReportsPage;

	private MADependentProfileReportPage maDependentReportsPage;

	private MAPanelClaimListingReportPage mAPanelClaimListingReportPage;

	private HRPage hRPage;

	private HREmployeeReportPage hREmployeeReportPage;

	private HRDependentReportPage hRDependentReportPage;

	private HRFlexReportPage hRFlexReportPage;

	private HREmployeeSearchPage hREmployeeSearchPage;

	private HRSoeSearchPage hRSoeSearchPage;

	private HRClaimSearchPage hRClaimSearchPage;

	private EESubmitClaimChildCarePage eeSubmitChildcareServicesClaimPage;

	private EESubmitClaimDentalExpencesPage eESubmitClaimDentalExpencesPage;

	private EESubmitClaimHealthScreeningPage eESubmitClaimHealthScreeningPage;

	private EESubmitClaimMedicalExpencesPage eESubmitClaimMedicalExpencesPage;

	private EESubmitClaimFamilyCarePage eESubmitClaimFamilyCarePage;

	private PortalConfigurationsPage portalConfigurationsPage;

	private LoginEEPage loginEEPage;

	private EnrolmentCompletePage enrolmentCompletePage;

	private MAFlexPointsAllocationReportPage mAFlexPointsAllocationReportPage;

	private MABenefitSelectionReportPage mABenefitSelectionReportPage;

	private MAClaimInitializationPage mAClaimInitializationPage;

	private MAAddNewChildcareBatchPage mAAddNewChildcareBatchPage;

	private MAAddNewDentalBatchPage mAAddNewDentalBatchPage;

	private MAAddNewHealthBatchPage mAAddNewHealthBatchPage;

	private MAAddNewMedicalBatchPage mAAddNewMedicalBatchPage;

	private MAAddNewFamilyBatchPage mAAddNewFamilyBatchPage;

	private MAClaimAdjudicateChildcarePage mAClaimAdjudicateChildcarePage;

	private MAClaimAdjudicateDentalPage mAClaimAdjudicateDentalPage;

	private MAClaimAdjudicateHealthPage mAClaimAdjudicateHealthPage;
	
	private MAClaimAdjudicateMedicalPage mAClaimAdjudicateMedicalPage;

	private MAClaimAdjudicateFamilyPage mAClaimAdjudicateFamilyPage;
	
    private MAClaimReviewCommonPage mAClaimReviewCommonPage; 
	
	private MAClaimPaymentPage mAClaimPaymentPage;
	
	private MAPanelClaimUploadPage mAPanelClaimUploadPage;
	
	private MASoaPage mASoaPage;

	// Constructor
	public PageObjectManager(WebDriver driver) {

		this.driver = driver;

	}

	public UploadEmpDepDataPage getUploadEmpDepDataPage() {

		return (uploadEmpDepDataPage == null) ? uploadEmpDepDataPage = new UploadEmpDepDataPage(driver)
				: uploadEmpDepDataPage;

	}

	public LoginPage getLoginPage() {

		return (loginPage == null) ? loginPage = new LoginPage(driver) : loginPage;

	}

	public ImplementationPage getImplementationPage() {

		return (implementationPage == null) ? implementationPage = new ImplementationPage(driver) : implementationPage;

	}

	public InitializationPage getInitializationPage() {

		return (initializationPage == null) ? initializationPage = new InitializationPage(driver) : initializationPage;

	}

	public HRPage getHrPage() {

		return (hRPage == null) ? hRPage = new HRPage(driver) : hRPage;

	}

	public HREmployeeReportPage getHrEmployeeReportPage() {

		return (hREmployeeReportPage == null) ? hREmployeeReportPage = new HREmployeeReportPage(driver)
				: hREmployeeReportPage;

	}

	public HRDependentReportPage getHrDependentReportPage() {

		return (hRDependentReportPage == null) ? hRDependentReportPage = new HRDependentReportPage(driver)
				: hRDependentReportPage;

	}

	public HRFlexReportPage getHrFlexReportPage() {

		return (hRFlexReportPage == null) ? hRFlexReportPage = new HRFlexReportPage(driver) : hRFlexReportPage;

	}

	public HREmployeeSearchPage getHrEmployeeSearchPage() {

		return (hREmployeeSearchPage == null) ? hREmployeeSearchPage = new HREmployeeSearchPage(driver)
				: hREmployeeSearchPage;

	}

	public HRSoeSearchPage getHrSoeSearchPage() {

		return (hRSoeSearchPage == null) ? hRSoeSearchPage = new HRSoeSearchPage(driver) : hRSoeSearchPage;

	}

	public HRClaimSearchPage getHrClaimSearchPage() {

		return (hRClaimSearchPage == null) ? hRClaimSearchPage = new HRClaimSearchPage(driver) : hRClaimSearchPage;

	}

	public MAEmployeeProfileReportPage getMaReportsPage() {

		return (maReportsPage == null) ? maReportsPage = new MAEmployeeProfileReportPage(driver) : maReportsPage;

	}

	public MADependentProfileReportPage getMaDependentReportsPage() {

		return (maDependentReportsPage == null) ? maDependentReportsPage = new MADependentProfileReportPage(driver)
				: maDependentReportsPage;

	}

	public MAPanelClaimListingReportPage getMaPanelClaimListingReportPage() {

		return (mAPanelClaimListingReportPage == null)
				? mAPanelClaimListingReportPage = new MAPanelClaimListingReportPage(driver)
				: mAPanelClaimListingReportPage;

	}

	public EESubmitClaimChildCarePage getEeSubmitClaimCommonPage() {

		return (eeSubmitChildcareServicesClaimPage == null)
				? eeSubmitChildcareServicesClaimPage = new EESubmitClaimChildCarePage(driver)
				: eeSubmitChildcareServicesClaimPage;
	}

	public EESubmitClaimDentalExpencesPage getEeSubmitClaimDentalExpencesPage() {

		return (eESubmitClaimDentalExpencesPage == null)
				? eESubmitClaimDentalExpencesPage = new EESubmitClaimDentalExpencesPage(driver)
				: eESubmitClaimDentalExpencesPage;
	}

	public EESubmitClaimHealthScreeningPage getEeSubmitClaimHealthScreeningPage() {

		return (eESubmitClaimHealthScreeningPage == null)
				? eESubmitClaimHealthScreeningPage = new EESubmitClaimHealthScreeningPage(driver)
				: eESubmitClaimHealthScreeningPage;
	}

	public EESubmitClaimMedicalExpencesPage getEeSubmitClaimMedicalExpencesPage() {

		return (eESubmitClaimMedicalExpencesPage == null)
				? eESubmitClaimMedicalExpencesPage = new EESubmitClaimMedicalExpencesPage(driver)
				: eESubmitClaimMedicalExpencesPage;
	}

	public EESubmitClaimFamilyCarePage getEeSubmitClaimFamilyCarePage() {

		return (eESubmitClaimFamilyCarePage == null)
				? eESubmitClaimFamilyCarePage = new EESubmitClaimFamilyCarePage(driver)
				: eESubmitClaimFamilyCarePage;
	}

	public PortalConfigurationsPage getPortalConfigurationsPage() {

		return (portalConfigurationsPage == null) ? portalConfigurationsPage = new PortalConfigurationsPage(driver)
				: portalConfigurationsPage;
	}

	public LoginEEPage getLoginEePage() {
		return (loginEEPage == null) ? loginEEPage = new LoginEEPage(driver) : loginEEPage;
	}

	public EnrolmentCompletePage getEnrolmentCompletePage() {
		return (enrolmentCompletePage == null) ? enrolmentCompletePage = new EnrolmentCompletePage(driver)
				: enrolmentCompletePage;
	}

	public MAFlexPointsAllocationReportPage getMaFlexPointsAllocationReportPage() {

		return (mAFlexPointsAllocationReportPage == null)
				? mAFlexPointsAllocationReportPage = new MAFlexPointsAllocationReportPage(driver)
				: mAFlexPointsAllocationReportPage;

	}

	public MABenefitSelectionReportPage getMaBenefitSelectionReportPage() {

		return (mABenefitSelectionReportPage == null)
				? mABenefitSelectionReportPage = new MABenefitSelectionReportPage(driver)
				: mABenefitSelectionReportPage;

	}

	public MAClaimInitializationPage getMaClaimInitializationPage() {

		return (mAClaimInitializationPage == null) ? mAClaimInitializationPage = new MAClaimInitializationPage(driver)
				: mAClaimInitializationPage;

	}

	public MAAddNewChildcareBatchPage getMaAddNewCommonBatchPage() {

		return (mAAddNewChildcareBatchPage == null)
				? mAAddNewChildcareBatchPage = new MAAddNewChildcareBatchPage(driver)
				: mAAddNewChildcareBatchPage;

	}

	public MAAddNewDentalBatchPage getMaAddNewDentalBatchPage() {

		return (mAAddNewDentalBatchPage == null) ? mAAddNewDentalBatchPage = new MAAddNewDentalBatchPage(driver)
				: mAAddNewDentalBatchPage;

	}

	public MAAddNewHealthBatchPage getMaAddNewHealthBatchPage() {

		return (mAAddNewHealthBatchPage == null) ? mAAddNewHealthBatchPage = new MAAddNewHealthBatchPage(driver)
				: mAAddNewHealthBatchPage;

	}

	public MAAddNewMedicalBatchPage getMaAddNewMedicalBatchPage() {

		return (mAAddNewMedicalBatchPage == null) ? mAAddNewMedicalBatchPage = new MAAddNewMedicalBatchPage(driver)
				: mAAddNewMedicalBatchPage;

	}

	public MAAddNewFamilyBatchPage getMaAddNewFamilyBatchPage() {

		return (mAAddNewFamilyBatchPage == null) ? mAAddNewFamilyBatchPage = new MAAddNewFamilyBatchPage(driver)
				: mAAddNewFamilyBatchPage;

	}

	public MAClaimAdjudicateChildcarePage getEEClaimAdjudicatePage() {

		return (mAClaimAdjudicateChildcarePage == null)
				? mAClaimAdjudicateChildcarePage = new MAClaimAdjudicateChildcarePage(driver)
				: mAClaimAdjudicateChildcarePage;

	}

	public MAClaimAdjudicateDentalPage getEEClaimAdjudicateDentalPage() {

		return (mAClaimAdjudicateDentalPage == null)
				? mAClaimAdjudicateDentalPage = new MAClaimAdjudicateDentalPage(driver)
				: mAClaimAdjudicateDentalPage;

	}

	public MAClaimAdjudicateHealthPage getEEClaimAdjudicateHealthPage() {

		return (mAClaimAdjudicateHealthPage == null)
				? mAClaimAdjudicateHealthPage = new MAClaimAdjudicateHealthPage(driver)
				: mAClaimAdjudicateHealthPage;

	}
	
	
	public MAClaimAdjudicateMedicalPage getEEClaimAdjudicateMedicalPage() {

		return (mAClaimAdjudicateMedicalPage == null)
				? mAClaimAdjudicateMedicalPage = new MAClaimAdjudicateMedicalPage(driver)
				: mAClaimAdjudicateMedicalPage;

	}
	
	
	public MAClaimAdjudicateFamilyPage getEEClaimAdjudicateFamilyPage() {

		return (mAClaimAdjudicateFamilyPage == null)
				? mAClaimAdjudicateFamilyPage = new MAClaimAdjudicateFamilyPage(driver)
				: mAClaimAdjudicateFamilyPage;

	}
	
    public MAClaimReviewCommonPage getMAClaimReviewCommonPage() {
        return (mAClaimReviewCommonPage == null) ? mAClaimReviewCommonPage = new MAClaimReviewCommonPage(driver) : mAClaimReviewCommonPage;
 }

	
	public MAClaimPaymentPage getMAClaimPaymentPage() {
        return (mAClaimPaymentPage == null) ? mAClaimPaymentPage = new MAClaimPaymentPage(driver) : mAClaimPaymentPage;
 }
	
	public MAPanelClaimUploadPage getMAPanelClaimUploadPage() {
        return (mAPanelClaimUploadPage == null) ? mAPanelClaimUploadPage = new MAPanelClaimUploadPage(driver) : mAPanelClaimUploadPage;
 }

	public MASoaPage getMASoaPage() {
        return (mASoaPage == null) ? mASoaPage = new MASoaPage(driver) : mASoaPage;
 }

}
