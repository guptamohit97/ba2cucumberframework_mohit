package pageObjects;



import java.io.IOException;
import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

//import cucumberDataProviders.ConfigFileReader;
import cucumberUtilities.Xls_Reader_Fillo;

import cucumberUtilities.Base;
//import io.cucumber.core.api.Scenario;
import cucumberUtilities.ExcelData;


public class PortalConfigurationsPage extends Base {

	WebDriver driver;
	Xls_Reader_Fillo filloReader = null;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'SecurityAccess')]")
	private WebElement SecurityAccess;

	@FindBy(how = How.XPATH, using = "//a[contains(@id, '_AdminMain_ucNavigation_lbtnPortalConfig')]")
	private WebElement portalConfigurationLink;

	@FindBy(how = How.XPATH, using = "//select[contains(@id, '_PortalConfigurations_ddlPortal2')]")
	private WebElement portalSelectionDropDown;

	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_PortalConfigurations_txtClientUserName')]")
	private WebElement clientLoginUserNameTextBox;

	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_PortalConfigurations_btnUserInfo')]")
	private WebElement getUserInfoButton;

	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_PortalConfigurations_txtClientPassword')]")
	private WebElement changePasswordTextBox;

	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_PortalConfigurations_btnResetPassword')]")
	private WebElement resetPasswordButton;


	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_LoginDefault_NewLoginDefault1_SignIn_v2_txtUsername')]")
	private WebElement userNameTextBox;

	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_LoginDefault_NewLoginDefault1_SignIn_v2_txtPassword')]")
	private WebElement passwordTextBox;

	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_LoginDefault_NewLoginDefault1_SignIn_v2_cmdLogin')]")
	private WebElement loginButton;

	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_LoginDefault_NewLoginDefault1_SignIn_v2_ctlTermsAndAgreements_btnAgreeNew')]")
	private WebElement iAgreeButton;

	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_LoginDefault_NewLoginDefault1_SignIn_v2_Password_txtOldPassword')]")
	private WebElement currentPasswordTextBox;

	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_LoginDefault_NewLoginDefault1_SignIn_v2_Password_txtNewPassword')]")
	private WebElement newPasswordTextBox;

	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_LoginDefault_NewLoginDefault1_SignIn_v2_Password_txtNewConfirm')]")
	private WebElement confirmPasswordTextBox;

	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_LoginDefault_NewLoginDefault1_SignIn_v2_Password_cmdUpdate')]")
	private WebElement updatePasswordButton;
	
	@FindBy(how = How.XPATH, using = "//a[@id='dnn_dnnLOGIN_cmdLogin']")
	private WebElement updatePasswordLogoutButton;
	
	//a[@id='dnn_dnnLOGIN_cmdLogin']

	@FindBy(how = How.XPATH, using = "//a[contains(@id,'LOGIN_cmdLogin')]")
	private WebElement logoutLink;

	// Constructor
	public PortalConfigurationsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void clickSecurityAccess() {
		clickJS(SecurityAccess);

	}

	public void clickPortalConfigurationLink() {
		clickJS(portalConfigurationLink);

	}

	public void selectPortalSelectionDropDown() {
		executeInputOperation(portalSelectionDropDown, "DSOSGAutomation");
	}

	public void EnterClientLoginUserNameTextBox(String testValue) throws IOException, FilloException {
		//enterText(clientLoginUserNameTextBox, "BA2_DSO_050_20");
		
		//filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");
		//enterText(clientLoginUserNameTextBox,filloReader.getCellData("Select * from ChangePassword", "EmployeeID"));
		
		
		//ExcelData d = new ExcelData("C:\\Users\\mohit-gupta\\Desktop\\NewTestData.xlsx");

		//ArrayList<String> data = d.getData("ChangePassword", "Testcase" , ""+ testValue);
		
		/*for(int i=1; i<data.size(); i++) {
			enterText(clientLoginUserNameTextBox, data.get(i).get("Testcase"));*/
			
		String data = getData(testValue,"ChangePassword","Testcase","EmployeeID");
		enterText(clientLoginUserNameTextBox, data);
		
		
		
		//enterText(clinicProviderName, data.get(2));
		
		/*filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");
		enterText(clientLoginUserNameTextBox,
				filloReader.getCellData("Select * from ChangePassword", "EmployeeID"));*/

	}
	
	/*public static String getData(String testvalue,String sheetName,String sourceField,String DestField) throws FilloException
	{
		String data;
		Fillo fillo = new Fillo();
		Connection conn = fillo.getConnection(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");
		String str = "select * from "+sheetName;
		Recordset record = conn.executeQuery(str);
		
		while(record.next())
		{
			if(testvalue.equalsIgnoreCase(record.getField(sourceField)))
			{
				data = record.getField(DestField);
				System.out.println(data);
				return data;
			}
		}
		return "Data not found";
	}*/

	public void clickGetUserInfoButton() {
		clickButton(getUserInfoButton);

	}

	public void EnterChangePasswordTextBox() {
		enterText(changePasswordTextBox, "1");

	}

	public void clickResetPasswordButton() {
		clickButton(resetPasswordButton);
		
		

	}

	public void enterUserNameTextBox(String testValue) throws FilloException {
		//enterText(userNameTextBox, "BA2_DSO_050_20");
		
		String data = getData(testValue,"ChangePassword","Testcase","EmployeeID");
		enterText(userNameTextBox, data);
		
	/*	filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");
		enterText(userNameTextBox,
				filloReader.getCellData("Select * from ChangePassword", "EmployeeID"));*/
	}

	public void enterPasswordTextBox() {
		executeInputOperation(passwordTextBox, "1");

	}

	public void clickLoginButton() {
		clickButton(loginButton);

	}

	public void clickIAgreeButton() {
		clickButton(iAgreeButton);

	}

	public void enterCurrentPasswordTextBox() {
		enterText(currentPasswordTextBox, "1");

	}

	public void enterNewPasswordTextBox() {
		enterText(newPasswordTextBox, "Password123$");

	}

	public void enterConfirmPasswordTextBox() {
		enterText(confirmPasswordTextBox, "Password123$");

	}

	public void clickUpdatePasswordButton() {
		clickButton(updatePasswordButton);

	}
	
	public void clickUpdatePasswordLogoutButton() {
		clickButton(updatePasswordLogoutButton);

	}

	public void clickLogoutLink() {
		clickJS(logoutLink);

	}

}
