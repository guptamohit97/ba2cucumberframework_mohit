package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.codoid.products.exception.FilloException;

import cucumberUtilities.Base;
import cucumberUtilities.Xls_Reader_Fillo;
import cucumberUtilities.Base;

public class MAClaimInitializationPage extends Base {

	public static Logger log = LogManager.getLogger(UploadEmpDepDataPage.class.getName());
	Xls_Reader_Fillo filloReader = null;


	@FindBy(how = How.XPATH, using = "//body[@id='Body']/form[@id='Form']/div[@class='genericContainer']/table[@class='genericContainer']/tbody/tr/td[@class='containerframe']/table[@class='genericContainer']/tbody/tr/td[@id='lcp']/div[@class='genericContainer']/div[@id='BA_Menu']/ul/li[18]/a[1]")
	private WebElement claimLink;

	@FindBy(how = How.XPATH, using = "//a[contains(@id, '_ClaimsAdminMenu_rptMenu_ctl27_LinkButton1')]")
	private WebElement massClaimInitializationLink;

	@FindBy(how = How.XPATH, using = "//select[contains(@id, '_ClaimInitialization_ddlPlans')]")
	private WebElement chooseClaimPlanDropDown;

	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimInitialization_txtHREmployeeId')]")
	private WebElement searchByEmployeeIdTextBox;
	
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimInitialization_btnSubmit')]")
	private WebElement searchByEmployeeIdButton;
	

	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimInitialization_btnInitialization')]")
	private WebElement claimInitializtionSubmitButton;

	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimInitialization_ucScheduleJob_btnOk')]")
	private WebElement claimInitializtionOkButton;
	
	@FindBy(how = How.XPATH, using = "//span[contains(@id, 'Summary')]")
	private WebElement claimMessageText;
	
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimInitialization_BA2JobProgress1_btnClose')]")
	private WebElement claimInitializtionCloseButton;
	
	
	// Constructor
	public MAClaimInitializationPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void clickClaimLink() {
		clickJS(claimLink);
	}
	
	public void clickMassClaimInitializationLink() {
		clickJS(massClaimInitializationLink);
	}
	
	public void selectChooseClaimPlanDropDown() {
		SelectFromListBox(chooseClaimPlanDropDown, "P00012");
	
	}
	
	public void enterSearchByEmployeeIdTextBox(String testValue) throws FilloException {
		
		String data = getData(testValue,"claimInitializationData","Testcase","EmployeeID");
		enterText(searchByEmployeeIdTextBox, data);
	
		/*filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");
		enterText(searchByEmployeeIdTextBox, filloReader.getCellData("Select * from claimInitializationData", "Employee Claim Id"));*/
	}
	
	public void clickSearchByEmployeeIdButton() {
		clickButton(searchByEmployeeIdButton);
	}
	
	public void clickClaimInitializtionSubmitButton() {
		clickButton(claimInitializtionSubmitButton);
	}
	
	public void clickClaimInitializtionOkButton() {
		clickButton(claimInitializtionOkButton);
	}
	
	public void verifyClaimMessageText() {
	    validateTextfieldvalue(claimMessageText, "1 of 1 Claim Initialization successfully.");
	
	}
	
	public void clickClaimInitializtionCloseButton() {
		clickButton(claimInitializtionCloseButton);
	}
}
