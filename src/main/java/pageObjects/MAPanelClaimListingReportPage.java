package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumberUtilities.Base;

public class MAPanelClaimListingReportPage extends Base {

	@FindBy(how = How.XPATH, using = "//*[@id='BA_Menu']//a[contains(text(),'Reports Version2')]")
	private WebElement ReportStatTab;

	@FindBy(how = How.LINK_TEXT, using = "Panel Claim Listing Report")
	private WebElement ClickPanelClaimReport;

	@FindBy(how = How.XPATH, using = "//select[contains(@id,'_RptReportLoader_lstStatus')]//option[contains(text(),'Pending Payment')]")
	private WebElement Status;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_RptReportLoader_btnSearch')]")
	private WebElement Generate;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Panel Claim Listing Report')]")
	private WebElement PanelClaimListingReport;

	// Constructor
	public MAPanelClaimListingReportPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void clickPanelClaimListingReport() {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(ReportStatTab));
		clickActionClass(ReportStatTab, ClickPanelClaimReport);
	}

	public void clickGenerateButton() {
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.elementToBeClickable(Generate));
		clickButton(Generate);
	}

	public void validatePanelClaimListingReportName() {

		validateTextfieldvalue(PanelClaimListingReport, "Panel Claim Listing Report");
	}
}
