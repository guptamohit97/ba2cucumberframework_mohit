package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumberUtilities.Base;

public class ImplementationPage extends Base {

	// MA - Home Page - Click Implementation link
	@FindBy(how = How.XPATH, using = "//*[@id='BA_Menu']//a[contains(text(),'Implementation')]")
	private WebElement Implementation;

	// MA - Home Page - Click Upload employee data link
	@FindBy(how = How.LINK_TEXT, using = "Upload Employee Data (New)")
	private WebElement UploadEmployeeData;

	// MA - Home Page - Click Upload Dependent data link
	@FindBy(how = How.LINK_TEXT, using = "Upload Dependent Data (New)")
	private WebElement UploadDepData;

	// MA - Home Page - Click Initialization link
	@FindBy(how = How.LINK_TEXT, using = "Initialization (New)")
	private WebElement Initialization;

	// Constructor
	public ImplementationPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	// MA - Home Page - Method to click Upload employee data link
	public void clickUploadEmployeeDataButton() {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(Implementation));
		clickActionClass(Implementation, UploadEmployeeData);
	}

	// MA - Home Page - Method to click Initialization link
	public void clickInitializationButton() {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(Implementation));
		clickActionClass(Implementation, Initialization);
	}

	// MA - Home Page - Method to click Upload dependent data link
	public void clickUploadDepDataButton() {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(Implementation));
		clickActionClass(Implementation, UploadDepData);
	}

}
