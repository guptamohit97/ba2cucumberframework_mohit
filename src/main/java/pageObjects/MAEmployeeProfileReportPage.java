package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumberUtilities.Base;

public class MAEmployeeProfileReportPage extends Base {

	// Employee Profile report

	@FindBy(how = How.XPATH, using = "//*[@id='BA_Menu']//a[contains(text(),'Report and Statistics')]")
	private WebElement ReportStatTab;

	@FindBy(how = How.LINK_TEXT, using = "Employee Profile Report")
	private WebElement ClickEmployeeReport;

	@FindBy(how = How.XPATH, using = "//select[contains(@id,'_RptEmployeeProfile_MY_lstddlStatus')]")
	private WebElement EmployeeStatus;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_RptEmployeeProfile_MY_btnShowReport')]")
	private WebElement EmployeeSearch;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Employee Profile Report')]")
	private WebElement EmployeeProfileReport;

	// Constructor
	public MAEmployeeProfileReportPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	// Employee Profile report

	public void clickEmployeeProfileReport() {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(ReportStatTab));
		clickActionClass(ReportStatTab, ClickEmployeeReport);

	}

	public void clickEmployeeSearchButton() {
		WebDriverWait wait = new WebDriverWait(driver, 150);
		wait.until(ExpectedConditions.elementToBeClickable(EmployeeSearch));
		clickButton(EmployeeSearch);
	}

	public void validateEmployeeProfileReportName() {
		WebDriverWait wait = new WebDriverWait(driver, 300);
		// wait.until(ExpectedConditions.elementToBeClickable(EmployeeProfileReport));
		validateTextfieldvalue(EmployeeProfileReport, "Employee Profile Report");
	}

}
