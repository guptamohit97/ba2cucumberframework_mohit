package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.codoid.products.exception.FilloException;

import cucumberUtilities.Base;
import cucumberUtilities.Xls_Reader_Fillo;
import cucumberUtilities.Base;

public class MAClaimAdjudicateDentalPage extends Base{

	public static Logger log = LogManager.getLogger(UploadEmpDepDataPage.class.getName());
	Xls_Reader_Fillo filloReader = null;

	//MA-Add to batch-Claim link
	@FindBy(how = How.XPATH, using = "//a[contains(@id, '_ClaimsAdminMenu_rptMenu_ctl17_LinkButton1')]")
	public WebElement searchAClaimLink;

	//MA-Add
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_SearchAClaim_txtInputSearch')]")
	public WebElement searchAClaimTextBox;

	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_SearchAClaim_btnSearch')]")
	public WebElement searchAClaimButton;
		
    //MA-Search claim-Adjudicate button
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_btnAdjudicate')]")
	public WebElement claimAdjudicateButton;

	//MA-Adjudicate-select status dropdown
	@FindBy(how = How.XPATH, using = "//select[contains(@id, '_cbStatus')]")
	public WebElement claimStatusSelectionDropdown;

	//MA-Adjudicate-Process button
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsBatchMassApprovalException_btnProcess')]")
	public WebElement claimAdjudicateProcessButton;

	@FindBy(how = How.XPATH, using = "//span[contains(@id, '_ClaimsBatchMassApprovalException_lblMessage') and contains(text(),'successfully.')]")
	public WebElement claimAdjudicateSuccessfullyMessage;
	
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsBatchMassApprovalException_btnPrint')]//following::input[@type='button']")
	public WebElement claimCloseButton;
	
	// Constructor
	public MAClaimAdjudicateDentalPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	 //MA - Search claim page - method to click search claim link
	public void clickSearchAClaimLink() {
		clickJS(searchAClaimLink);
	}
	
	//MA - Search claim page - method to enter text in search claim link
	public void enterSearchAClaimTextBox(String testValue) throws FilloException {
		
		String data = getData(testValue,"DentalExpencesClaim","Testcase","Claimant Name");
		enterText(searchAClaimTextBox, data);
		
		/*filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");
		enterText(searchAClaimTextBox, filloReader.getCellData("Select * from claimAdjudicate", "Employee"));*/
	}
	
	//MA - Search claim page - method to click search claim button
	public void clickSearchAClaimButton() {
		clickButton(searchAClaimButton);
	}
	
    //MA - Search claim page - method to click Adjudicate button
	public void clickClaimAdjudicateButton() {
		clickButton(claimAdjudicateButton);
	}
	
	//MA - Adjudicate page - method to select text in claim status dropdown
	public void selectClaimStatusSelectionDropdown(String testValue) throws FilloException {
		
		String data = getData(testValue,"DentalExpencesClaim","Testcase","Claim Status");
		SelectFromListBox(claimStatusSelectionDropdown, data);
		
		/*filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");
		SelectFromListBox(claimStatusSelectionDropdown, filloReader.getCellData("Select * from claimAdjudicate", "claim status"));*/
	}
	
	//MA - Search claim page - method to click process button
	public void clickClaimAdjudicateProcessButton() {
		clickButton(claimAdjudicateProcessButton);
	}
	
	//MA - Search claim page - method to verify claim process message
	public void verifyClaimAdjudicateSuccessfullyMessage() {
	    elementIsPresent(claimAdjudicateSuccessfullyMessage);
	}
	
	//MA - Search claim page - method to click close button
		public void clickClaimCloseButton() {
			clickButton(claimCloseButton);
		}
	
}
