package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumberUtilities.Base;
import cucumberUtilities.Xls_Reader_Fillo;

public class LoginEEPage extends Base {
	
	public static Logger log = LogManager.getLogger(LoginPage.class.getName());

	WebDriver driver;

	Xls_Reader_Fillo filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");

	    @FindBy(how = How.XPATH, using = "//input[contains(@id, '_LoginDefault_NewLoginDefault1_SignIn_v2_txtUsername')]")
	   	public WebElement userNameTextBox;
	    
	    @FindBy(how = How.XPATH, using = "//input[contains(@id, '_LoginDefault_NewLoginDefault1_SignIn_v2_txtPassword')]")
	   	public WebElement passwordTextBox;
	    
	    @FindBy(how = How.XPATH, using = "//input[contains(@id, '_LoginDefault_NewLoginDefault1_SignIn_v2_cmdLogin')]")
	   	public WebElement loginButton;
	    
	// Constructor
		public LoginEEPage(WebDriver driver) {
			this.driver = driver;
			PageFactory.initElements(driver, this);
		}
		public void enterUserNameTextBox(String testValue) {
			//enterText(userNameTextBox, "BA2_DSO_050_19");
			filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");
			String data1= filloReader.getCellData("Select * from LoginEESite", testValue);
			enterText(userNameTextBox, data1);
			
			
		}

		public void enterPasswordTextBox() {
			executeInputOperation(passwordTextBox, "Password123$");
			
		}

		public void clickLoginButton() {
			clickButton(loginButton);
			
		}
}