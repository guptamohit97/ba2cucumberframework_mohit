package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.WebDriverWait;

import cucumberUtilities.Base;
//import cucumberUtilities.Xls_Reader_Fillo;
import cucumberUtilities.Xls_Reader_Fillo;

public class HRSoeSearchPage extends Base {

	public static Logger log = LogManager.getLogger(LoginPage.class.getName());
	
	Xls_Reader_Fillo filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");

	WebDriver driver;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Start Enrollment')]")
	private WebElement hrEnrolmentTab;

	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ViewEmployeeEnrolmentStatement_SearchEnrolmentStatus_v2_txtSearch')]")
	private WebElement hrEmployeeEnrolmentTextBox;

	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ViewEmployeeEnrolmentStatement_SearchEnrolmentStatus_v2_btnSearch')]")
	private WebElement hrEnrolmentSearchButton;

	@FindBy(how = How.XPATH, using = "//h2[contains(text(),'Employees')]")
	private WebElement hrEnrolmentHeader;

	@FindBy(how = How.XPATH, using = "//*[contains(@id,'_ViewEmployeeEnrolmentStatement_SearchEnrolmentStatus_v2_GrdEmployee_ctl02_HyperLinkEnrolmentID')]")
	private WebElement hrStatementOfEnrolmentLink;

	@FindBy(how = How.XPATH, using = "//*[contains(@id, '_EnrolmentStatement_EnrolmentStatementV2_lblTitle')]")
	private WebElement hrStatementOfEnrolmentHeader;

	// Constructor
	public HRSoeSearchPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void clickHrEnrolmentTab() {
		clickButton(hrEnrolmentTab);
	}

	public void clickHrEmployeeEnrolmentTextBox() {
		//enterText(hrEmployeeEnrolmentTextBox, "BA2_DSO_050_20");
		filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");
		enterText(hrEmployeeEnrolmentTextBox,
				filloReader.getCellData("Select * from HRpage", "Search Employee"));
	}

	public void clickHrEnrolmentSearchButton() {
		clickButton(hrEnrolmentSearchButton);
	}

	public void verifyHrEnrolmentHeader() {
		validateTextfieldvalue(hrEnrolmentHeader, "Employees");

	}

	public void clickHrStatementOfEnrolmentLink() {
		clickJS(hrStatementOfEnrolmentLink);
	}

	public void verifyHrStatementOfEnrolmentHeader() {
		validateTextfieldvalue(hrStatementOfEnrolmentHeader, "Enrollment Statement");

	}

}