package pageObjects;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.codoid.products.exception.FilloException;

import cucumberUtilities.Base;
import cucumberUtilities.Xls_Reader_Fillo;

public class EnrolmentCompletePage extends Base {

	public static Logger log = LogManager.getLogger(LoginPage.class.getName());

	Xls_Reader_Fillo filloReader = null;

	// Close Enrolment
	// MA - Enrollment Page - Click enrollment menu
	@FindBy(how = How.XPATH, using = "//*[@id='BA_Menu']//a[(text()='Enrollment')]")
	private WebElement clickEnrolmentMenu;

	// MA - Enrollment Page - Click view enrollment menu
	@FindBy(how = How.XPATH, using = "//a[(text()='View Enrollment Status')]")
	private WebElement clickViewEnrolmentMenu;

	// MA - Enrollment Page - select radio button
	@FindBy(how = How.XPATH, using = "//input[contains(@id,\"_rdbsearch_3\")]")
	private WebElement radio_EmployeeID;

	// MA - Enrollment Page - enter search field
	@FindBy(how = How.XPATH, using = "//input[contains(@id,\"_txtSearch\")]")
	private WebElement search_Field;

	// MA - Enrollment Page - Click search button
	@FindBy(how = How.XPATH, using = "//input[contains(@id,\"_btnSearch\")]")
	private WebElement search_Button;

	// MA - Enrollment Page - Click edit period
	@FindBy(how = How.XPATH, using = "//input[contains(@id,\"_btnEditPeriod\")]")
	private WebElement edit_Period;

	// MA - Enrollment Page - enter enrollment start date
	@FindBy(how = How.XPATH, using = "//input[contains(@id,\"_EditEnrolmentPeriod_tbEnrolmentStart\")]")
	private WebElement enrollmentStartDate;

	// MA - Enrollment Page - enter enrollment end date
	@FindBy(how = How.XPATH, using = "//input[contains(@id,\"_EditEnrolmentPeriod_tbEnrolmentEnd\")]")
	private WebElement enrollmentEndDate;

	// MA - Enrollment Page - enter enrollment close date
	@FindBy(how = How.XPATH, using = "//input[contains(@id,\"_EditEnrolmentPeriod_tbCloseDate\")]")
	private WebElement closeDate;

	// MA - Enrollment Page - enter enrollment complete date
	@FindBy(how = How.XPATH, using = "//input[contains(@id,\"_EditEnrolmentPeriod_tbCompleteBy\")]")
	private WebElement completeDate;

	// MA - Enrollment Page - click submit button
	@FindBy(how = How.XPATH, using = "//input[contains(@id,\"_btnSubmit\")]")
	private WebElement submit_Button;

	// Complete Enrollment

	// MA - Enrollment Page - click complete enrollment
	@FindBy(how = How.XPATH, using = "//a[(text()='CompleteEnrollment(New)')]")
	private WebElement clickCompleteEnrollment;

	// MA - Enrollment Page - click employeeID radio button
	@FindBy(how = How.XPATH, using = "//input[contains(@id,\"_CompleteEnrolmentNew_SearchEnrolmentStatus_rdbsearch_3\")]")
	private WebElement radio_EmployeeIDComplete;

	// MA - Enrollment Page - click complete enrollment link
	@FindBy(how = How.XPATH, using = "//a[contains(@id,\"_CompleteEnrolmentNew_SearchEnrolmentStatus_GrdEmployee_ctl02_lbCompleteEnrolment\")]")
	private WebElement completeEnrollmentLink;

	// MA - Enrollment Page - enter employeeID textbox
	@FindBy(how = How.XPATH, using = "//input[contains(@id,\"_txtSearch\")]")
	private WebElement employeeIDTextbox;

	// MA - Enrollment Page - click batch complete all
	@FindBy(how = How.XPATH, using = "//input[contains(@id,\"_BatchCompleteAll\")]")
	private WebElement batchCompleteAll;

	// MA - Enrollment Page - click OK button
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_CompleteEnrolmentNew_ucScheduleJob_btnOk')]")
	private WebElement ScheduleJob_btnOk;

	// MA - Enrollment Page - click close button
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_CompleteEnrolmentNew_BA2JobProgress1_btnClose')]")
	private WebElement jobProgress1_btnClose;

	// Constructor
	public EnrolmentCompletePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	// Functions close
	// MA - Enrollment Page - method to click view enrollment status
	public void clickViewEnrollmentStatusButton() {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(clickEnrolmentMenu));
		clickActionClass(clickEnrolmentMenu, clickViewEnrolmentMenu);
	}

	// MA - Enrollment Page - method to select employeeID radio button
	public void selectEmployeeIDRadioClose() {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(radio_EmployeeID));
		RadioSelector(radio_EmployeeID, "");

	}

	// MA - Enrollment Page - method to enter employeeID
	public void enterEmployeeIDTextbox(String testValue) throws FilloException {

		String data = getData(testValue,"enrolmentDates","Testcase","EmployeeID");
		enterText(search_Field, data);
		
	/*	filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");
		enterText(search_Field, filloReader.getCellData("Select * from enrolmentDates", "EmployeeID"));
*/
	}

	// MA - Enrollment Page - method to click search button
	public void clickSearchButton() {
		clickButton(search_Button);
	}

	// MA - Enrollment Page - method to click edit period button
	public void clickEditPeriodButton() {
		clickButton(edit_Period);
	}

	// MA - Enrollment Page - method to enter enrollment start date
	public void enterEnrolmentStartDate() {
		filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");
		enterText(enrollmentStartDate,
				filloReader.getCellData("Select * from enrolmentDates", "Enrollment Start (dd/mm/yyyy)"));
	}

	// MA - Enrollment Page - method to enter enrollment end date
	public void enterEnrolmentEndDate() {
		filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");
		enterText(enrollmentEndDate,
				filloReader.getCellData("Select * from enrolmentDates", "Enrollment End (dd/mm/yyyy)"));
	}

	// MA - Enrollment Page - method to enter enrollment close date
	public void enterEnrolmentCloseDate() {
		filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");
		enterText(closeDate, filloReader.getCellData("Select * from enrolmentDates", "Close Date (dd/mm/yyyy)"));
	}

	// MA - Enrollment Page - method to enter enrollment complete date
	public void enterEnrolmentCompleteDate() {
		filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");
		enterText(completeDate, filloReader.getCellData("Select * from enrolmentDates", "Complete By (dd/mm/yyyy)"));
	}

	// MA - Enrollment Page - method to click submit
	public void clickSubmitButton() {
		clickButton(submit_Button);
	}

	// Functions complete
	// MA - Enrollment Page - method to click complete enrollment
	public void clickCompleteEnrollmentButton() {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(clickEnrolmentMenu));
		clickActionClass(clickEnrolmentMenu, clickCompleteEnrollment);
	}

	// MA - Enrollment Page - method to select radio button
	public void selectEmployeeIDRadioComplete() {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(radio_EmployeeIDComplete));
		RadioSelector(radio_EmployeeIDComplete, "");

	}

	// MA - Enrollment Page - method to enter employeeID
	public void enterEmployeeIDTextboxComplete() {

		filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");
		enterText(employeeIDTextbox, filloReader.getCellData("Select * from enrolmentDates", "Complete EmployeeID"));

	}

	// MA - Enrollment Page - method to click Batch complete all
	public void clickBatchCompleteAll() {
		clickButton(batchCompleteAll);

	}

	// MA - Enrollment Page - method to click OK button
	public void clickScheduleJob_btnOk() {
		clickButton(ScheduleJob_btnOk);
	}

	// MA - Enrollment Page - method to click close button
	public void clickJobProgress1_btnClose() {
		clickButton(jobProgress1_btnClose);
	}

}
