package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumberUtilities.Base;
import cucumberUtilities.Xls_Reader_Fillo;

public class MAClaimPaymentPage extends Base{

	@FindBy(how = How.XPATH, using = "//body[@id='Body']/form[@id='Form']/div[@class='genericContainer']/table[@class='genericContainer']/tbody/tr/td[@class='containerframe']/table[@class='genericContainer']/tbody/tr/td[@id='lcp']/div[@class='genericContainer']/div[@id='BA_Menu']/ul/li[19]/a[1]")
	private WebElement maPaymentLink;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Generate Files (New)')]")
	private WebElement gernatePaymentFileLink;
	
	@FindBy(how = How.XPATH, using = "//img[contains(@id, '_PaymentGenerateNew_imgFrom')]")
	private WebElement fromDateLink;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@id, '_PaymentGenerateNew_ceFrom_today')]")
	private WebElement fromTodayDate;
	
	@FindBy(how = How.XPATH, using = "//img[contains(@id, '_PaymentGenerateNew_imgTo')]")
	private WebElement toDateLink;
	
	@FindBy(how = How.XPATH, using = "//div[contains(@id,'_PaymentGenerateNew_ceTo_today')]")
	private WebElement toTodayDate;
	
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_PaymentGenerateNew_btnGenerate')]")
	private WebElement generatePaymentButton;
	
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_PaymentGenerateNew_BA2JobProgress1_btnClose')]")
	private WebElement generatePaymentPopUpCloseButton;
	
	@FindBy(how = How.XPATH, using = "//b[contains(text(),'$10.00')]")
	private WebElement generatePaymentAmount;
	
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_overallCheckBox')]")
	private WebElement paymentCheckbox;
	
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_PaymentGenerateNew_btnConfirmThread')]")
	private WebElement confirmPaymentButton;
	
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_PaymentGenerateNew_ucScheduleJob_btnOk')]")
	private WebElement paymentOkButton;
	
	@FindBy(how = How.XPATH, using = "//span[contains(@id, 'Summary') and contains(text(), 'Payment file confirmed sucessfully')]")
	private WebElement paymentSucessfulMessage;
	
	
	// Constructor
	public MAClaimPaymentPage(WebDriver driver) {
			  this.driver = driver;
			  PageFactory.initElements(driver, this);
	}
			
	public void clickGernatePaymentFileLink() {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(maPaymentLink));
		clickActionClass(maPaymentLink, gernatePaymentFileLink);
	}
	
	
	
	public void clickFromTodayDate() {
		clickJS(fromDateLink);
		clickJS(fromTodayDate);
		
	}
	
	public void clickToTodayDate() {
		clickJS(toDateLink);
		clickJS(toTodayDate);
	}
	
	public void clickGeneratePaymentPopUpCloseButton() {
		clickButton(generatePaymentPopUpCloseButton);
	}
	public void clickGeneratePaymentButton() {
		clickButton(generatePaymentButton);
	}

	public void verifyGeneratePaymentAmount() {
	    elementIsPresent(generatePaymentAmount);
	}
	
	public void clickPaymentCheckbox() {
		SelectCheckbox(paymentCheckbox, "select");
	}

	public void clickConfirmPaymentButton() {
		clickButton(confirmPaymentButton);
	}
	
	public void clickPaymentOkButton() {
		clickButton(paymentOkButton);
	}
	
	public void verifyPaymentSucessfulMessage() {
	    elementIsPresent(paymentSucessfulMessage);
	}	
	
}
