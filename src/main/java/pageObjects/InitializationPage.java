package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumberUtilities.Base;
import cucumberUtilities.Xls_Reader_Fillo;

public class InitializationPage extends Base {

	public static Logger log = LogManager.getLogger(LoginPage.class.getName());

	Xls_Reader_Fillo filloReader = null;

	// Page Factory- OR:
	// MA - Initialization Page - enter enrollment start date
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_ChoosePlanNew_txtEnrolStDt')]")
	private WebElement enrolmentFrom;

	// MA - Initialization Page - enter enrollment to date
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_ChoosePlanNew_txtEnrolEndDt')]")
	private WebElement enrolmentTo;

	// MA - Initialization Page - enter enrollment to date
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_ChoosePlanNew_txtHREmployeeId')]")
	private WebElement employeeName;

	// MA - Initialization Page - click search employee
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_ChoosePlanNew_btnSubmit')]")
	private WebElement searchEmployee;

	// MA - Initialization Page - click search employee textbox
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_SearchEmployeeNew_txtSearch')]")
	private WebElement SearchEmployeeNew_txtSearch;

	// MA - Initialization Page - click select radio button
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_SearchEmployeeNew_rdbsearch_2')]")
	private WebElement SearchEmployeeNew_rdbsearch;

	// MA - Initialization Page - click search button
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_SearchEmployeeNew_btnSearch')]")
	private WebElement SearchEmployeeNew_btnSearch;

	// MA - Initialization Page - Select no of record
	@FindBy(how = How.XPATH, using = "//*[contains(@id,'_SearchEmployeeNew_ddlPageSize')]")
	private WebElement selectNoOfRecords;

	// MA - Initialization Page - click submit button
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_SearchEmployeeNew_btnSubmit')]")
	private WebElement SearchEmployeeNew_btnSubmit;

	// MA - Initialization Page - click OK button
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_SearchEmployeeNew_ucScheduleJob_btnOk')]")
	private WebElement ScheduleJob_btnOk;

	// MA - Initialization Page - click close button
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_SearchEmployeeNew_BA2JobProgress1_btnClose')]")
	private WebElement JobProgress1_btnClose;

	// MA - Initialization Page - click close button
	@FindBy(how = How.XPATH, using = "//*[Contains(@id, '_ChoosePlanNew_BA2JobProgress1_btnClose')]")
	private WebElement ChoosePlanNew_BA2JobProgress1_btnClose;

	// Constructor
	public InitializationPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	// MA - Initialization Page - Method to enter enrollment start date
	public void enterEmrolmentStartDate() {
		filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");
		enterText(enrolmentFrom, filloReader.getCellData("Select * from testData", "Enrolment From"));
	}
	
	// MA - Initialization Page - Method to enter enrollment end date
	public void enterEnrolmentEndDate() {
		filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");
		enterText(enrolmentTo, filloReader.getCellData("Select * from testData", "Enrolment To"));
	}

	// MA - Initialization Page - Method to enter employee name
	public void enterEmployeeName() {
		enterText(employeeName, "BA2_DSO_");
	}

	// MA - Initialization Page - Method to click search employee
	public void searchEmployeeButton() {
		clickButton(searchEmployee);
	}

	// MA - Initialization Page - Method to enter employee name in search field
	public void enterEmployeeNameInSearchField() {
		enterText(SearchEmployeeNew_txtSearch, "BA2_DSO_");
	}

	// MA - Initialization Page - Method to click radio button
	public void clickEmployeeRadioButton() {
		clickButton(SearchEmployeeNew_rdbsearch);
	}

	// MA - Initialization Page - Method to click submit button
	public void clickSubmitButton() {
		clickButton(SearchEmployeeNew_btnSearch);
	}

	// MA - Initialization Page - Method to select no of record dropdown button
	public void selectNoOFRecords() {
		executeInputOperation(selectNoOfRecords, "100");
	}

	// MA - Initialization Page - Method to click submit button
	public void clickSubmitButtonForInitialization() {
		clickButton(SearchEmployeeNew_btnSubmit);
	}

	// MA - Initialization Page - Method to click OK button
	public void clickScheduleJob_btnOk() {
		clickButton(ScheduleJob_btnOk);
	}

	// MA - Initialization Page - Method to click close button
	public void clickJobProgress1_btnClose() {
		clickButton(JobProgress1_btnClose);
	}

}
