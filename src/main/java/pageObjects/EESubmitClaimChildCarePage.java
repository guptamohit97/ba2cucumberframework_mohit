package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.codoid.products.exception.FilloException;

import cucumberUtilities.Xls_Reader_Fillo;

import cucumberUtilities.Base;
import cucumberUtilities.Xls_Reader_Fillo;

public class EESubmitClaimChildCarePage extends Base {

	public static Logger log = LogManager.getLogger(LoginPage.class.getName());

	Xls_Reader_Fillo filloReader = null;

	// EE - Login Page - Select Employee Portal
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_LoginDefault_NewLoginDefault1_SignIn_v2_ctlEmployeeOrAdmin_btnEmployee')]")
	private WebElement eeAdminButton;
	
	// EE - Login Page - Select Employee Portal
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_LoginDefault_NewLoginDefault1_SignIn_v2_ctlEmployeeOrAdmin_btnEmployee')]")
	private WebElement eeEmployeeButton;

	// EE - Create New Claim Page - Click create claim link
	@FindBy(how = How.XPATH, using = "//a[contains(@id, '_BA2DashBoard_SUBMITCLAIM')]")
	private WebElement createClaimLink;

	// EE - Create New Claim Page - select claimant name dropdown
	@FindBy(how = How.XPATH, using = "//select[contains(@id, '_ClaimsSubmit_ClaimSubmitControlV2_lstClaimantName')]")
	private WebElement claimantNameDropDown;

	// EE - Create New Claim Page - select claim type dropdown
	@FindBy(how = How.XPATH, using = "//select[contains(@id,'_ClaimsSubmit_ClaimSubmitControlV2_lstClaimItemName')]")
	private WebElement claimTypeDropdown;

	// EE - Create New Claim Page - enter provider name
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsSubmit_ClaimSubmitControlV2_txtProviderName')]")
	private WebElement clinicProviderName;

	// EE - Create New Claim Page - enter receipt date
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsSubmit_ClaimSubmitControlV2_txtReceiptDate')]")
	private WebElement receiptDate;

	// EE - Create New Claim Page - enter receipt no
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsSubmit_ClaimSubmitControlV2_txtReceiptNo')]")
	private WebElement receiptNo;

	// EE - Create New Claim Page - enter receipt amount
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsSubmit_ClaimSubmitControlV2_txtReceiptAmount')]")
	private WebElement receiptAmount;

	// EE - Create New Claim Page - enter remarks
	@FindBy(how = How.XPATH, using = "//textarea[contains(@id, '_ClaimsSubmit_ClaimSubmitControlV2_txtEmployeeRemarks')]")
	private WebElement remarkTextBox;

	// EE - Create New Claim Page - click add attachment
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsSubmit_ClaimSubmitControlV2_AttachFiles_File1')]")
	private WebElement attachAttachment;

	// EE - Create New Claim Page - enter attachment remarks
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsSubmit_ClaimSubmitControlV2_AttachFiles_EditUploadedFiles_grdFiles_ctl02_txtRemarks')]")
	private WebElement attachAttachmentTextBox;

	// EE - Create New Claim Page - click ready to submit button
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsSubmit_ClaimSubmitControlV2_ready')]")
	private WebElement readyToSubmitButton;

	// EE - Create New Claim Page - click confirm button
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsSubmit_ClaimSubmissionListV2_btnConfirmClaims')]")
	private WebElement confirmSubmitButton;

	// EE - Create New Claim Page - click OK button
	@FindBy(how = How.XPATH, using = "//input[contains(@id, 'confirmButton')]")
	private WebElement claimConfirmButton;

	// EE - Create New Claim Page - click search claim button
	@FindBy(how = How.XPATH, using = "//span[contains(@id, '_ClaimsSearch_lblTitle')]")
	private WebElement eeClaimSearchHeader;
	
	@FindBy(how = How.XPATH, using = "//a[@id='aCreateClaim']")
	private WebElement eeCreateClaim;

	// Constructor
	public EESubmitClaimChildCarePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	// EE - Claims Page - Method to enter employee Portal
	public void clickChildcareEeAdminButton() {
		clickButton(eeAdminButton);
	}

	// EE - Claims Page - Method to enter employee Portal
	public void clickChildcareEeEmployeeButton() {
		clickButton(eeEmployeeButton);
	}

	// EE - Claims Page - Method to click create new claim link
	public void clickChildcareCreateClaimLink() {
		clickJS(createClaimLink);
	}

	// EE - Claims Page - Method to select claimant name dropdown
	public void selectChildcareClaimantNameDropDown(String testValue) throws FilloException {
		
		String data = getData(testValue,"ChildCareClaim","Testcase","Claimant Name");
		SelectFromListBox(claimantNameDropDown, data);
		//SelectFromListBox(claimantNameDropDown, "BA2_DSO_050_23_Wife");

	}

	// EE - Claims Page - Method to select claim type dropdown
	public void selectChildcareClaimTypeDropdown(String testValue) throws FilloException {

		String data = getData(testValue,"ChildCareClaim","Testcase","Claim Type");
		SelectFromListBox(claimTypeDropdown, "   "+data);
		
		//SelectFromListBox(claimTypeDropdown,"   Childcare services including infant care services (licensed centres only)");
		
	}

	// EE - Claims Page - Method to enter provider name
	public void enterChildcareClinicProviderName(String testValue) throws FilloException {

		String data = getData(testValue,"ChildCareClaim","Testcase","Clinic/Provider Name");
		enterText(clinicProviderName, data);
		
		/*filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");
		enterText(clinicProviderName, filloReader.getCellData("Select * from ChildCareClaim", "Clinic/Provider Name"));*/

	}

	// EE - Claims Page - Method to enter receipt date
	public void enterChildcareReceiptDate(String testValue) throws FilloException {

		String data = getData(testValue,"ChildCareClaim","Testcase","Receipt Date (DD/MM/YYYY)");
		enterText(receiptDate, data);
		
		/*filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");
		enterText(receiptDate, filloReader.getCellData("Select * from ChildCareClaim", "Receipt Date (DD/MM/YYYY)"));*/

	}

	// EE - Claims Page - Method to enter receipt no
	public void enterChildcareReceiptNo(String testValue) throws FilloException {

		String data = getData(testValue,"ChildCareClaim","Testcase","Receipt No");
		enterText(receiptNo, data);
		
	/*	filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");
		enterText(receiptNo, filloReader.getCellData("Select * from ChildCareClaim", "Receipt No"));*/

	}

	// EE - Claims Page - Method to enter receipt amount
	public void enterChildcareReceiptAmount(String testValue) throws FilloException {
		
		String data = getData(testValue,"ChildCareClaim","Testcase","Receipt Amount (SGD$)");
		enterText(receiptAmount, data);

		/*filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");
		enterText(receiptAmount, filloReader.getCellData("Select * from ChildCareClaim", "Receipt Amount (SGD$)"));*/

	}

	// EE - Claims Page - Method to add attachment
	public void clickChildcareAddAttachment() {
		attachFile();
	}

	// EE - Claims Page - Method to click ready to submit button
	public void clickChildcareReadyToSubmitButton() {
		clickButton(readyToSubmitButton);
	}

	// EE - Claims Page - Method to click confirm button
	public void clickChildcareConfirmSubmitButton() {
		clickButton(confirmSubmitButton);
	}

	// EE - Claims Page - Method to click OK button
	public void clickChildcareConfirmButton() {
		clickButton(claimConfirmButton);
	}

	// EE - Claims Page - Method to click search claim button
	public void clickChildcareSearchButton() {
		clickButton(eeClaimSearchHeader);

	}
	
	// EE - Claims Page - CLick create new claim from search page
			public void clickCreateNewClaim() {
				clickJS(eeCreateClaim);
			
		}
}