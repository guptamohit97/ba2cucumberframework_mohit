package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.codoid.products.exception.FilloException;

import cucumberUtilities.Xls_Reader_Fillo;

import cucumberUtilities.Base;


public class EESubmitClaimDentalExpencesPage extends Base {

	public static Logger log = LogManager.getLogger(LoginPage.class.getName());

	Xls_Reader_Fillo filloReader = null;

	/*
	 * @FindBy(how = How.XPATH, using =
	 * "//input[contains(@id, '_LoginDefault_NewLoginDefault1_SignIn_v2_ctlEmployeeOrAdmin_btnEmployee')]"
	 * ) public WebElement eeAdminButton;
	 */

	// EE - Login Page - Select Employee Portal
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_LoginDefault_NewLoginDefault1_SignIn_v2_ctlEmployeeOrAdmin_btnEmployee')]")
	private WebElement eeEmployeeButton;

	// EE - Create New Claim Page - Click create claim link
	@FindBy(how = How.XPATH, using = "//a[contains(@id, '_BA2DashBoard_SUBMITCLAIM')]")
	private WebElement createClaimLink;

	// EE - Create New Claim Page - select claimant name dropdown
	@FindBy(how = How.XPATH, using = "//select[contains(@id, '_ClaimsSubmit_ClaimSubmitControlV2_lstClaimantName')]")
	private WebElement claimantNameDropDown;

	// EE - Create New Claim Page - select claim type dropdown
	@FindBy(how = How.XPATH, using = "//select[contains(@id,'_ClaimsSubmit_ClaimSubmitControlV2_lstClaimItemName')]")
	private WebElement claimTypeDropdown;

	// EE - Create New Claim Page - enter provider name
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsSubmit_ClaimSubmitControlV2_txtProviderName')]")
	private WebElement clinicProviderName;

	// EE - Create New Claim Page - enter receipt date
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsSubmit_ClaimSubmitControlV2_txtReceiptDate')]")
	private WebElement receiptDate;

	// EE - Create New Claim Page - enter receipt no
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsSubmit_ClaimSubmitControlV2_txtReceiptNo')]")
	private WebElement receiptNo;

	// EE - Create New Claim Page - enter receipt amount
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsSubmit_ClaimSubmitControlV2_txtReceiptAmount')]")
	private WebElement receiptAmount;

	// EE - Create New Claim Page - enter remarks
	@FindBy(how = How.XPATH, using = "//textarea[contains(@id, '_ClaimsSubmit_ClaimSubmitControlV2_txtEmployeeRemarks')]")
	private WebElement remarkTextBox;

	// EE - Create New Claim Page - click add attachment
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsSubmit_ClaimSubmitControlV2_AttachFiles_File1')]")
	private WebElement attachAttachment;

	// EE - Create New Claim Page - enter attachment remarks
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsSubmit_ClaimSubmitControlV2_AttachFiles_EditUploadedFiles_grdFiles_ctl02_txtRemarks')]")
	private WebElement attachAttachmentTextBox;

	// EE - Create New Claim Page - click ready to submit button
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsSubmit_ClaimSubmitControlV2_ready')]")
	private WebElement readyToSubmitButton;

	// EE - Create New Claim Page - click confirm button
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsSubmit_ClaimSubmissionListV2_btnConfirmClaims')]")
	private WebElement confirmSubmitButton;

	// EE - Create New Claim Page - click OK button
	@FindBy(how = How.XPATH, using = "//input[contains(@id, 'confirmButton')]")
	private WebElement claimConfirmButton;

	// EE - Create New Claim Page - click search claim button
	@FindBy(how = How.XPATH, using = "//span[contains(@id, '_ClaimsSearch_lblTitle')]")
	private WebElement eeClaimSearchHeader;
	
	@FindBy(how = How.XPATH, using = "//a[@id='aCreateClaim']")
	private WebElement eeCreateClaim;
	

	// Constructor
	public EESubmitClaimDentalExpencesPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	// EE - Claims Page - Method to enter employee Portal
	public void clickDentalEeEmployeeButton() {
		clickButton(eeEmployeeButton);
	}

	// EE - Claims Page - Method to click create new claim link
	public void clickDentalCreateClaimLink() {
		clickJS(createClaimLink);
	}

	// EE - Claims Page - Method to select claimant name dropdown
	public void selectDentalClaimantNameDropDown(String testValue) throws FilloException {
		
		
		String data = getData(testValue,"DentalExpencesClaim","Testcase","Claimant Name");
		SelectFromListBox(claimantNameDropDown, data);
		//enterText(clientLoginUserNameTextBox, data);

	}

	// EE - Claims Page - Method to select claim type dropdown
	public void selectDentalClaimTypeDropdown(String testValue) throws FilloException {

		
		String data = getData(testValue,"DentalExpencesClaim","Testcase","Claim Type");
		SelectFromListBox(claimTypeDropdown, "   "+data);

	}

	// EE - Claims Page - Method to enter provider name
	public void enterDentalClinicProviderName(String testValue) throws FilloException {

	
		String data = getData(testValue,"DentalExpencesClaim","Testcase","Clinic/Provider Name");
		enterText(clinicProviderName, data);

	}

	// EE - Claims Page - Method to enter receipt date
	public void enterDentalReceiptDate(String testValue) throws FilloException {

		String data = getData(testValue,"DentalExpencesClaim","Testcase","Receipt Date (DD/MM/YYYY)");
		enterText(receiptDate, data);
		

	}

	// EE - Claims Page - Method to enter receipt no
	public void enterDentalReceiptNo(String testValue) throws FilloException {

		

		String data = getData(testValue,"DentalExpencesClaim","Testcase","Receipt No");
		enterText(receiptNo, data);
	}

	// EE - Claims Page - Method to enter receipt amount
	public void enterDentalReceiptAmount(String testValue) throws FilloException {


		String data = getData(testValue,"DentalExpencesClaim","Testcase","Receipt Amount (SGD$)");
		enterText(receiptAmount, data);
	}

	// EE - Claims Page - Method to add attachment
	public void clickDentalAddAttachment() {
		attachFile();
	}

	// EE - Claims Page - Method to click ready to submit button
	public void clickDentalReadyToSubmitButton() {
		clickButton(readyToSubmitButton);
	}

	// EE - Claims Page - Method to click confirm button
	public void clickDentalConfirmSubmitButton() {
		clickButton(confirmSubmitButton);
	}

	// EE - Claims Page - Method to click OK button
	public void clickDentalConfirmButton() {
		clickButton(claimConfirmButton);
	}

	// EE - Claims Page - Method to click search claim button
	public void clickDentalSearchButton() {
		clickButton(eeClaimSearchHeader);

	}
	
	// EE - Claims Page - CLick create new claim from search page
		public void clickCreateNewClaim() {
			clickJS(eeCreateClaim);
		}

}