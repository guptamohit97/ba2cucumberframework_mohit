package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumberUtilities.Base;

public class MABenefitSelectionReportPage extends Base {

	@FindBy(how = How.XPATH, using = "//*[@id='BA_Menu']//a[contains(text(),'Report and Statistics')]")
	private WebElement reportStatTab;

	@FindBy(how = How.LINK_TEXT, using = "Benefit Selection Report")
	private WebElement cickBenefitSelectionReport;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_RptBenefitSelection_btnSearch')]")
	private WebElement benefitSelectionSearch;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Benefit Selection Report')]")
	private WebElement benefitSelectionReport;

	// Constructor

	public MABenefitSelectionReportPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void clickBenefitSelectionReport() {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(reportStatTab));
		clickActionClass(reportStatTab, cickBenefitSelectionReport);
	}

	public void clickBenefitSelectionSearchButton() {
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.elementToBeClickable(benefitSelectionSearch));
		clickButton(benefitSelectionSearch);
	}

	public void validateBenefitSelectionReportName() {

		validateTextfieldvalue(benefitSelectionReport, "Benefit Selection Report");
	}

}
