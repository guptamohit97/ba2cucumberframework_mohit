package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.WebDriverWait;

import cucumberUtilities.Base;

public class HRClaimSearchPage extends Base {

	public static Logger log = LogManager.getLogger(LoginPage.class.getName());

	WebDriver driver;

	// HR - Claims Page - Click claims tab
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Claims')]")
	private WebElement hrClaimTab;

	// HR - Claims Page - select status
	@FindBy(how = How.XPATH, using = "//select[contains(@id, '_ClaimsSearch_ddlStatusSearchNew')]")
	private WebElement hrClaimStatus;

	// HR - Claims Page - Click search button
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsSearch_btnSearchNew')]")
	private WebElement hrClaimSearchButton;

	// HR - Claims Page - transaction code header
	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Transaction Code')]")
	private WebElement hrClaimTransactionCodeHeader;

	// Constructor
	public HRClaimSearchPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	// HR - Claims Page - Method to click claims tab
	public void clickHrClaimTab() {
		clickButton(hrClaimTab);
	}

	// HR - Claims Page - Method to select status
	public void selectHrClaimStatus() {
		SelectFromListBox(hrClaimStatus, "Active Claims (in processing)");

	}

	// HR - Claims Page - method to click search button
	public void clickHrClaimSearchButton() {
		clickButton(hrClaimSearchButton);
	}

	// HR - Claims Page - method to validate transaction code header
	public void verifyHrClaimTransactionCodeHeader() {
		validateTextfieldvalue(hrClaimTransactionCodeHeader, "Transaction Code");

	}
}