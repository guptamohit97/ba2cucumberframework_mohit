package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import cucumberUtilities.Xls_Reader_Fillo;

import cucumberUtilities.Base;

public class HRPage extends Base {

	public static Logger log = LogManager.getLogger(LoginPage.class.getName());
	
	Xls_Reader_Fillo filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");

	WebDriver driver;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'SecurityAccess')]")
	private WebElement SecurityAccess;

	@FindBy(how = How.LINK_TEXT, using = "Manage User")
	private WebElement ManageUser;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_UserSearch_btnAddNewClientAdmin')]")
	private WebElement addClientAdmin;

	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_UserCreateClientAdminExisting_btnSearchForEmployee')]")
	private WebElement searchForEmployee;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_UserCreateClientAdminExisting_txtFullName')]")
	private WebElement employeeFullName;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_UserCreateClientAdminExisting_btnSearch')and(@value='Search')]")
	private WebElement searchEnteredEmployee;

	@FindBy(how = How.XPATH, using = "//*[contains(@id,'_UserCreateClientAdminExisting_dbgrdData')]/tbody/tr[2]/td/a")
	private WebElement searchedEmployeeLink;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_UserCreateClientAdminExisting_btnSave')]")
	private WebElement finalSavebuttton;

	// Constructor
	public HRPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void clickManageUserButton() {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(SecurityAccess));
		clickActionClass(SecurityAccess, ManageUser);
	}

	public void clickAddClientAdminButton() {
		clickButton(addClientAdmin);
	}

	public void clickSearchForEmployeeButton() {
		clickButton(searchForEmployee);
	}

	public void enterEmployeeFullName() {

		//enterText(employeeFullName, "BA2_DSO_050_20");
		filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");
		enterText(employeeFullName,
				filloReader.getCellData("Select * from HRpage", "EmployeeID"));
	}

	public void clickSearchEnteredEmployeeButton() {
		clickButton(searchEnteredEmployee);
	}

	public void clickSearchedEmployeeLinkButton() {
		clickButton(searchedEmployeeLink);
	}

	public void clickFinalSavebutttonButton() {
		clickButton(finalSavebuttton);
	}

}