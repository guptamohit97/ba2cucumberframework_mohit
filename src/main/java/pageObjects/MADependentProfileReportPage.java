
package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumberUtilities.Base;

public class MADependentProfileReportPage extends Base {

	@FindBy(how = How.XPATH, using = "//*[@id='BA_Menu']//a[contains(text(),'Report and Statistics')]")
	private WebElement ReportStatTab;

	@FindBy(how = How.LINK_TEXT, using = "Dependent Profile Report")
	private WebElement ClickDependentReport;

	@FindBy(how = How.XPATH, using = "//select[contains(@id,'_RptDependentProfile_lstddlStatus')]//option[contains(text(),'ACTIVE')]")
	private WebElement EmployeeStatus;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_RptDependentProfile_btnShowReport')]")
	private WebElement DependentSearch;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Dependent Profile Report')]")
	private WebElement DependentProfileReport;

	// Constructor

	public MADependentProfileReportPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void clickDependentProfileReport() {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(ReportStatTab));
		clickActionClass(ReportStatTab, ClickDependentReport);
	}

	public void clickDependentSearchButton() {
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.elementToBeClickable(DependentSearch));
		clickButton(DependentSearch);
	}

	public void validateDependentProfileReportName() {

		validateTextfieldvalue(DependentProfileReport, "Dependent Profile Report");
	}
}
