package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import cucumberUtilities.Base;

public class MAClaimReviewCommonPage extends Base{


    //MA-Side link-Claim link
	@FindBy(how = How.XPATH, using = "//body[@id='Body']/form[@id='Form']/div[@class='genericContainer']/table[@class='genericContainer']/tbody/tr/td[@class='containerframe']/table[@class='genericContainer']/tbody/tr/td[@id='lcp']/div[@class='genericContainer']/div[@id='BA_Menu']/ul/li[18]/a[1]")
	public WebElement maClaimLink;

	//MA-MA claim home page-mass review claim link
	@FindBy(how = How.XPATH, using = "//a[contains(@id, '_ClaimsAdminMenu_rptMenu_ctl11_LinkButton1')]")
	public WebElement massReviewClaimsLink;

	//MA-review claim-review all button
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsMassApprovalNew_btnReviewAll')]")
	public WebElement claimReviewAllButton;

	//MA-review claim-review all link
	@FindBy(how = How.XPATH, using = "//a[contains(@id, '_lbnApproveAll')]")
	public WebElement claimReviewAllLink;
	
	//MA-review Click claim-Schedular close
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsMassApprovalNew_BA2JobProgress1_btnClose')]")
	public WebElement claimSchedularClose;
	
	
	//MA-Review claim-start review button
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsMassApproval_btnStart')]")
	public WebElement cliamReviewStartButton;
	
	//MA-Review claim-Mass Review Completed Successfully  message
	@FindBy(how = How.XPATH, using = "//span[contains(@id, '_ClaimsMassApproval_lbProcessOngoingMessage') and contains(text(), 'Mass Review Completed Successfully.')]")
	public WebElement reviewClaimSuccessfulMessage;

	
	
	// Constructor
	public MAClaimReviewCommonPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void clickMaClaimLink() {
		clickJS(maClaimLink);
	}
	
	public void clickMassReviewClaimsLink() {
		clickJS(massReviewClaimsLink);
	}
	
	public void clickClaimReviewAllButton() {
		clickButton(claimReviewAllButton);
	}
	
	public void clickClaimReviewAllLink() {
		clickButton(claimReviewAllLink);
	}
	
	public void clickCliamSchedularCloseButton() {
		clickButton(claimSchedularClose);
	}
	
	public void verifyReviewClaimSuccessfulMessage() {
	    elementIsPresent(reviewClaimSuccessfulMessage);
	}
	
}
