package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.WebDriverWait;

import cucumberUtilities.Base;
//import cucumberUtilities.Xls_Reader_Fillo;

public class HRDependentReportPage extends Base {

	public static Logger log = LogManager.getLogger(LoginPage.class.getName());

	WebDriver driver;
	
	// HR - Reports Page - Click Dependent Profile Report
	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Dependent Profile Report')]")
	private WebElement dependentProfileReport;

	// HR - Reports Page - Dependent status
	@FindBy(how = How.XPATH, using = "//select[contains(@id, '_RptDependentProfile_lstddlStatus')]")
	private WebElement dependentStatus;

	// HR - Reports Page - seacrh Dependent Profile Report
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_RptDependentProfile_btnShowReport')]")
	private WebElement searchDependentReport;

	// HR - Reports Page - Dependent Profile Report header
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Dependent Profile Report')]")
	private WebElement dependentReportHeader;

	// Constructor
	public HRDependentReportPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	// HR - Reports Page - method to click Dependent Profile Report
	public void clickDependentProfileReport() {
		clickJS(dependentProfileReport);

	}

	// HR - Reports Page - method to click Dependent status
	public void selectDependentStatus() {
		SelectFromListBox(dependentStatus, "ACTIVE");

	}

	// HR - Reports Page - method to click Dependent Profile Report search button
	public void clickSearchDependentReportButton() {
		clickButton(searchDependentReport);
	}

	// HR - Reports Page - method to verify Dependent Profile Report header
	public void verifyDependentReportHeader() {
		validateTextfieldvalue(dependentReportHeader, "Dependent Profile Report");

	}

}