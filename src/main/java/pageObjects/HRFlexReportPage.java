package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.WebDriverWait;

import cucumberUtilities.Base;
//import cucumberUtilities.Xls_Reader_Fillo;

public class HRFlexReportPage extends Base {

	public static Logger log = LogManager.getLogger(LoginPage.class.getName());

	WebDriver driver;

	// HR - Reports Page - Click flex Report
	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Flex Balance And Usage Report')]")
	private WebElement employeeFlexReport;

	// HR - Reports Page - click flex status
	@FindBy(how = How.XPATH, using = "//select[contains(@id, '_RptFlexUsuage_lstddlStatus')]")
	private WebElement employeeFlexStatus;

	// HR - Reports Page - search flex  Report
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_RptFlexUsuage_btnShowReport')]")
	private WebElement flexReportSearch;

	// HR - Reports Page - verify flex  Report header
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Flex Balance & Usage Report')]")
	private WebElement flexReportHeader;

	// Constructor
	public HRFlexReportPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	// HR - Reports Page - method to Click flex Report
	public void clickEmployeeFlexReport() {
		clickButton(employeeFlexReport);

	}

	// HR - Reports Page - method to click flex status
	public void selectEmployeeFlexStatus() {
		SelectFromListBox(employeeFlexStatus, "ACTIVE");

	}
	
	// HR - Reports Page - method to click search button for flex  Report
	public void clickFlexReportSearchButton() {
		clickButton(flexReportSearch);
	}

	// HR - Reports Page - method to verify flex  Report header
	public void verifyFlexReportHeader() {
		validateTextfieldvalue(flexReportHeader, "Flex Balance & Usage Report");
	}

}