package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumberUtilities.Base;

public class UploadEmpDepDataPage extends Base {

	public static Logger log = LogManager.getLogger(UploadEmpDepDataPage.class.getName());

	// MA - Home Page - Click Browse File button for employee
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_fupdDataFile')]")
	private WebElement BrowseFile;

	// MA - Home Page - Click Start Upload button
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_EmployeeDataNew_btnStartUpload')]")
	private WebElement StartEmpUpload;

	// MA - Home Page - Click employee submit button
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_EmployeeDataNew_btnSubmit')]")
	private WebElement EmpSubmitButton;

	// MA - Home Page - Click dependent submit button
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_DependentDataNew_btnSubmitDep')]")
	private WebElement DepSubmitButton;

	// MA - Home Page - Click close button
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_btnClose')]")
	private WebElement CloseButton;

	// MA - Home Page - Click Start Upload button for dependent
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_btnStartUpload')]")
	private WebElement StartDepUpload;

	// Constructor
	public UploadEmpDepDataPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	// MA - Home Page - Method for click close button
	public void clickCloseButton() {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(CloseButton));
		clickButton(CloseButton);
		staticwait(3);
	}

	// MA - Home Page - Method for upload csv file of employee
	public void uploadEmployeeCsv() {
		// executeInputOperation(BrowseFile, System.getProperty("user.dir") +
		// "\\TestData\\emp.csv");
		BrowseFile.sendKeys(System.getProperty("user.dir") + "\\TestData\\emp.csv");
	}

	// MA - Home Page - Method for upload csv file of dependent
	public void uploadDepCsv() {
		// executeInputOperation(BrowseFile, System.getProperty("user.dir") +
		// "\\TestData\\emp.csv");
		BrowseFile.sendKeys(System.getProperty("user.dir") + "\\TestData\\dep.csv");
	}

	// MA - Home Page - Method for click start upload for employee
	public void clickStartEmpUpload() {
		clickButton(StartEmpUpload);
	}

	// MA - Home Page - Method for click start upload for dependent
	public void clickStartDepUpload() {
		clickButton(StartDepUpload);
	}

	// MA - Home Page - Method for click submit button for employee
	public void clickEmpSubmitButton() {
		clickButton(EmpSubmitButton);
	}

	// MA - Home Page - Method for click start upload for dependent
	public void clickDepSubmitButton() {
		clickButton(DepSubmitButton);
	}

}
