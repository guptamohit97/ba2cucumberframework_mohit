package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.codoid.products.exception.FilloException;

import cucumberUtilities.Base;
import cucumberUtilities.Xls_Reader_Fillo;
import cucumberUtilities.Base;

public class MAAddNewChildcareBatchPage extends Base {

	public static Logger log = LogManager.getLogger(UploadEmpDepDataPage.class.getName());
	Xls_Reader_Fillo filloReader = null;

    //MA-Add to batch-Claim link
	@FindBy(how = How.XPATH, using = "//a[contains(@id, '_ClaimsAdminMenu_rptMenu_ctl17_LinkButton1')]")
	public WebElement searchAClaimLink;

	//MA-Add
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_SearchAClaim_txtInputSearch')]")
	public WebElement searchAClaimTextBox;

	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_SearchAClaim_btnSearch')]")
	public WebElement searchAClaimButton;

	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_SearchAClaim_gvResult_ctl02_btnAddToBatch')]")
	public WebElement addToBatchButton;
	
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsBatchList_btnAddBatch')]")
	public WebElement addBatchNewButton;
	

	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsBatchForm_txtBatchName')]")
	public WebElement newBatchNameTextBox;

	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_ClaimsBatchForm_btnSave')]")
	public WebElement batchSaveButton;
	
	@FindBy(how = How.XPATH, using = "//a[contains(@id, '_ClaimsBatchManagement_lnkBtnAddDocToBatch')]")
	public WebElement addDocumentToBatchLink;
	
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_BatchAddDoc_btnTransactionCode')]")
	public WebElement addTrancationCodeToBatchButton;
	
	@FindBy(how = How.XPATH, using = "//span[contains(@id, '_BatchAddDoc_lblDocumentID') and contains(text(), 'is successfully tagged to this batch.')]")
	public WebElement claimAddedtoBatchMessage;
	
	@FindBy(how = How.XPATH, using = "//body[@id='Body']/form[@id='Form']/div[@class='genericContainer']/table[@class='genericContainer']/tbody/tr/td[@class='containerframe']/table[@class='genericContainer']/tbody/tr/td[@id='lcp']/div[@class='genericContainer']/div[@id='BA_Menu']/ul/li[18]/a[1]")
	private WebElement claimLink;
	
	// Constructor
	public MAAddNewChildcareBatchPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void clickSearchAClaimLink() {
		clickJS(searchAClaimLink);
	}
	
	public void enterSearchAClaimTextBox(String testValue) throws FilloException {
		
		String data = getData(testValue,"ChildCareClaim","Testcase","Claimant Name");
		enterText(searchAClaimTextBox, data);
		
		/*filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");
		enterText(searchAClaimTextBox, filloReader.getCellData("Select * from batchName", "Employee"));clickJS(searchAClaimTextBox);*/
	}
	
	public void clickSearchAClaimButton() {
		clickButton(searchAClaimButton);
	}
	
	public void clickAddToBatchButton() {
		clickButton(addToBatchButton);
	}
	
	public void clickAddBatchNewButton() {
		clickButton(addBatchNewButton);
	}

	public void enterNewBatchNameTextBox(String testValue) throws FilloException {
		
		String data = getData(testValue,"ChildCareClaim","Testcase","Batch Name");
		enterText(newBatchNameTextBox, data);
		
		/*filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");
		enterText(newBatchNameTextBox, filloReader.getCellData("Select * from batchName", "Batch Name"));*/
	}
	
	public void clickBatchSaveButton() {
		clickButton(batchSaveButton);
	}

	public void clickAddDocumentToBatchLink() {
		clickJS(addDocumentToBatchLink);
	}
	
	public void clickAddTrancationCodeToBatchButton() {
		clickButton(addTrancationCodeToBatchButton);
	}
	
	public void verifyClaimIsAddedToBatch() {
	    elementIsPresent(claimAddedtoBatchMessage);
	}
	
	public void clickClaimLink() {
		clickJS(claimLink);
	}
	
}
