package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.codoid.products.exception.FilloException;

import cucumberUtilities.Base;
import cucumberUtilities.Xls_Reader_Fillo;

public class MASoaPage extends Base{

	public static Logger log = LogManager.getLogger(UploadEmpDepDataPage.class.getName());
	Xls_Reader_Fillo filloReader = null;

	//MA-SOA-search employee link
	@FindBy(how = How.XPATH, using = "//body[@id='Body']/form[@id='Form']/div[@class='genericContainer']/table[@class='genericContainer']/tbody/tr/td[@class='containerframe']/table[@class='genericContainer']/tbody/tr/td[@id='lcp']/div[@class='genericContainer']/div[@id='BA_Menu']/ul/li[13]/a[1]")
	public WebElement searchEmployeeLink;

	//MA-SOA-Employee search text box
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_SearchEmployee_txtSearch')]")
	public WebElement searchEmployeeIdTextBox;

	//MA-SOA-search employee id radio button
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_SearchEmployee_rdbsearch_2')]")
	public WebElement searchEmployeeIdButton;
		
    //MA-SOA-search employee SOA button
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_SearchEmployee_btnSearch')]")
	public WebElement searchEmployeeSoaButton;

	//MA-SOA-employee link
	@FindBy(how = How.XPATH, using = "//tr//a[contains(@href, 'Edit$0')]")
	public WebElement searchEmployeeSoaLink;

	//MA-SOA-SOA icon button
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_EditEmployee_imgBtnViewAccountsClaims')]")
	public WebElement soaSearchIconButton;

	//MA-SOA-Balance
	@FindBy(how = How.XPATH, using = "//span[contains(@id, '_StatementOfAccount_DSOSG_lblBalanceAfterPendingAmount')]")
	public WebElement soaBalancePoints;
	
	// Constructor
	public MASoaPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void clickSearchEmployeeLink() {
		clickJS(searchEmployeeLink);
	}
	
	
	public void enterSearchEmployeeIdTextBox(String testValue) throws FilloException {
		
		String data = getData(testValue,"SOAPage","Testcase","EmployeeID");
		enterText(searchEmployeeIdTextBox, data);
		
		/*filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");
		enterText(searchEmployeeIdTextBox, filloReader.getCellData("Select * from stateMentOfAccount", "EmployeeID"));*/
	}
	
	public void clickSearchEmployeeIdButton() {
		executeInputOperation(searchEmployeeIdButton, "select");
	}
	
    
	public void clickSearchEmployeeSoaButton() {
		clickButton(searchEmployeeSoaButton);
	}
	
	public void clicksearchEmployeeSoaLink() {
		clickJS(searchEmployeeSoaLink);
	}
	
	public void clickSoaSearchIconButton() {
		clickButton(soaSearchIconButton);
	}
	
	public void verifySoaBalancePoints(String testValue) throws FilloException {
		
		String data = getData(testValue,"SOAPage","Testcase","SOA Balance");
		validateTextfieldvalue(soaBalancePoints, data);
		
		/*filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");
		validateTextfieldvalue(soaBalancePoints, filloReader.getCellData("Select * from stateMentOfAccount", "SOA Balance"));*/
	}
	
}
