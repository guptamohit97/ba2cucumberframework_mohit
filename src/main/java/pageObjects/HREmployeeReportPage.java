package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumberUtilities.Base;
//import cucumberUtilities.Xls_Reader_Fillo;

public class HREmployeeReportPage extends Base {

	public static Logger log = LogManager.getLogger(LoginPage.class.getName());

	WebDriver driver;

	// HR - Login Page - Click Admin button
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_LoginDefault_NewLoginDefault1_SignIn_v2_ctlEmployeeOrAdmin_btnAdmin')]")
	private WebElement hrAdminButton;

	// HR - Home Page - Click reporta tab
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Reports')]")
	private WebElement hrReportTab;

	// HR - Reports Page - Click Employee Profile Report
	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Employee Profile Report')]")
	private WebElement employeeProfileRepot;

	// HR - Reports Page - Employee status
	@FindBy(how = How.XPATH, using = "//select[contains(@id,'_RptEmployeeProfile_MY_lstddlStatus')]")
	private WebElement employeeStatus;

	// HR - Reports Page - seacrh Employee Profile Report
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_RptEmployeeProfile_MY_btnShowReport')]")
	private WebElement searchEmployeeReport;

	// HR - Reports Page - Employee Profile Report header
	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Employee Profile Report')]")
	private WebElement employeeReportHeader;

	// Constructor
	public HREmployeeReportPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	// HR - Reports Page - method to click HR admin button
	public void clickHrAdminButton() {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(hrAdminButton));
		clickButton(hrAdminButton);
	}

	// HR - Reports Page - method to click reports tab
	public void clickHrReportTab() {
		clickButton(hrReportTab);
	}

	// HR - Reports Page - method to click Employee Profile Report
	public void clickEmployeeProfileRepot() {
		clickJS(employeeProfileRepot);

	}

	// HR - Reports Page - method to click Employee status
	public void selectEmployeeStatus() {
		SelectFromListBox(employeeStatus, "ACTIVE");

	}

	// HR - Reports Page - method to click Employee Profile Report search button
	public void clickSearchEmployeeReportButton() {
		clickButton(searchEmployeeReport);
	}

	// HR - Reports Page - method to verify Employee Profile Report header
	public void verifyEmployeeReportHeader() {
		validateTextfieldvalue(employeeReportHeader, "Employee Profile Report");

	}

}