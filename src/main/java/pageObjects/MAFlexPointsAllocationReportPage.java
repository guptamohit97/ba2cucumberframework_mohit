package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumberUtilities.Base;

public class MAFlexPointsAllocationReportPage extends Base {

	@FindBy(how = How.XPATH, using = "//*[@id='BA_Menu']//a[contains(text(),'Report and Statistics')]")
	private WebElement reportStatTab;

	@FindBy(how = How.LINK_TEXT, using = "Flex Points Allocation Report")
	private WebElement cickFlexPointReport;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_RptFlexPtsAllocation_btnShowReport')]")
	private WebElement flexpointSearch;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Flex Points Allocation Report')]")
	private WebElement flexPointAllocationReport;

	// Constructor

	public MAFlexPointsAllocationReportPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void clickFlexPointReport() {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(reportStatTab));
		clickActionClass(reportStatTab, cickFlexPointReport);
	}

	public void clickFlexPointSearchButton() {
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.elementToBeClickable(flexpointSearch));
		clickButton(flexpointSearch);
	}

	public void validateFlexPointReportName() {

		validateTextfieldvalue(flexPointAllocationReport, "Flex Points Allocation Report");
	}
}
