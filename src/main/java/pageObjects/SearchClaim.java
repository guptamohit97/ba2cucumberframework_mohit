package pageObjects;

import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import cucumberUtilities.Base;

public class SearchClaim extends Base {

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_SearchAClaim_txtInputSearch')]")
	private WebElement SearchClaim;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_SearchAClaim_rblTextSearch') and @value='TransactionCode']")
	private WebElement ClickTransactionCode;

	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_SearchAClaim_btnSearch')]")
	private WebElement ClickClaimSearch;

	public SearchClaim(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

}
