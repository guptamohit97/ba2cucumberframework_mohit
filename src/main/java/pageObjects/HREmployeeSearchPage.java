package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.WebDriverWait;

import cucumberUtilities.Base;
import cucumberUtilities.Xls_Reader_Fillo;
//import cucumberUtilities.Xls_Reader_Fillo;
import pageObjects.LoginPage;

public class HREmployeeSearchPage extends Base {

	public static Logger log = LogManager.getLogger(LoginPage.class.getName());
	
	Xls_Reader_Fillo filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");
	
	WebDriver driver;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Search Employee')]")
	private WebElement hrSearchEmployeeTab;

	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_SearchEmployee_v2_rdbsearch_2')]")
	private WebElement hrEmployeeIdRadioButton;

	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_SearchEmployee_v2_txtSearch')]")
	private WebElement hrEmployeeTextBox;

	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_SearchEmployee_v2_btnSearch')]")
	private WebElement hrEmployeeSearchButton;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'BA2_DSO_')]")
	private WebElement hrEmployeeIdLink;

	@FindBy(how = How.XPATH, using = "//h1[contains(text(),'Personal Details')]")
	private WebElement hrEmployeeSearchHeader;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Dependents')]")
	private WebElement hrDependentSearchLink;

	@FindBy(how = How.XPATH, using = "//h1[contains(text(),'Dependents')]")
	private WebElement hrDependentSearchHeader;

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Statement Of Account')]")
	private WebElement hrSoaSearchLink;

	@FindBy(how = How.XPATH, using = "//h1[contains(text(),'Statement of Account')]")
	private WebElement hrSoaHeader;

	// Constructor
	public HREmployeeSearchPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void clickHrSearchEmployeeTab() {
		clickButton(hrSearchEmployeeTab);
	}

	public void clickHrEmployeeIdRadioButton() {
		RadioSelector(hrEmployeeIdRadioButton, "Select");

	}

	public void enterHrEmployeeTextBox() {
		//enterText(hrEmployeeTextBox, "BA2_DSO_050_20");
		filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");
		enterText(hrEmployeeTextBox,
				filloReader.getCellData("Select * from HRpage", "Search Employee"));
	}

	public void clickHrEmployeeSearchButton() {
		clickButton(hrEmployeeSearchButton);
	}

	public void clickHrEmployeeIdLink() {
		clickJS(hrEmployeeIdLink);
	}

	public void verifyHrEmployeeSearchHeader() {
		validateTextfieldvalue(hrEmployeeSearchHeader, "Personal Details");

	}

	public void clickHrDependentSearchLink() {
		clickJS(hrDependentSearchLink);
	}

	public void verifyHrDependentSearchHeader() {
		validateTextfieldvalue(hrDependentSearchHeader, "Dependent detail page");

	}

	public void clickHrSoaSearchLink() {
		clickJS(hrSoaSearchLink);
	}

	public void verifyHrSoaHeader() {
		validateTextfieldvalue(hrSoaHeader, "Statement of Account");

	}

}
