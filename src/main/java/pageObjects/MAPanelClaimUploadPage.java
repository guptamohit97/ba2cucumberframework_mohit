package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumberUtilities.Base;
import cucumberUtilities.Xls_Reader_Fillo;

public class MAPanelClaimUploadPage extends Base{

	public static Logger log = LogManager.getLogger(UploadEmpDepDataPage.class.getName());


	@FindBy(how = How.XPATH, using = "//*[@id='BA_Menu']//a[contains(text(),'Implementation')]")
	private WebElement Implementation;

    @FindBy(how = How.LINK_TEXT, using = "Panel Claims Upload (New)")
	private WebElement uploadPanelClaimLink;
	
	@FindBy(how = How.XPATH, using = "//input[contains(@id,'_PanelClaimsUploadNew_fupCSVFile1')]")
	private WebElement BrowseFile;
	
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_PanelClaimsUploadNew_btnStartUpload')]")
	private WebElement startPanelUploadButton;
	
	@FindBy(how = How.XPATH, using = "//span[contains(@id, 'Summary') and contains(text(), 'processed successfully')]")
	private WebElement panelClaimSucessfulMessage;
	
	@FindBy(how = How.XPATH, using = "//input[contains(@id, '_PanelClaimsUploadNew_BA2JobProgress1_btnClose')]")
	private WebElement popupPanelUploadCloseButton;
	
	@FindBy(how = How.XPATH, using = "//b[contains(text(),'$10.00')]")
	private WebElement generatePaymentAmount;
	
	
	// Constructor
	public MAPanelClaimUploadPage(WebDriver driver) {
			  this.driver = driver;
			  PageFactory.initElements(driver, this);
	}
			
	public void clickUploadPanelClaimButton() {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(Implementation));
		clickActionClass(Implementation, uploadPanelClaimLink);
		
		
		/*WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(Implementation));
		clickActionClass(Implementation, UploadEmployeeData);*/
	}
	
	public void uploadPanelClaimCsv() {
	
		//chooseUploadFile.sendKeys(System.getProperty("user.dir") + "\\TestData\\PanelClaimTemplate.csv");
		BrowseFile.sendKeys(System.getProperty("user.dir") + "\\TestData\\PanelClaimTemplate.csv");
	}
	
	public void clickStartPanelUploadButton() {
		clickButton(startPanelUploadButton);
	}

	public void verifyPanelClaimSucessfulMessage() {
	    elementIsPresent(panelClaimSucessfulMessage);
	}
	
	

	public void clickPopupPanelUploadCloseButton() {
		clickButton(popupPanelUploadCloseButton);
	}
	
	
	public void verifyGeneratePaymentAmount() {
	    elementIsPresent(generatePaymentAmount);
	}
		
			
			
	
}
