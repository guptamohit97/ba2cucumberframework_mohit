package pageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumberUtilities.Base;
import cucumberUtilities.Xls_Reader_Fillo;

public class LoginPage extends Base {

	public static Logger log = LogManager.getLogger(LoginPage.class.getName());

	WebDriver driver;

	Xls_Reader_Fillo filloReader = new Xls_Reader_Fillo(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");

	String email = filloReader.getCellData("Select * from testData", "Email");

	// MA - Login Page - Enter email address
	@FindBy(how = How.ID, using = "ContentPlaceHolder1_TextBox1")
	private WebElement emailAddress;

	// MA - Login Page - Click Login Button
	@FindBy(how = How.ID, using = "ContentPlaceHolder1_PassiveSignInButton")
	private WebElement nextButton;

	// MA - Home Page - Click Logout Button
	@FindBy(how = How.XPATH, using = "//a[contains(@id,'_dnnLOGIN_cmdLogin')]")
	private WebElement logOut;

	// Constructor
	public LoginPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	// MA - Login Page - Method to enter email address
	public void enterEmailAddress() {
		// System.out.println(email);
		executeInputOperation(emailAddress, email);
		log.info("Enter the email address" + email + " for the employee in the " + emailAddress + "Field");

	}

	// MA - Login Page - Method to click login button
	public void clickNextButton() {
		log.info("Clicked on the " + nextButton.getText());
		clickButton(nextButton);

	}

	// MA - Login Page - Method to click logout button
	public void clickLogOut() {
		log.info("Clicking on logout button");
		clickButton(logOut);
		log.info("Clicked on logout button");

	}

}
