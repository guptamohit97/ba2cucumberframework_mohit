package cucumberUtilities;

import java.awt.Robot;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

import cucumbersManagers.WebDriverManager;

public class Base {

	public static Logger log = LogManager.getLogger(Base.class.getName());

	WebDriverManager wd = new WebDriverManager();

	protected WebDriver driver = wd.getDriverr();

	public void performLogOutOperation() {
		WebElement element = driver
				.findElement(By.xpath("//*[contains(@id,'_evoHeader_profileSideBar')]//following::div"));
		waitFortheElement(60, element);
		clickJS(element);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		driver.findElement(By.id("dnn_evoHeader_lnkLogOut")).click();
	}

	public void executeInputOperation(WebElement element, String Value) {

		// gets the type of the object e.g checkbox, textbox etc
		String ObjectType = element.getAttribute("Type");
		// gets the tagname for the object e.g. input, select etc
		String ObjectTag = element.getTagName();

		log.info("ObjectTag " + ObjectTag + " and ObjectType " + ObjectType);
		// if object type is not found then mark as failed
		if (ObjectType == null) {
			ObjectType = element.getAttribute("type");
			if (ObjectType == null) {
				log.info("Object Type is null");
				ObjectType = "Cannot find";
			}
		}
		// if object tag cannot be found then mark as failed
		if (ObjectTag == null) {
			log.info("Object Tag is null");
			ObjectTag = "Cannot find";
		}

		// if object tag is select or option i.e dropdown, listbox or radio button etc
		if ((ObjectTag.equalsIgnoreCase("select")) || (ObjectTag.equalsIgnoreCase("option"))) {
			log.info("select combobox operation ");
			waitFortheElement(120, element);
			SelectFromListBox(element, Value);
		}

		// this block is execute if we have to select a checkbox
		else if (ObjectType.equalsIgnoreCase("checkbox")) {
			log.info("select checkbox operation ");
			waitFortheElement(120, element);
			SelectCheckbox(element, Value);
		}
		// select checkbox
		else if (ObjectType.equalsIgnoreCase("checkbox") && (ObjectTag.equalsIgnoreCase("input"))) {
			log.info("select checkbox operation ");
			waitFortheElement(120, element);
			SelectCheckbox(element, Value);
		}
		// Text input field
		else if (ObjectType.equalsIgnoreCase("text") && ObjectTag.equalsIgnoreCase("input")
				|| ObjectType.equalsIgnoreCase("text")) {
			log.info("Enter data operation");
			waitFortheElement(120, element);
			enterText(element, Value);
		}

		// Text area field
		else if (ObjectTag.equalsIgnoreCase("textarea")) {
			log.info("Enter data operation");
			waitFortheElement(120, element);
			enterText(element, Value);
		}

		// Android Mobile Input Field
		else if (ObjectType.equalsIgnoreCase("number") && ObjectTag.equalsIgnoreCase("input")) {
			log.info("Enter data operation");
			waitFortheElement(120, element);
			enterText(element, Value);
		}

		// Android Mobile date Input Field
		else if (ObjectType.equalsIgnoreCase("date") && ObjectTag.equalsIgnoreCase("input")) {
			log.info("Enter data operation");
			waitFortheElement(120, element);
			enterText(element, Value);
		}

		// Text input field
		else if (ObjectType.equalsIgnoreCase("password") && ObjectTag.equalsIgnoreCase("input")
				|| ObjectType.equalsIgnoreCase("password")) {
			log.info("Enter data operation");
			waitFortheElement(120, element);
			enterText(element, Value);
		}
		// select radio
		else if (ObjectType.equalsIgnoreCase("radio")) {
			log.info("Radio operation ");
			waitFortheElement(120, element);
			RadioSelector(element, Value);
		}
		// select radio
		else if (ObjectType.equalsIgnoreCase("radio") && ObjectTag.equalsIgnoreCase("input")) {
			log.info("Radio operation ");
			waitFortheElement(120, element);
			RadioSelector(element, Value);
		}
		// default message
		else {
			log.info(" Object type and tag has not been built yet");

		}

	}

	public void elementIsPresent(WebElement element) {
		if (element != null) {
			log.info("Element is Present");
		} else {
			log.info("Element is not Present");
		}
	}

	public boolean retryingFindClick(By by) {
		boolean result = false;
		int attempts = 0;
		while (attempts < 2) {
			try {
				driver.findElement(by).click();
				result = true;
				break;
			} catch (StaleElementReferenceException e) {
			}
			attempts++;
		}
		return result;
	}

	public boolean retryingFindElement(WebElement element) {
		boolean result = false;
		int attempts = 0;
		while (attempts < 2) {
			try {
				elementIsPresent(element);
				result = true;
				break;
			} catch (StaleElementReferenceException e) {
			}
			attempts++;
		}
		return result;
	}

	public void clickActionClass(WebElement element, WebElement hoverLink) {
		driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
		retryingFindElement(element);
		Actions builder = new Actions(driver);
		builder.moveToElement(element).build().perform();
		waitFortheElement(60, element);
		WebElement link = hoverLink;
		builder.moveToElement(link).click().build().perform();
		log.info("Element is Present and clicked");
		staticwait(3);
	}

	public void enterText(WebElement element, String ValueToEnter) {

		waitFortheElement(120, element);
		element.clear();
		// enter the value in the textfield using sendkeys
		element.sendKeys(ValueToEnter);

	}

	public String currentDate() {

		DateFormat dateFormat = new SimpleDateFormat("d/MM/yyyy ");

		// get current date time with Date()
		Date date = new Date();

		// Now format the date
		String todayDate = dateFormat.format(date);

		System.out.println(todayDate);

		String[] str = todayDate.split("[/]");
		return str[0];
	}

	// this function accepts the webelement and the value for the webelement
	public boolean validateTextfieldvalue(WebElement ele, String vl) {
		boolean rsult = false;
		// gets the value for the webelement and stores in a variable

		String attvalue = ele.getAttribute("value");

		if (attvalue.equalsIgnoreCase(vl)) {
			rsult = true;
		}
		return rsult;
	}

	// this function accepts the object for the radio button and the value select or
	// unselect
	public static void RadioSelector(WebElement element, String flag) {
		// WebElement element=GenericFunctions.Field_obj(element);

		// WebElement object_name = element;
		// get the current status of the radio button
		boolean currentFlag = element.isSelected();

		// if operation is to select and the radio button is already selected then set
		// status as passed
		if (flag.equalsIgnoreCase("select") || flag == null || flag.equals("")) {
			if (currentFlag == true) {
				log.info("The Radio Button " + element + " is already selected");
				System.out.println("The Radio Button  " + element + " is already selected");
			}

			// if current status is not already selected then select the radio button
			else {
				element.click();
			}
		}

		// if operation is to unselect and current status is selected then click the
		// radio button and unselect
		else if (flag.equalsIgnoreCase("unselect")) {
			if (currentFlag == true) {
				element.click();
			}
			// else if it is already unselected then set status as passed and do logging
			else {
				log.info("The Radio Button " + element + " is already De-Selected");
				System.out.println("The Radio Button  " + element + " is already De-Selected");
			}
		}
		// executed if there is someother value then select or unselect
		else {
			System.out.println("select is not found in excel");
		}

	}

	public void typecharacter(String s) {
		try {
			// create an object for the robot class
			Robot robot = new Robot();
			byte[] bytes = s.getBytes();
			for (byte b : bytes) {
				int code = b;
				// keycode only handles [A-Z] (which is ASCII decimal [65-90])
				if (code > 96 && code < 123)
					code = code - 32;
				robot.delay(40);
				robot.keyPress(code);
				robot.keyRelease(code);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
	}

	public static void SelectFromListBox(WebElement element, String ValueToSelect) {
		Select Object_NM = new Select(element);
		String value = "";
		try {
			// get the value selected
			value = Object_NM.getFirstSelectedOption().getText();
			if (value.equalsIgnoreCase(ValueToSelect)) {
				log.info(ValueToSelect + " Value is already selected in dropdown!!");
				// exe_rst_status = 1;
			} else {
				if (!Object_NM.isMultiple()) {
					// select the value in the list box and status as passed and log entry
					Object_NM.selectByVisibleText(ValueToSelect);
					log.info(ValueToSelect + " value selected!!");
					// exe_rst_status = 1;
				}
				// incase of a multiselect listbox set status as failed and do log entry
				else {
					log.info(Object_NM + " is multiselect drop down list.. Use different Action");
					// for this object");
					// exe_rst_status = 2;
				}
			}

		}
		// catch block for function
		catch (Exception e) {
			log.warn("Exception handled for dropdown function");

			// calling the select method again if it is not a multiselect list box
			if (!Object_NM.isMultiple()) {
				Object_NM.selectByVisibleText(ValueToSelect);
				log.info(ValueToSelect + " value selected!!");
				// exe_rst_status = 1;
			}
			// else for multiselect list box
			else {
				log.info(Object_NM + " is multiselect drop down list.. Use different Action");
				// for this object");
				// exe_rst_status = 2;
			}
		}

	}

	// this function accepts the object name and value incase of checkbox value will
	// be select or unselect
	public static void SelectCheckbox(WebElement element, String flag) {

		// get the current status of the checkbox i.e true or false
		boolean currentFlag = element.isSelected();
		// if operation is to select and the state is already selected then set status
		// as passed
		if (flag.equalsIgnoreCase("select") || flag == null || flag.equals("")) {
			if (currentFlag == true) {
				// //Logger.info("The checkbox "+element+" is already selected");
				log.info("The checkbox is already selected");
				// exe_rst_status = 1;
			}

			// select the checkbox, set status as passed
			else {
				element.click();
				// //Logger.info("Checkbox is selected!!");
				log.info("Checkbox is selected!!");
				// exe_rst_status = 1;
			}
		}
		// if operation is to unselect
		else if (flag.equalsIgnoreCase("unselect")) {
			// if currently the chechbox is selected then click and unselect and set status
			// as passed
			if (currentFlag == true) {
				element.click();
				// //Logger.info("Checkbox is unselected!!");
				log.info("Checkbox is unselected!!");
				// exe_rst_status = 1;
			}
			// if currently status is unselected then set status as passed and do log entry
			else {
				// //Logger.info("The checkbox "+element+" is already De-Selected");
				log.info("The checkbox is already De-Selected");
				// exe_rst_status = 1;
			}
		}

	}

	// The title of the window is passed as parameter to this function and it loops
	// through all
	// the windows and matches the title provided as parameter and the title of the
	// current window
	// if window is switched successfully sets flag as true otherwise calls function
	// to switch window based
	// on object
	public void handleMultipleWindows(String windowTitle, WebElement element) {
		// log.info("Title in config file - "+ prop.getProperty("windowTitle"));
		// get all the window handles in the array windows
		Set<String> windows = driver.getWindowHandles();
		// this is flag to be set if window is found, default value set to false
		boolean window_ana = false;
		// Looping through the string array windows
		for (String window : windows) {
			// switching to open windows one by one
			driver.switchTo().window(window);
			// if the title passed as parameter matches with the title of the current window
			if (driver.getTitle().equalsIgnoreCase(windowTitle)) {
				// set flag to true
				window_ana = true;
				log.info("Desired window is activated!!");
				// maximize the window
				driver.manage().window().maximize();
				break;
			}
		}
		// if window not found then log warning and call the function to handle window
		// bases on object
		if (window_ana != true) {
			log.warn("Window is not switched, now trying based on element!!");
			// handleMultipleWindowsObjectBased(WebElement element);
			handleMultipleWindowsObjectBased(element);
		}
	}

	public void handleMultipleWindowsObjectBased(WebElement element) {

		// gets the handle of all open windows in a array
		Set<String> windows = driver.getWindowHandles();
		// sets flag to false
		boolean window_ana = false;

		// loop through all windows one by one
		for (String window : windows) {
			// switch to window
			driver.switchTo().window(window);
			log.info("Window title " + driver.getTitle());
			// calls the function that returns the webelement object
			List<WebElement> fieldobjs = driver.findElements((By) element);
			// sets the flag for windows found to true and maximizes the windows and comes
			// out
			if (fieldobjs.size() > 0) {
				log.info("Desired window is activated!!");
				window_ana = true;

				break;
			}
		}
		// switching result-
		if (window_ana != true) {
			log.warn("Window is not switched !!");
		}
	}

	/*
	 * public void waitForPageLoad() throws InterruptedException {
	 * 
	 * JavascriptExecutor executor = (JavascriptExecutor) driver; while
	 * (!executor.executeScript("return document.readyState").toString().equals(
	 * "complete")) { System.out.println("waiting"); Thread.sleep(1000);
	 * 
	 * @SuppressWarnings("deprecation") Wait<WebDriver> wait = new
	 * FluentWait<WebDriver>(driver).withTimeout(600, TimeUnit.SECONDS)
	 * .pollingEvery(3, TimeUnit.SECONDS) .ignoring(NoSuchElementException.class,
	 * StaleElementReferenceException.class);
	 * 
	 * int tries = 0; boolean notfound = false; while (tries < 7) {
	 * System.out.println("Searching for element. Try number " + (tries++)); try {
	 * wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className(
	 * "loadingimg"))); notfound = true; break; } catch (NoSuchElementException e) {
	 * System.out.println("No such element: \n" + e.getMessage() + "\n"); } catch
	 * (UnhandledAlertException e) { System.out.println("Unhandled alert" +
	 * e.getMessage() + "\n"); } catch (NoSuchWindowException e) {
	 * System.out.println("No such element: \n" + e.getMessage() + "\n"); } }
	 * 
	 * if (notfound) { System.out.println("element disappeared after trying for " +
	 * tries + " times"); } else {
	 * System.out.println("element present after trying for " + tries + " times"); }
	 * }
	 */

	public boolean isAlertPresent() {
		boolean presentFlag = false;
		try {
			driver.switchTo().alert();
			// Alert present
			presentFlag = true;
		} catch (NoAlertPresentException ex) {
			// ex.printStackTrace();
			System.out.println("Alert is not present");
		}
		return presentFlag;

	}

	public void staticwait(int time) {
		try {
			System.out.println("Total static wait time " + time);
			for (int i = time; i > 0; i--) {
				Thread.sleep(1000);
				if (i % 5 == 0) {
					System.out.println("System waiting for the element.." + i + " sec to appear!!");
				}
			}
		} catch (Exception e) {
			e.getMessage();
		}
	}

	public void waitForTime(String i) {
		int waitTime = Integer.parseInt(i);
		try {
			Thread.sleep(1000 * waitTime);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void clickJS(WebElement element) {
		waitFortheElement(60, element);

		WebDriverWait wait2 = new WebDriverWait(driver, 30);
		wait2.until(ExpectedConditions.elementToBeClickable(element));

		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);
	}

	public void clickButton(WebElement element) {
		waitFortheElement(120, element);
		WebDriverWait wait2 = new WebDriverWait(driver, 60);
		wait2.until(ExpectedConditions.elementToBeClickable(element));

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

		element.click();

		// //Logger.info("Clicked " + element );
		if (isAlertPresent() == true) {
			try {
				Thread.sleep(3000);

			} catch (InterruptedException e) {

				e.printStackTrace();
			}
			Alert alert = driver.switchTo().alert();
			log.info(alert.getText());
			alert.accept();
			try {
				Thread.sleep(5000l);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	public int genrateRandomNumber() {
		Random r = new Random();
		int Low = 10;
		int High = 10000;
		int Result = r.nextInt(High - Low) + Low;

		return Result;
	}

	public int genrateRandomNumberForAmount() {
		Random r = new Random();
		int Low = 1;
		int High = 10;
		int Result = r.nextInt(High - Low) + Low;

		return Result;
	}

	// thjis functions tries to find the object specified as parameter
	public void waitFortheElement(int waitTime, WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, waitTime);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	
	public void attachFile() {
		
		  WebElement attachfile = driver.findElement(By.xpath("//input[@id='dnn_ctr17770_ClaimsSubmit_ClaimSubmitControlV2_AttachFiles_File1']"));
		  attachfile.sendKeys(System.getProperty("user.dir")+"\\Fixture\\Desert1234.jpg");
//driver.findElement(By.cssSelector("div[class='submitbtnsec'] > button[type='submit']")).click();

	}

	public static String getData(String testvalue,String sheetName,String sourceField,String DestField) throws FilloException
	{
		String data;
		Fillo fillo = new Fillo();
		Connection conn = fillo.getConnection(System.getProperty("user.dir") + "\\Fixture\\TestData.xlsx");
		String str = "select * from "+sheetName;
		Recordset record = conn.executeQuery(str);
		
		while(record.next())
		{
			if(testvalue.equalsIgnoreCase(record.getField(sourceField)))
			{
				data = record.getField(DestField);
				System.out.println(data);
				return data;
			}
		}
		return "Data not found";
	}

	/*
	 * public static void untilJqueryIsDone(WebDriver driver) {
	 * untilJqueryIsDone(driver,
	 * FileReaderManager.getInstance().getConfigReader().getImplicitlyWait()); }
	 * 
	 * public static void untilJqueryIsDone(final WebDriver driver, Long
	 * timeoutInSeconds) { until(driver, new Function<WebDriver, Boolean>() { public
	 * Boolean apply(WebDriver d) { Boolean isJqueryCallDone = (Boolean)
	 * ((JavascriptExecutor) driver) .executeScript("return jQuery.active==0"); if
	 * (!isJqueryCallDone) System.out.println("JQuery call is in Progress"); return
	 * isJqueryCallDone; } }, timeoutInSeconds); }
	 * 
	 * public static void untilPageLoadComplete(WebDriver driver) {
	 * untilPageLoadComplete(driver,
	 * FileReaderManager.getInstance().getConfigReader().getImplicitlyWait()); }
	 * 
	 * public static void untilPageLoadComplete(final WebDriver driver, Long
	 * timeoutInSeconds) { until(driver, new Function<WebDriver, Boolean>() { public
	 * Boolean apply(WebDriver d) { Boolean isPageLoaded = (Boolean)
	 * ((JavascriptExecutor) driver)
	 * .executeScript("return document.readyState").equals("complete"); if
	 * (!isPageLoaded) System.out.println("Document is loading"); return
	 * isPageLoaded; } }, timeoutInSeconds); }
	 * 
	 * public static void until(WebDriver driver, Function<WebDriver, Boolean>
	 * waitCondition) { until(driver, waitCondition,
	 * FileReaderManager.getInstance().getConfigReader().getImplicitlyWait()); }
	 * 
	 * private static void until(WebDriver driver, Function<WebDriver, Boolean>
	 * waitCondition, Long timeoutInSeconds) { WebDriverWait webDriverWait = new
	 * WebDriverWait(driver, timeoutInSeconds);
	 * webDriverWait.withTimeout(timeoutInSeconds, TimeUnit.SECONDS); try {
	 * webDriverWait.until(waitCondition); } catch (Exception e) {
	 * System.out.println(e.getMessage()); } }
	 */

}
