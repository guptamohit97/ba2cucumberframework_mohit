package cucumberUtilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelData extends Base {

	public String path;
	public FileInputStream fis = null;
	private XSSFWorkbook workbook = null;

	public ExcelData(String path) {

		this.path = path;
		try {
			fis = new FileInputStream(path);
			workbook = new XSSFWorkbook(fis);
			fis.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

/*	public static void main(String[] args) throws IOException {

		ExcelData d = new ExcelData("C:\\Users\\mohit-gupta\\Desktop\\Excel_Code.xlsx");

		ArrayList<String> data = d.getData("testdata", "TestCase", "TC1");

		System.out.println(data.get(1));

		System.out.println(data.get(3));
		System.out.println(data.get(4));
	}
*/
	public ArrayList<String> getData(String sheetName, String cloumnName, String testCaseName) throws IOException {

		ArrayList<String> list = new ArrayList<>();

		int sheets = workbook.getNumberOfSheets();

		for (int i = 0; i < sheets; i++) {

			if (workbook.getSheetName(i).equalsIgnoreCase(sheetName)) {

				XSSFSheet sheet = workbook.getSheetAt(i);

				Iterator<Row> row = sheet.iterator();

				Row cell = row.next();

				Iterator<Cell> ce = cell.cellIterator();

				int k = 0;

				int coloumn = 0;

				while (ce.hasNext()) {

					Cell cv = ce.next();

					if (cv.getStringCellValue().equalsIgnoreCase(cloumnName)) {

						coloumn = k;

					}

					k++;
				}

				while (row.hasNext()) {

					Row value = row.next();

					if (value.getCell(coloumn).getStringCellValue().equalsIgnoreCase(testCaseName)) {

						Iterator<Cell> c1 = value.cellIterator();

						while (c1.hasNext()) {

							Cell c = c1.next();

							if (c.getCellType() == CellType.STRING) {

								list.add(c.getStringCellValue());

							} else if (c.getCellType() == CellType.NUMERIC || c.getCellType() == CellType.FORMULA) {

								if (DateUtil.isCellDateFormatted(c)) {
									DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
									Date date = c.getDateCellValue();
									String date1 = df.format(date);
									list.add(date1);
								} else {
									list.add(NumberToTextConverter.toText(c.getNumericCellValue()));
								}

							}
						}

					}

				}
			}

		} 
		return list;

	}

}
