package cucumberUtilities;

import java.util.ArrayList;
import java.util.List;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;

public class Xls_Reader_Fillo {

	public String path;

	public Connection connection;

	public Xls_Reader_Fillo(String path) {
		this.path = path;
		Fillo fillo = new Fillo();
		connection = null;
		try {
			connection = fillo.getConnection(path);
		} catch (FilloException e) {
			e.printStackTrace();
		}
	}

	/*
	 * public static void main(String args[]) {
	 * 
	 * String valueString =
	 * getCellData("C:\\BA2_Cucumber_Automation\\Fixture\\TestData.xlsx");
	 * 
	 * System.out.println(valueString); }
	 */

	public String getCellData(String strQuery, String columnName) {

		List<String> data = new ArrayList<>();
		Recordset recordset = null;
		try {
			recordset = connection.executeQuery(strQuery);
		} catch (FilloException e) {
			e.printStackTrace();
		}

		try {
			while (recordset.next()) {
				System.out.println(recordset.getField(columnName));
				System.out.println(recordset.getCount());
				strQuery = recordset.getField(columnName);
				data.add(recordset.getField(columnName));
			}
		} catch (FilloException e) {
			e.printStackTrace();
		}

		try {
			strQuery = recordset.getField(columnName).toString();

		} catch (FilloException e) {
			e.printStackTrace();
		}

		recordset.close();
		connection.close();
		return strQuery;
	}

}
