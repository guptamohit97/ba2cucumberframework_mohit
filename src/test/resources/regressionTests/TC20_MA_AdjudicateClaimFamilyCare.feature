Feature: verify Adjudicate functionality for claim type as Family Care & Well-Being claim for  BA2 MA application 

@RegressionTest
Scenario: verify Adjudicate  functionality for claim type as Family Care & Well-Being claim for  BA2 MA site
Given user login into the application
When user click claim link on MA page
And user click search a claim link for Family
And user enter value for claiment name and process Adjudication for Family
|TestCase|
|TC1|
|TC2|
|TC3|
Then user logout from the application