Feature: Verify Employee profile report functionality from MA portal

@RegressionTest
Scenario: Verify Employee profile report functionality from MA portal
Given user login into the application
When user click on the Employee profile report under Report Stat
And user click MA employee search button
Then verify report header as Employee Profile Report
Then user logout from the application

@RegressionTest
Scenario: Verify Dependent Profile Report functionality from MA portal
Given user login into the application
When user click on the Dependent Profile Report under Report Stat
And user click MA dependent search button
Then verify report header as Dependent Profile Report
Then user logout from the application

@RegressionTest
Scenario: Verify Panel Claim Listing Report functionality from MA portal
Given user login into the application
When user click on the Panel Claim Listing Report under Report Stat
And user click Generate button
Then verify report header as Panel Claim Listing Report
Then user logout from the application

@RegressionTest
Scenario: Verify Flex Points Allocation Report functionality from MA portal
Given user login into the application
When user click on the Flex Point Detailed Report under Report Stat DSOSG
And user click flex point search button
Then verify report header as Flex Point Allocation Report
Then user logout from the application

@RegressionTest
Scenario: Verify Benefit Selection Report functionality from MA portal
Given user login into the application
When user click on the Benefit Selection Report under Report Stat
And user click Benefit Selection search button
Then verify report header as Benefit Selection Report
Then user logout from the application
