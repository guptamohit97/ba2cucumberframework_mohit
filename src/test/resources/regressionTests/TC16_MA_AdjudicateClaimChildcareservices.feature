Feature: verify Adjudicate functionality for claim type as Childcare services including infant care services (licensed centers only) claim for  BA2 MA application 

@RegressionTest
Scenario: verify Adjudicate  functionality for claim type as Childcare services including infant care services (licensed centers only) claim for  BA2 MA site
Given user login into the application
When user click claim link on MA page
And user click search a claim link for childcare
And user enter value for claiment name and process Adjudication for childcare
|TestCase|
|TC1|
|TC2|
|TC3|
Then user logout from the application