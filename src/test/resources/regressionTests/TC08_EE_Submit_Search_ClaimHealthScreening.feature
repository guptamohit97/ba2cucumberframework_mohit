Feature: Verify claimsubmission  functionality for claim type as Health Screening & Vaccination claim in BA2 application

@RegressionTest
Scenario: Verify claim submit functionality for claim type as Health Screening & Vaccination claim from EE site
Given user login into EE application
|LoginUserName|
|Employee_Health|
When user select create new claim link for health claim
And user select claimant name dropdown on EE claim screen for health claim
|TestCase|
|TC1|
|TC2|
|TC3|
Then user logout from the application