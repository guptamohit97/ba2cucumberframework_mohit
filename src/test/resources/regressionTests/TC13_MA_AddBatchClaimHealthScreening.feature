Feature: Verify claim add to batch functionality for claim type as Health Screening & Vaccination claim in BA2 application


@RegressionTest
Scenario: Verify create claim batch functionality from application
Given user login into the application
When user add claim to batch for health claims
|TestCase|
|TC1|
|TC2|
|TC3|
Then user logout from the application