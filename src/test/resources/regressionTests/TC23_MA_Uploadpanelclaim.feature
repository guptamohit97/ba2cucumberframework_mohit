Feature: Verify Panel claim upload functionality for BA2 application

@RegressionTest
Scenario: Verify panel claim CSV upload functionality
Given user login into the application
When user click on the panel claims Upload button
Then user upload the panel claim csv 
And user click on the start upload button
Then user click on the submit button
Then user logout from the application

@RegressionTest
Scenario: Verify payment functionality from MA application
Given user login into the application
When user mose over payment link
And user enter salary deduction date
Then user select generate button
And user select close button on pop up screen
And verify amount generated for upload claim
Then user select check box for payment
And user click confirm button
And user click ok button for payment
And verify payment confirmation is dislayed on popup screen
Then user logout from the application
