Feature: Verify Search Employee functionality from HR portal


@RegressionTest
Scenario: Verify search Employee and dependent details functionality from HR portal
Given user login into EE application
|LoginUserName|
|Employee_HR|
And user click enter button under admin section
When user click search employee tab
And user select employee id radio button
And user enter employee_id in search text box
And user click search button to search employee
And user click employee link
Then verify header for Employee Detail page      
When user click dependent link
Then verify verify header for Dependent detail page
When user click stataement of account link
Then verify header for stetement of account page
Then user logout from the application

@RegressionTest
Scenario: verify search SOE functionality from HR portal
Given user login into EE application
|LoginUserName|
|Employee_HR|
And user click enter button under admin section
When user click start enrollment tab
And user enter employee_id in enrolment search text box
And user click search button to search SOE
Then verify employees SOE header as employee
Then user logout from the application



Scenario: verify searh claim functionality from employee HR portal
Given user login into EE application
|LoginUserName|
|Employee_HR|
And user click enter button under admin section
When user click claims tab
And user enter employee_id in claim search text box
And user click on search button on search claim page
Then verify claim tables header is displayed as Transaction Code
Then user logout from the application

