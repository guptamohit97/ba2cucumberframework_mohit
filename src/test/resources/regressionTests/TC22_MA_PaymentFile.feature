Feature: Verify payment functionality for claim

@RegressionTest
Scenario: Verify payment functionality from MA application
Given user login into the application
When user mose over payment link
And user enter salary deduction date
Then user select generate button
And user select close button on pop up screen
Then user select check box for payment
And user click confirm button
And user click ok button for payment
And verify payment confirmation is dislayed on popup screen
Then user logout from the application