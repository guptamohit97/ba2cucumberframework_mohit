Feature: Verify claimsubmission  functionality for claim type as Dental expenses claim in BA2 application

@RegressionTest
Scenario: Verify claim submit functionality for claim type as Dental expenses claim from EE site
Given user login into EE application
|LoginUserName|
|Employee_Dental|
When user select create new claim link for dental claim
And user select claimant name dropdown on EE claim screen for dental claim
|TestCase|
|TC1|
|TC2|
|TC3|
Then user logout from the application