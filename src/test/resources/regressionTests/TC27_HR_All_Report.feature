Feature: verify Employee profile report functionality from HR portal

@RegressionTest
Scenario: verify Employee profile report details functionality from HR portal
Given user login into EE application
|LoginUserName|
|Employee_HR|
And user click enter button under admin section
When user click reports tab
And user click employee profile report link
And user click employee status as Active
And user click employee search button
Then verify employee report header as Employee Profile Report
Then user logout from the application


@RegressionTest
Scenario: verify Dependent profile report details functionality from HR portal
Given user login into EE application
|LoginUserName|
|Employee_HR|
And user click enter button under admin section
When user click reports tab
And user click dependent profile report link
And user click dependent status as Active
And user click dependent search button
Then verify  dependent report header as Dependent Profile Report
Then user logout from the application


@RegressionTest
Scenario: verify Flex Balance & Usage Report details functionality from HR portal
Given user login into EE application
|LoginUserName|
|Employee_HR|
And user click enter button under admin section
When user click reports tab
And user click dsosg flex point detailed report link
And user click employee flex status as Active
And user click flex report search button
Then verify report header as Flex Balance & Usage Report
Then user logout from the application