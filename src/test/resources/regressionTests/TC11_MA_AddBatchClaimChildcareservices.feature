Feature: Verify claim add to batch functionality for claim type as Childcare services including infant care services (licensed centers only) claim in BA2 application

@RegressionTest
Scenario: Verify create claim batch functionality from application
Given user login into the application
When user add claim to batch for childcare claims
|TestCase|
|TC1|
|TC2|
|TC3|
Then user logout from the application 