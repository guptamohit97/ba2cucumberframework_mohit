Feature: Verify claim review functionality in MA application

@RegressionTest
Scenario: Verify claim review functionality for MA site
Given user login into the application
When user click claims link
And user click on Mass Review Claims and Exception Processing link
And user click review all button
And user click review all link
And user click on schedular close button
Then user logout from the application