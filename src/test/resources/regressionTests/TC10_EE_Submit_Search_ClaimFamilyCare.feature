Feature: Verify claimsubmission  functionality for claim type as Family Care & Well-Being claim in BA2 application

@RegressionTest
Scenario: Verify claim submit functionality for claim type as Family Care & Well-Being claim from EE site
Given user login into EE application
|LoginUserName|
|Employee_Family|
When user select create new claim link for familycare claim
And user select claimant name dropdown on EE claim screen for familycare claim
|TestCase|
|TC1|
|TC2|
|TC3|
Then user logout from the application