Feature: Verify claimsubmission  functionality for claim type as Medical Expenses (FSA) claim in BA2 application

@RegressionTest
Scenario: Verify claim submit functionality for claim type as Childcare services including infant care services (licensed centers only) claim from EE site
Given user login into EE application
|LoginUserName|
|Employee_Medical|
When user select create new claim link for medical claim
And user select claimant name dropdown on EE claim screen for medical claim
|TestCase|
|TC1|
|TC2|
|TC3|
Then user logout from the application