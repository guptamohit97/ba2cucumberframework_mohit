
Feature: Initialize employee for claim submission from MA BA2 application
 
@RegressionTest
Scenario: Initialize employee for claim submission from MA BA2 application
Given user login into the application
When user click claim link on MA page
Then user click Mass Claims Initialization link
And user select choose plan year
|TestCase|
|TC1|
|TC2|
|TC3|
|TC4|
|TC5|
Then user logout from the application  
 
    

 