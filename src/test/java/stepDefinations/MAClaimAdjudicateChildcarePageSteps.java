package stepDefinations;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import java.util.List;
import java.util.Map;
import com.codoid.products.exception.FilloException;
import cucumberUtilities.TestContext;
import io.cucumber.java.en.Then;
import pageObjects.MAClaimAdjudicateChildcarePage;


@SuppressWarnings("deprecation")
public class MAClaimAdjudicateChildcarePageSteps {
	
	MAClaimAdjudicateChildcarePage mAClaimAdjudicateChildcarePage;
	TestContext testContext;
	

	public MAClaimAdjudicateChildcarePageSteps(TestContext context) {
		testContext = context;
		mAClaimAdjudicateChildcarePage = testContext.getPageObjectManager().getEEClaimAdjudicatePage();
	}
	
	
	@When("user click search a claim link for childcare")
	public void user_click_search_a_claim_link_for_childcare() {
		mAClaimAdjudicateChildcarePage.clickSearchAClaimLink();
	}

	@When("user enter value for claiment name and process Adjudication for childcare")
	public void user_enter_value_for_claiment_name_and_process_Adjudication_for_childcare(DataTable dt) throws FilloException {
		
		List<Map<String, String>> list = dt.asMaps();
		for(Map<String, String> form : list)
		{
			String TestCase = form.get("TestCase");
			
		mAClaimAdjudicateChildcarePage.enterSearchAClaimTextBox(TestCase);
		mAClaimAdjudicateChildcarePage.clickSearchAClaimButton();
		mAClaimAdjudicateChildcarePage.clickClaimAdjudicateButton();
		mAClaimAdjudicateChildcarePage.selectClaimStatusSelectionDropdown(TestCase);
		mAClaimAdjudicateChildcarePage.clickClaimAdjudicateProcessButton();
		mAClaimAdjudicateChildcarePage.clickClaimCloseButton();
		
	}
	}

	/*@Then("user click search claim button for Adjudicate")
	public void user_click_search_claim_button_for_Adjudicate() {
		mAClaimAdjudicateChildcarePage.clickSearchAClaimButton();
	}

	@Then("user click adjudicate button")
	public void user_click_adjudicate_button() {
		mAClaimAdjudicateChildcarePage.clickClaimAdjudicateButton();
	}

	@Then("user click claim status as status from dropdown")
	public void user_click_claim_status_as_status_from_dropdown() {
		mAClaimAdjudicateChildcarePage.selectClaimStatusSelectionDropdown();
	}

	@Then("user click process button")
	public void user_click_process_button() {
		mAClaimAdjudicateChildcarePage.clickClaimAdjudicateProcessButton();
	}

	@Then("verify status Text successfully has been displayed")
	public void verify_status_Text_successfully_has_been_displayed() {
		mAClaimAdjudicateChildcarePage.verifyClaimAdjudicateSuccessfullyMessage();
	}*/


	
}