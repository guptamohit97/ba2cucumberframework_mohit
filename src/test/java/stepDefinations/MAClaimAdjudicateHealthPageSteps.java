package stepDefinations;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import java.util.List;
import java.util.Map;
import com.codoid.products.exception.FilloException;
import cucumberUtilities.TestContext;
import io.cucumber.java.en.Then;
import pageObjects.MAClaimAdjudicateHealthPage;


public class MAClaimAdjudicateHealthPageSteps {

	MAClaimAdjudicateHealthPage mAClaimAdjudicateHealthPage;
	TestContext testContext;
	

	public MAClaimAdjudicateHealthPageSteps(TestContext context) {
		testContext = context;
		mAClaimAdjudicateHealthPage = testContext.getPageObjectManager().getEEClaimAdjudicateHealthPage();
	}
	
	
	@When("user click search a claim link for Health")
	public void user_click_search_a_claim_link_for_Health() {
		mAClaimAdjudicateHealthPage.clickSearchAClaimLink();
	}

	@When("user enter value for claiment name and process Adjudication for Health")
	public void user_enter_value_for_claiment_name_and_process_Adjudication_for_Health(DataTable dt) throws FilloException {
		
		List<Map<String, String>> list = dt.asMaps();
		for(Map<String, String> form : list)
		{
			String TestCase = form.get("TestCase");
			
			mAClaimAdjudicateHealthPage.enterSearchAClaimTextBox(TestCase);
			mAClaimAdjudicateHealthPage.clickSearchAClaimButton();
			mAClaimAdjudicateHealthPage.clickClaimAdjudicateButton();
			mAClaimAdjudicateHealthPage.selectClaimStatusSelectionDropdown(TestCase);
			mAClaimAdjudicateHealthPage.clickClaimAdjudicateProcessButton();
			mAClaimAdjudicateHealthPage.clickClaimCloseButton();
		
	}
	}

	
	
}
