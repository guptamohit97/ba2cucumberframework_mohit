package stepDefinations;

import java.util.List;
import java.util.Map;

import com.codoid.products.exception.FilloException;

import cucumberUtilities.TestContext;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import pageObjects.EESubmitClaimHealthScreeningPage;

@SuppressWarnings("deprecation")
public class EESubmitHealthScreeningClaimPageSteps {

	EESubmitClaimHealthScreeningPage eESubmitClaimHealthScreeningPage;
	TestContext testContext;

	public EESubmitHealthScreeningClaimPageSteps(TestContext context) {
		testContext = context;
		eESubmitClaimHealthScreeningPage = testContext.getPageObjectManager().getEeSubmitClaimHealthScreeningPage();
	}

	/*
	 * @Given("user click enter button under employee section for health claim")
	 * public void user_click_enter_button_under_employee_section_for_health_claim()
	 * { eESubmitClaimHealthScreeningPage.clickHealthEeEmployeeButton(); }
	 */

	@When("user select create new claim link for health claim")
	public void user_select_create_new_claim_link_for_health_claim() {
		eESubmitClaimHealthScreeningPage.clickHealthCreateClaimLink();

	}

	@When("user select claimant name dropdown on EE claim screen for health claim")
	public void user_select_claimant_name_dropdown_on_EE_claim_screen_for_health_claim(DataTable dt)
			throws InterruptedException, FilloException {
		// Write code here that turns the phrase above into concrete actions

		List<Map<String, String>> list = dt.asMaps();
		for (Map<String, String> form : list) 
		{
			String TestCase = form.get("TestCase");

			eESubmitClaimHealthScreeningPage.selectHealthClaimantNameDropDown(TestCase);
			eESubmitClaimHealthScreeningPage.selectHealthClaimTypeDropdown(TestCase);
			eESubmitClaimHealthScreeningPage.enterHealthClinicProviderName(TestCase);
			eESubmitClaimHealthScreeningPage.enterHealthReceiptDate(TestCase);
			eESubmitClaimHealthScreeningPage.enterHealthReceiptNo(TestCase);
			eESubmitClaimHealthScreeningPage.enterHealthReceiptAmount(TestCase);
			eESubmitClaimHealthScreeningPage.clickHealthAddAttachment();
			eESubmitClaimHealthScreeningPage.clickHealthReadyToSubmitButton();
			eESubmitClaimHealthScreeningPage.clickHealthConfirmSubmitButton();
			eESubmitClaimHealthScreeningPage.clickHealthConfirmButton();
			Thread.sleep(10000);
			eESubmitClaimHealthScreeningPage.clickCreateNewClaim();
		}
	}

	/*
	 * @When("user select claim type from dropdown list for health claim") public
	 * void user_select_claim_type_from_dropdown_list_for_health_claim() {
	 * eESubmitClaimHealthScreeningPage.selectHealthClaimTypeDropdown(); }
	 * 
	 * @When("user enter value for clinic provider name textbox for health claim")
	 * public void
	 * user_enter_value_for_clinic_provider_name_textbox_for_health_claim() {
	 * eESubmitClaimHealthScreeningPage.enterHealthClinicProviderName(); }
	 * 
	 * @When("user enter value for receipt date textbox for health claim") public
	 * void user_enter_value_for_receipt_date_textbox_for_health_claim() {
	 * eESubmitClaimHealthScreeningPage.enterHealthReceiptDate(); }
	 * 
	 * @When("user enter value for receipt no textbox for health claim") public void
	 * user_enter_value_for_receipt_no_textbox_for_health_claim() {
	 * eESubmitClaimHealthScreeningPage.enterHealthReceiptNo(); }
	 * 
	 * @When("user enter value for receipt amount textbox for health claim") public
	 * void user_enter_value_for_receipt_amount_textbox_for_health_claim() {
	 * eESubmitClaimHealthScreeningPage.enterHealthReceiptAmount(); }
	 * 
	 * @When("user upload document through attach scanned receipt link for health claim"
	 * ) public void
	 * user_upload_document_through_attach_scanned_receipt_link_for_health_claim() {
	 * 
	 * eESubmitClaimHealthScreeningPage.clickHealthAddAttachment(); }
	 * 
	 * @When("user select ready to submit button for health claim") public void
	 * user_select_ready_to_submit_button_for_health_claim() {
	 * 
	 * eESubmitClaimHealthScreeningPage.clickHealthReadyToSubmitButton(); }
	 * 
	 * @When("user select confirm claims button for health claim") public void
	 * user_select_confirm_claims_button_for_health_claim() {
	 * eESubmitClaimHealthScreeningPage.clickHealthConfirmSubmitButton(); }
	 * 
	 * @When("user select ok button for health claim") public void
	 * user_select_ok_button_for_health_claim() {
	 * eESubmitClaimHealthScreeningPage.clickHealthConfirmButton(); }
	 * 
	 * @Then("user click search claim button for health claim") public void
	 * user_click_search_claim_button_for_health_claim() {
	 * eESubmitClaimHealthScreeningPage.clickHealthSearchButton(); }
	 */

}
