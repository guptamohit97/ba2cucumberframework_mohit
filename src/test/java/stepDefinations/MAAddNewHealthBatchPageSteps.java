package stepDefinations;


import java.util.List;
import java.util.Map;

import com.codoid.products.exception.FilloException;

import cucumberUtilities.TestContext;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.MAAddNewHealthBatchPage;

public class MAAddNewHealthBatchPageSteps {

	MAAddNewHealthBatchPage mAAddNewHealthBatchPage;
	TestContext testContext;
	

	public MAAddNewHealthBatchPageSteps(TestContext context) {
		testContext = context;
		mAAddNewHealthBatchPage = testContext.getPageObjectManager().getMaAddNewHealthBatchPage();
	}
	
	@When("user add claim to batch for health claims")
	public void user_add_claim_to_batch_for_health_claims(DataTable dt) throws FilloException {
		
		List<Map<String, String>> list = dt.asMaps();
		for(Map<String, String> form : list)
		{
			String TestCase = form.get("TestCase");
			mAAddNewHealthBatchPage.clickClaimLink();
			mAAddNewHealthBatchPage.clickSearchAClaimLink();
			mAAddNewHealthBatchPage.enterSearchAClaimTextBox(TestCase);
			mAAddNewHealthBatchPage.clickSearchAClaimButton();
			mAAddNewHealthBatchPage.clickAddToBatchButton();
			mAAddNewHealthBatchPage.clickAddBatchNewButton();
			mAAddNewHealthBatchPage.enterNewBatchNameTextBox(TestCase);
			mAAddNewHealthBatchPage.clickBatchSaveButton();
			mAAddNewHealthBatchPage.clickAddDocumentToBatchLink();
			mAAddNewHealthBatchPage.clickAddTrancationCodeToBatchButton();
			mAAddNewHealthBatchPage.clickClaimLink();
	}
	}


	
	
}
