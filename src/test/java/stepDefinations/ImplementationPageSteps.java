package stepDefinations;

import cucumberUtilities.TestContext;
import io.cucumber.java.en.Then;
import pageObjects.ImplementationPage;

public class ImplementationPageSteps {

	ImplementationPage implementationPage;
	TestContext testContext;

	public ImplementationPageSteps(TestContext context) {
		testContext = context;
		implementationPage = testContext.getPageObjectManager().getImplementationPage();
	}

	@Then("user click on the Initialization link under implementation")
	public void user_click_on_the_Initialization_link_under_implementation() {
		implementationPage.clickInitializationButton();
	}

	@Then("user click on the upload employee data button")
	public void user_click_on_the_upload_employee_data_button() {
		implementationPage.clickUploadEmployeeDataButton();
	}

	@Then("user click on the upload Dependent data button")
	public void user_click_on_the_upload_Dependent_data_button() {
		implementationPage.clickUploadDepDataButton();
	}

}
