package stepDefinations;

import java.util.List;
import java.util.Map;

import cucumberUtilities.TestContext;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.HREmployeeReportPage;
import pageObjects.LoginEEPage;
import pageObjects.HRPage;
//import pageObjects.HrReportPage;
import pageObjects.PortalConfigurationsPage;

@SuppressWarnings("deprecation")
public class LoginEEPageSteps {

	
	
	LoginEEPage loginEEPage;
	TestContext testContext;

	public LoginEEPageSteps(TestContext context) {
		testContext = context;
		loginEEPage = testContext.getPageObjectManager().getLoginEePage();
	}

	@Given("user login into EE application")
	public void user_login_into_EE_application(DataTable dt) {
		
		List<Map<String, String>> list = dt.asMaps();
		for (Map<String, String> form : list) 
		{
			String loginUserName = form.get("LoginUserName");
			
		loginEEPage.enterUserNameTextBox(loginUserName);
		loginEEPage.enterPasswordTextBox();
		loginEEPage.clickLoginButton();
		}
	}
	
}
