package stepDefinations;

import java.util.List;
import java.util.Map;

import com.codoid.products.exception.FilloException;

import cucumberUtilities.TestContext;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.MAAddNewDentalBatchPage;

@SuppressWarnings("deprecation")
public class MAAddNewDentalBatchPageSteps {

	MAAddNewDentalBatchPage mAAddNewDentalBatchPage;
	TestContext testContext;
	

	public MAAddNewDentalBatchPageSteps(TestContext context) {
		testContext = context;
		mAAddNewDentalBatchPage = testContext.getPageObjectManager().getMaAddNewDentalBatchPage();
	}
	
	@When("user add claim to batch for dental claims")
	public void user_add_claim_to_batch_for_dental_claims(DataTable dt) throws FilloException {
		
		List<Map<String, String>> list = dt.asMaps();
		for(Map<String, String> form : list)
		{
			String TestCase = form.get("TestCase");
			mAAddNewDentalBatchPage.clickClaimLink();
			mAAddNewDentalBatchPage.clickSearchAClaimLink();
			mAAddNewDentalBatchPage.enterSearchAClaimTextBox(TestCase);
			mAAddNewDentalBatchPage.clickSearchAClaimButton();
			mAAddNewDentalBatchPage.clickAddToBatchButton();
			mAAddNewDentalBatchPage.clickAddBatchNewButton();
			mAAddNewDentalBatchPage.enterNewBatchNameTextBox(TestCase);
			mAAddNewDentalBatchPage.clickBatchSaveButton();
			mAAddNewDentalBatchPage.clickAddDocumentToBatchLink();
			mAAddNewDentalBatchPage.clickAddTrancationCodeToBatchButton();
			mAAddNewDentalBatchPage.clickClaimLink();
	}
	}


	
}
