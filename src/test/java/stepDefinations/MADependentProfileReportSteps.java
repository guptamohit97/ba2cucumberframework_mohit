package stepDefinations;

import cucumberUtilities.TestContext;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.MADependentProfileReportPage;

public class MADependentProfileReportSteps {

	MADependentProfileReportPage mADependentProfileReportPage;
	TestContext testContext;

	public MADependentProfileReportSteps(TestContext context) {
		testContext = context;
		mADependentProfileReportPage = testContext.getPageObjectManager().getMaDependentReportsPage();
	}

	@When("user click on the Dependent Profile Report under Report Stat")
	public void user_click_on_the_Dependent_Profile_Report_under_Report_Stat() {
		mADependentProfileReportPage.clickDependentProfileReport();
	}

	@When("user click MA dependent search button")
	public void user_click_MA_dependent_search_button() {
		mADependentProfileReportPage.clickDependentSearchButton();
	}

	@Then("verify report header as Dependent Profile Report")
	public void verify_report_header_as_Dependent_Profile_Report() {
		mADependentProfileReportPage.validateDependentProfileReportName();
	}

}
