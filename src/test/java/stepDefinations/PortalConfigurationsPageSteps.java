package stepDefinations;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.codoid.products.exception.FilloException;

import cucumberUtilities.TestContext;
import io.cucumber.core.api.Scenario;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.PortalConfigurationsPage;

public class PortalConfigurationsPageSteps {

	PortalConfigurationsPage portalConfigurationsPage;
	TestContext testContext;

	public PortalConfigurationsPageSteps(TestContext context) {
		testContext = context;
		portalConfigurationsPage = testContext.getPageObjectManager().getPortalConfigurationsPage();

	}

	@When("user naviagte to portal configuration page")
	public void user_naviagte_to_portal_configuration_page() {
		portalConfigurationsPage.clickSecurityAccess();
		portalConfigurationsPage.clickPortalConfigurationLink();

	}

	@When("user change the password for the employee")
	public void user_change_the_password_for_the_employee(DataTable dt) throws IOException, FilloException {
		portalConfigurationsPage.selectPortalSelectionDropDown();
		
		List<Map<String, String>> list = dt.asMaps();
		for(Map<String, String> form : list)
		{
			String TestCase = form.get("TestCase");
			portalConfigurationsPage.EnterClientLoginUserNameTextBox(TestCase);
			portalConfigurationsPage.clickGetUserInfoButton();
			portalConfigurationsPage.EnterChangePasswordTextBox();
			portalConfigurationsPage.clickResetPasswordButton();
			portalConfigurationsPage.clickSecurityAccess();
			portalConfigurationsPage.clickPortalConfigurationLink();
			portalConfigurationsPage.selectPortalSelectionDropDown();
		}
		
		
		
	}

	@Given("user login into the application as an employee")
	public void user_login_into_the_application_as_an_employee() {
		//portalConfigurationsPage.getApplicationUrl("EE");
	}

	@Then("user enter user name and pasword for the employee")
	public void user_enter_user_name_and_pasword_for_the_employee(DataTable dt) throws FilloException {
		
		List<Map<String, String>> list = dt.asMaps();
		for(Map<String, String> form : list)
		{
			String TestCase = form.get("TestCase");
			portalConfigurationsPage.enterUserNameTextBox(TestCase);
			portalConfigurationsPage.enterPasswordTextBox();
			portalConfigurationsPage.clickLoginButton();
			portalConfigurationsPage.clickIAgreeButton();
			portalConfigurationsPage.enterCurrentPasswordTextBox();
			portalConfigurationsPage.enterNewPasswordTextBox();
			portalConfigurationsPage.enterConfirmPasswordTextBox();
			portalConfigurationsPage.clickUpdatePasswordButton();
			portalConfigurationsPage.clickUpdatePasswordLogoutButton();
		}
		
		
		
	}

	/*@Then("user change the default password")
	public void user_change_the_default_password() {
		portalConfigurationsPage.enterCurrentPasswordTextBox();
		portalConfigurationsPage.enterNewPasswordTextBox();
		portalConfigurationsPage.enterConfirmPasswordTextBox();
		portalConfigurationsPage.clickUpdatePasswordButton();
	}*/

	/*@Then("user logout from the employee")
	public void user_logout_from_the_employee() {
		portalConfigurationsPage.clickLogoutLink();

	}*/

}
