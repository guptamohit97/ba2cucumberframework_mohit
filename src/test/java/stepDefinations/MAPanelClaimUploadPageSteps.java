package stepDefinations;

import cucumberUtilities.TestContext;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.MAPanelClaimUploadPage;

public class MAPanelClaimUploadPageSteps {

	MAPanelClaimUploadPage mAPanelClaimUploadPage;
	TestContext testContext;
	

	public MAPanelClaimUploadPageSteps(TestContext context) {
		testContext = context;
		mAPanelClaimUploadPage = testContext.getPageObjectManager().getMAPanelClaimUploadPage();
	}
	
	@When("user click on the panel claims Upload button")
	public void user_click_on_the_panel_claims_Upload_button() {
		mAPanelClaimUploadPage.clickUploadPanelClaimButton();
	}

	@Then("user upload the panel claim csv")
	public void user_upload_the_panel_claim_csv() {
		mAPanelClaimUploadPage.clickUploadPanelClaimButton();
		mAPanelClaimUploadPage.uploadPanelClaimCsv();
	}

	@Then("user click on the start upload button")
	public void user_click_on_the_start_upload_button() {
		mAPanelClaimUploadPage.clickStartPanelUploadButton();
	}

	@Then("user click on the submit button")
	public void user_click_on_the_submit_button() {
		mAPanelClaimUploadPage.verifyPanelClaimSucessfulMessage();
		mAPanelClaimUploadPage.clickPopupPanelUploadCloseButton();
	}

	@Then("verify amount generated for upload claim")
	public void verify_amount_generated_for_upload_claim() {
		mAPanelClaimUploadPage.verifyGeneratePaymentAmount();
	}

	
}
