package stepDefinations;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import java.util.List;
import java.util.Map;
import com.codoid.products.exception.FilloException;
import cucumberUtilities.TestContext;
import io.cucumber.java.en.Then;
import pageObjects.MAClaimAdjudicateFamilyPage;


@SuppressWarnings("deprecation")

public class MAClaimAdjudicateFamilyPageSteps {

	MAClaimAdjudicateFamilyPage mAClaimAdjudicateFamilyPage;
	TestContext testContext;
	

	public MAClaimAdjudicateFamilyPageSteps(TestContext context) {
		testContext = context;
		mAClaimAdjudicateFamilyPage = testContext.getPageObjectManager().getEEClaimAdjudicateFamilyPage();
	}
	
	
	@When("user click search a claim link for Family")
	public void user_click_search_a_claim_link_for_Family() {
		mAClaimAdjudicateFamilyPage.clickSearchAClaimLink();
	}

	@When("user enter value for claiment name and process Adjudication for Family")
	public void user_enter_value_for_claiment_name_and_process_Adjudication_for_Family(DataTable dt) throws FilloException {
		
		List<Map<String, String>> list = dt.asMaps();
		for(Map<String, String> form : list)
		{
			String TestCase = form.get("TestCase");
			
			mAClaimAdjudicateFamilyPage.enterSearchAClaimTextBox(TestCase);
			mAClaimAdjudicateFamilyPage.clickSearchAClaimButton();
			mAClaimAdjudicateFamilyPage.clickClaimAdjudicateButton();
			mAClaimAdjudicateFamilyPage.selectClaimStatusSelectionDropdown(TestCase);
			mAClaimAdjudicateFamilyPage.clickClaimAdjudicateProcessButton();
			mAClaimAdjudicateFamilyPage.clickClaimCloseButton();
		
	}
	}

	
	
}
