package stepDefinations;


import java.util.List;
import java.util.Map;

import com.codoid.products.exception.FilloException;

import cucumberUtilities.TestContext;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.MAAddNewChildcareBatchPage;
//import pageObjects.MaClaimInitializationPage;


@SuppressWarnings("deprecation")
public class MAAddNewChildcareBatchPageSteps {
	
	MAAddNewChildcareBatchPage mAAddNewChildcareBatchPage;
	TestContext testContext;
	

	public MAAddNewChildcareBatchPageSteps(TestContext context) {
		testContext = context;
		mAAddNewChildcareBatchPage = testContext.getPageObjectManager().getMaAddNewCommonBatchPage();
	}
	
	@When("user add claim to batch for childcare claims")
	public void user_add_claim_to_batch_for_childcare_claims(DataTable dt) throws FilloException {
		
		List<Map<String, String>> list = dt.asMaps();
		for(Map<String, String> form : list)
		{
			String TestCase = form.get("TestCase");
		mAAddNewChildcareBatchPage.clickClaimLink();
		mAAddNewChildcareBatchPage.clickSearchAClaimLink();
		mAAddNewChildcareBatchPage.enterSearchAClaimTextBox(TestCase);
		mAAddNewChildcareBatchPage.clickSearchAClaimButton();
		mAAddNewChildcareBatchPage.clickAddToBatchButton();
		mAAddNewChildcareBatchPage.clickAddBatchNewButton();
		mAAddNewChildcareBatchPage.enterNewBatchNameTextBox(TestCase);
		mAAddNewChildcareBatchPage.clickBatchSaveButton();
		mAAddNewChildcareBatchPage.clickAddDocumentToBatchLink();
		mAAddNewChildcareBatchPage.clickAddTrancationCodeToBatchButton();
		mAAddNewChildcareBatchPage.clickClaimLink();
	}
	}

	/*@When("user enter value for claiment name in search claim text box")
	public void user_enter_value_for_claiment_name_in_search_claim_text_box() {
		mAAddNewChildcareBatchPage.enterSearchAClaimTextBox();
	}

	@Then("user click search claim button")
	public void user_click_search_claim_button() {
		mAAddNewChildcareBatchPage.clickSearchAClaimButton();
	}

	@Then("user click add to batch button")
	public void user_click_add_to_batch_button() {
		mAAddNewChildcareBatchPage.clickAddToBatchButton();
	}

	@Then("user click new batch button")
	public void user_click_new_batch_button() {
		mAAddNewChildcareBatchPage.clickAddBatchNewButton();
	}

	@Then("user enter value for batch name text box")
	public void user_enter_value_for_batch_name_text_box() {
		mAAddNewChildcareBatchPage.enterNewBatchNameTextBox();
	}

	@Then("user click save batch button")
	public void user_click_save_batch_button() {
		mAAddNewChildcareBatchPage.clickBatchSaveButton();
	}

	@Then("user select add document to batch link")
	public void user_select_add_document_to_batch_link() {
		mAAddNewChildcareBatchPage.clickAddDocumentToBatchLink();
	}

	@Then("user select add button to add claim")
	public void user_select_add_button_to_add_claim() {
		mAAddNewChildcareBatchPage.clickAddTrancationCodeToBatchButton();
	}

    @Then("verify name for new batch created")
	public void verify_name_for_new_batch_created() {
		mAAddNewChildcareBatchPage.verifyClaimIsAddedToBatch();
	}
*/

}