package stepDefinations;


import java.util.List;
import java.util.Map;

import com.codoid.products.exception.FilloException;

import cucumberUtilities.TestContext;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.MAClaimInitializationPage;


@SuppressWarnings("deprecation")
public class MAClaimInitializationPageSteps {
	
	MAClaimInitializationPage mAClaimInitializationPage;
	TestContext testContext;
	

	public MAClaimInitializationPageSteps(TestContext context) {
		testContext = context;
		mAClaimInitializationPage = testContext.getPageObjectManager().getMaClaimInitializationPage();
	}
    @Then("user click claim link on MA page")
    public void user_click_claim_link_on_MA_page() {
    	mAClaimInitializationPage.clickClaimLink();
    }

    @Then("user click Mass Claims Initialization link")
    public void user_click_Mass_Claims_Initialization_link() {
    	
    	mAClaimInitializationPage.clickMassClaimInitializationLink();
    	
    	
    }
    
   
    @Then("user select choose plan year")
    public void user_select_choose_plan_year(DataTable dt) throws FilloException {
    	
    	List<Map<String, String>> list = dt.asMaps();
		for(Map<String, String> form : list)
		{
			String TestCase = form.get("TestCase");
    	
    	mAClaimInitializationPage.selectChooseClaimPlanDropDown();
    	mAClaimInitializationPage.enterSearchByEmployeeIdTextBox(TestCase);
    	mAClaimInitializationPage.clickSearchByEmployeeIdButton();
    	mAClaimInitializationPage.clickClaimInitializtionSubmitButton();
    	mAClaimInitializationPage.clickClaimInitializtionOkButton();
    	mAClaimInitializationPage.clickClaimInitializtionCloseButton();
    	
    }
    }
    
    /*
    @Then("user enter employee id in text box")
    public void user_enter_employee_id_in_text_box() {
    	mAClaimInitializationPage.enterSearchByEmployeeIdTextBox();
    }

    @Then("user click on search employee for claim initialization")
    public void user_click_on_search_employee_for_claim_initialization() {
    	mAClaimInitializationPage.clickSearchByEmployeeIdButton();
    }

    @Then("user click on submit button for claim initialization")
    public void user_click_on_submit_button_for_claim_initialization() {
    	mAClaimInitializationPage.clickClaimInitializtionSubmitButton();
    }

    @Then("user click on ok button for claim initialization")
    public void user_click_on_ok_button_for_claim_initialization() {
    	mAClaimInitializationPage.clickClaimInitializtionOkButton();
    }

    @Then("verify successful claim initialization message is displayed")
    public void verify_successful_claim_initialization_message_is_displayed() {
    	mAClaimInitializationPage.verifyClaimMessageText();
    }

    @Then("user click on close button on popup screen")
    public void user_click_on_close_button_on_popup_screen() {
    	mAClaimInitializationPage.clickClaimInitializtionCloseButton();
    }
*/
}
