package stepDefinations;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import java.util.List;
import java.util.Map;
import com.codoid.products.exception.FilloException;
import cucumberUtilities.TestContext;
import io.cucumber.java.en.Then;
import pageObjects.MAClaimAdjudicateMedicalPage;

@SuppressWarnings("deprecation")
public class MAClaimAdjudicateMedicalPageSteps {

	MAClaimAdjudicateMedicalPage mAClaimAdjudicateMedicalPage;
	TestContext testContext;
	

	public MAClaimAdjudicateMedicalPageSteps(TestContext context) {
		testContext = context;
		mAClaimAdjudicateMedicalPage = testContext.getPageObjectManager().getEEClaimAdjudicateMedicalPage();
	}
	
	
	@When("user click search a claim link for Medical")
	public void user_click_search_a_claim_link_for_Medical() {
		mAClaimAdjudicateMedicalPage.clickSearchAClaimLink();
	}

	@When("user enter value for claiment name and process Adjudication for Medical")
	public void user_enter_value_for_claiment_name_and_process_Adjudication_for_Medical(DataTable dt) throws FilloException {
		
		List<Map<String, String>> list = dt.asMaps();
		for(Map<String, String> form : list)
		{
			String TestCase = form.get("TestCase");
			
			mAClaimAdjudicateMedicalPage.enterSearchAClaimTextBox(TestCase);
			mAClaimAdjudicateMedicalPage.clickSearchAClaimButton();
			mAClaimAdjudicateMedicalPage.clickClaimAdjudicateButton();
			mAClaimAdjudicateMedicalPage.selectClaimStatusSelectionDropdown(TestCase);
			mAClaimAdjudicateMedicalPage.clickClaimAdjudicateProcessButton();
			mAClaimAdjudicateMedicalPage.clickClaimCloseButton();
		
	}
	}

	
	
	
}
