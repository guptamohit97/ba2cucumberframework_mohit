package stepDefinations;

import cucumberUtilities.TestContext;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.MAClaimPaymentPage;

public class MAClaimPaymentPageSteps {

	MAClaimPaymentPage mAClaimPaymentPage;
	TestContext testContext;
	

	public MAClaimPaymentPageSteps(TestContext context) {
		testContext = context;
		mAClaimPaymentPage = testContext.getPageObjectManager().getMAClaimPaymentPage();
	}
	
	@When("user mose over payment link")
	public void user_mose_over_payment_link() {
		mAClaimPaymentPage.clickGernatePaymentFileLink();
	}

	@When("user enter salary deduction date")
	public void user_enter_salary_deduction_date() {
		mAClaimPaymentPage.clickFromTodayDate();
		mAClaimPaymentPage.clickToTodayDate();
	}

	@Then("user select generate button")
	public void user_select_generate_button() {
		mAClaimPaymentPage.clickGeneratePaymentButton();
	}

	@Then("user select close button on pop up screen")
	public void user_select_close_button_on_pop_up_screen() {
		mAClaimPaymentPage.clickGeneratePaymentPopUpCloseButton();
	}

	/*@Then("verify amount generated")
	public void verify_amount_generated() {
		mAClaimPaymentPage.verifyGeneratePaymentAmount();
	}
*/
	@Then("user select check box for payment")
	public void user_select_check_box_for_payment() {
		mAClaimPaymentPage.clickPaymentCheckbox();
	}

	@Then("user click confirm button")
	public void user_click_confirm_button() {
		mAClaimPaymentPage.clickConfirmPaymentButton();
	}

	@Then("user click ok button for payment")
	public void user_click_ok_button_for_payment() {
		mAClaimPaymentPage.clickPaymentOkButton();
	}

	@Then("verify payment confirmation is dislayed on popup screen")
	public void verify_payment_confirmation_is_dislayed_on_popup_screen() {
		mAClaimPaymentPage.verifyPaymentSucessfulMessage();
	}

	
}
