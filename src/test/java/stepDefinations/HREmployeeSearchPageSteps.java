package stepDefinations;

import cucumberUtilities.TestContext;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.HREmployeeSearchPage;

@SuppressWarnings("deprecation")
public class HREmployeeSearchPageSteps {

	HREmployeeSearchPage hREmployeeSearchPage;
	TestContext testContext;

	public HREmployeeSearchPageSteps(TestContext context) {
		testContext = context;
		hREmployeeSearchPage = testContext.getPageObjectManager().getHrEmployeeSearchPage();
	}

	@When("user click search employee tab")
	public void user_click_search_employee_tab() {
		hREmployeeSearchPage.clickHrSearchEmployeeTab();
	}

	@When("user select employee id radio button")
	public void user_select_employee_id_radio_button() {
		hREmployeeSearchPage.clickHrEmployeeIdRadioButton();
	}

	@When("user enter employee_id in search text box")
	public void user_enter_employee_id_in_search_text_box() {
		hREmployeeSearchPage.enterHrEmployeeTextBox();
	}

	@When("user click search button to search employee")
	public void user_click_search_button_to_search_employee() {
		hREmployeeSearchPage.clickHrEmployeeSearchButton();
	}

	@When("user click employee link")
	public void user_click_employee_link() {
		hREmployeeSearchPage.clickHrEmployeeIdLink();
	}

	@Then("verify header for Employee Detail page")
	public void verify_header_for_Employee_Detail_page() {
		hREmployeeSearchPage.verifyHrEmployeeSearchHeader();
	}

	@When("user click dependent link")
	public void user_click_dependent_link() {
		hREmployeeSearchPage.clickHrDependentSearchLink();
	}

	@Then("verify verify header for Dependent detail page")
	public void verify_verify_header_for_Dependent_detail_page() {
		hREmployeeSearchPage.verifyHrDependentSearchHeader();
	}

	@When("user click stataement of account link")
	public void user_click_stataement_of_account_link() {
		hREmployeeSearchPage.clickHrSoaSearchLink();
	}

	@Then("verify header for stetement of account page")
	public void verify_header_for_stetement_of_account_page() {
		hREmployeeSearchPage.verifyHrSoaHeader();
	}
}