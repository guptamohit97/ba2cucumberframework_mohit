package stepDefinations;

import cucumberUtilities.TestContext;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.MADependentProfileReportPage;
import pageObjects.MAPanelClaimListingReportPage;

public class MAPanelClaimListingReportSteps {

	MAPanelClaimListingReportPage mAPanelClaimListingReportPage;
	TestContext testContext;

	public MAPanelClaimListingReportSteps(TestContext context) {
		testContext = context;
		mAPanelClaimListingReportPage = testContext.getPageObjectManager().getMaPanelClaimListingReportPage();
	}

	@When("user click on the Panel Claim Listing Report under Report Stat")
	public void user_click_on_the_Panel_Claim_Listing_Report_under_Report_Stat() {
		mAPanelClaimListingReportPage.clickPanelClaimListingReport();
	}

	@When("user click Generate button")
	public void user_click_Generate_button() {
		mAPanelClaimListingReportPage.clickGenerateButton();
	}

	@Then("verify report header as Panel Claim Listing Report")
	public void verify_report_header_as_Panel_Claim_Listing_Report() {
		mAPanelClaimListingReportPage.validatePanelClaimListingReportName();
	}

}
