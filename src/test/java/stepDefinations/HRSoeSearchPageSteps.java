package stepDefinations;

import cucumberUtilities.TestContext;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.HRSoeSearchPage;

//@SuppressWarnings("deprecation")
public class HRSoeSearchPageSteps {

	HRSoeSearchPage hRSoeSearchPage;
	TestContext testContext;

	public HRSoeSearchPageSteps(TestContext context) {
		testContext = context;
		hRSoeSearchPage = testContext.getPageObjectManager().getHrSoeSearchPage();

	}

	@When("user click start enrollment tab")
	public void user_click_start_enrollment_tab() {
		hRSoeSearchPage.clickHrEnrolmentTab();
	}

	@When("user enter employee_id in enrolment search text box")
	public void user_enter_employee_id_in_enrolment_search_text_box() {
		hRSoeSearchPage.clickHrEmployeeEnrolmentTextBox();
	}

	@When("user click search button to search SOE")
	public void user_click_search_button_to_search_SOE() {
		hRSoeSearchPage.clickHrEnrolmentSearchButton();
	}

	@Then("verify employees SOE header as employee")
	public void verify_employees_SOE_header_as_employee() {
		hRSoeSearchPage.verifyHrEnrolmentHeader();
		// hRSoeSearchPage.clickHrStatementOfEnrolmentLink();
		// hRSoeSearchPage.verifyHrStatementOfEnrolmentHeader();
	}
}