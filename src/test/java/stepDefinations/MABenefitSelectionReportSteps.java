package stepDefinations;

import cucumberUtilities.TestContext;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.MABenefitSelectionReportPage;
import pageObjects.MAFlexPointsAllocationReportPage;

public class MABenefitSelectionReportSteps {

	MABenefitSelectionReportPage mABenefitSelectionReportPage;
	TestContext testContext;

	public MABenefitSelectionReportSteps(TestContext context) {
		testContext = context;
		mABenefitSelectionReportPage = testContext.getPageObjectManager().getMaBenefitSelectionReportPage();
	}

	@When("user click on the Benefit Selection Report under Report Stat")
	public void user_click_on_the_Benefit_Selection_Report_under_Report_Stat() {
		mABenefitSelectionReportPage.clickBenefitSelectionReport();
	}

	@When("user click Benefit Selection search button")
	public void user_click_Benefit_Selection_search_button() {
		mABenefitSelectionReportPage.clickBenefitSelectionSearchButton();
	}

	@Then("verify report header as Benefit Selection Report")
	public void verify_report_header_as_Benefit_Selection_Report() {
		mABenefitSelectionReportPage.validateBenefitSelectionReportName();
	}

}
