package stepDefinations;


import cucumberUtilities.TestContext;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.HRClaimSearchPage;

@SuppressWarnings("deprecation")
public class HRClaimSearchPageSteps {

	HRClaimSearchPage hRClaimSearchPage;
	TestContext testContext;

	public HRClaimSearchPageSteps(TestContext context) {
		testContext = context;
		hRClaimSearchPage = testContext.getPageObjectManager().getHrClaimSearchPage();
	}

	@When("user click claims tab")
	public void user_click_claims_tab() {
		hRClaimSearchPage.clickHrClaimTab();
	}

	@When("user enter employee_id in claim search text box")
	public void user_enter_employee_id_in_claim_search_text_box() {
		hRClaimSearchPage.selectHrClaimStatus();
	}

	@When("user click on search button on search claim page")
	public void user_click_on_search_button_on_search_claim_page() {
		hRClaimSearchPage.clickHrClaimSearchButton();
	}

	@Then("verify claim tables header is displayed as Transaction Code")
	public void verify_claim_tables_header_is_displayed_as_Transaction_Code() {
		hRClaimSearchPage.verifyHrClaimTransactionCodeHeader();
	}

}
