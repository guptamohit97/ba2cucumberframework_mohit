package stepDefinations;

import cucumberUtilities.TestContext;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.MADependentProfileReportPage;
import pageObjects.MAFlexPointsAllocationReportPage;

public class MAFlexPointDetailedReportSteps {

	MAFlexPointsAllocationReportPage mAFlexPointsAllocationReportPage;
	TestContext testContext;

	public MAFlexPointDetailedReportSteps(TestContext context) {
		testContext = context;
		mAFlexPointsAllocationReportPage = testContext.getPageObjectManager().getMaFlexPointsAllocationReportPage();
	}

	@When("user click on the Flex Point Detailed Report under Report Stat DSOSG")
	public void user_click_on_the_Flex_Point_Detailed_Report_under_Report_Stat_DSOSG() {
		mAFlexPointsAllocationReportPage.clickFlexPointReport();
	}

	@When("user click flex point search button")
	public void user_click_flex_point_search_button() {
		mAFlexPointsAllocationReportPage.clickFlexPointSearchButton();
	}

	@Then("verify report header as Flex Point Allocation Report")
	public void verify_report_header_as_Flex_Point_Allocation_Report() {
		mAFlexPointsAllocationReportPage.validateFlexPointReportName();
	}

}
