package stepDefinations;

import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import cucumberUtilities.TestContext;
import cucumbersManagers.PageObjectManager;
import cucumbersManagers.WebDriverManager;
import io.cucumber.core.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;

public class Hooks {

	TestContext testContext;

	public Hooks(TestContext context) {
		testContext = context;
	}

	@Before("@RegressionTest")
	public void beforeScenario(Scenario scenario) {
		testContext.webDriverManager = new WebDriverManager();
		testContext.pageObjectManager = new PageObjectManager(testContext.webDriverManager.getDriver(scenario));
		System.out.println("BA2 Application Automation");
		System.out.println("Scenario name: " + scenario.getName());

	}

	@After
	public void afterScenario() throws InterruptedException {
		testContext.getWebDriverManager().closeDriver();
	}

	@AfterStep
	public void captureScreenshot(Scenario scenario) throws IOException {
		if (scenario.isFailed()) {
			// String screenshotName = scenario.getName().replaceAll(" ", "_");
			// This takes a screenshot from the driver at save it to the specified location
			byte[] sourcePath = ((TakesScreenshot) testContext.getWebDriverManager().getDriver(scenario))
					.getScreenshotAs(OutputType.BYTES);
			scenario.embed(sourcePath, "image/png");

		}
	}

}
