package stepDefinations;

import java.util.List;
import java.util.Map;

import com.codoid.products.exception.FilloException;

import cucumberUtilities.TestContext;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.MAAddNewFamilyBatchPage;

public class MAAddNewFamilyBatchPageSteps {

	MAAddNewFamilyBatchPage mAAddNewFamilyBatchPage;
	TestContext testContext;
	

	public MAAddNewFamilyBatchPageSteps(TestContext context) {
		testContext = context;
		mAAddNewFamilyBatchPage = testContext.getPageObjectManager().getMaAddNewFamilyBatchPage();
	}
	
	@When("user add claim to batch for family claims")
	public void user_add_claim_to_batch_for_family_claims(DataTable dt) throws FilloException {
		
		List<Map<String, String>> list = dt.asMaps();
		for(Map<String, String> form : list)
		{
			String TestCase = form.get("TestCase");
			mAAddNewFamilyBatchPage.clickClaimLink();
			mAAddNewFamilyBatchPage.clickSearchAClaimLink();
			mAAddNewFamilyBatchPage.enterSearchAClaimTextBox(TestCase);
			mAAddNewFamilyBatchPage.clickSearchAClaimButton();
			mAAddNewFamilyBatchPage.clickAddToBatchButton();
			mAAddNewFamilyBatchPage.clickAddBatchNewButton();
			mAAddNewFamilyBatchPage.enterNewBatchNameTextBox(TestCase);
			mAAddNewFamilyBatchPage.clickBatchSaveButton();
			mAAddNewFamilyBatchPage.clickAddDocumentToBatchLink();
			mAAddNewFamilyBatchPage.clickAddTrancationCodeToBatchButton();
			mAAddNewFamilyBatchPage.clickClaimLink();
	}
	}


	
	
	
}
