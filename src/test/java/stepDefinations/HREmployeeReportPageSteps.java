package stepDefinations;

import cucumberUtilities.TestContext;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.HREmployeeReportPage;
import pageObjects.LoginEEPage;
import pageObjects.HRPage;
//import pageObjects.HrReportPage;
import pageObjects.PortalConfigurationsPage;

@SuppressWarnings("deprecation")
public class HREmployeeReportPageSteps {

	HREmployeeReportPage hREmployeeReportPage;
	LoginEEPage loginEEPage;
	TestContext testContext;

	public HREmployeeReportPageSteps(TestContext context) {
		testContext = context;
		hREmployeeReportPage = testContext.getPageObjectManager().getHrEmployeeReportPage();
		loginEEPage = testContext.getPageObjectManager().getLoginEePage();
	}

	/*@Given("user login into EE application")
	public void user_login_into_EE_application() {
		loginEEPage.enterUserNameTextBox();
		loginEEPage.enterPasswordTextBox();
		loginEEPage.clickLoginButton();

	}*/

	@Given("user click enter button under admin section")
	public void user_click_enter_button_under_admin_section() {
		hREmployeeReportPage.clickHrAdminButton();

	}

	@When("user click reports tab")
	public void user_click_reports_tab() {
		hREmployeeReportPage.clickHrReportTab();
	}

	@When("user click employee profile report link")
	public void user_click_employee_profile_report_link() {
		hREmployeeReportPage.clickEmployeeProfileRepot();
	}

	@When("user click employee status as Active")
	public void user_click_employee_status_as_Active() {
		hREmployeeReportPage.selectEmployeeStatus();
	}

	@When("user click employee search button")
	public void user_click_employee_search_button() {
		hREmployeeReportPage.clickSearchEmployeeReportButton();
	}

	@Then("verify employee report header as Employee Profile Report")
	public void verify_employee_report_header_as_Employee_Profile_Report() {
		hREmployeeReportPage.verifyEmployeeReportHeader();
	}
}