package stepDefinations;

import cucumberUtilities.TestContext;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.LoginPage;

public class LoginPageSteps {

	LoginPage loginPage;
	TestContext testContext;

	public LoginPageSteps(TestContext context) {
		testContext = context;
		loginPage = testContext.getPageObjectManager().getLoginPage();
	}

	@Given("user login into the application")
	public void user_login_into_the_application() {
		System.out.println("User launch the browser");
		loginPage.enterEmailAddress();
		loginPage.clickNextButton();
	}

	@Then("user logout from the application")
	public void user_logout_from_the_application() {
		loginPage.clickLogOut();
	}

	/*
	 * @Given("user launched the browser for automation") public void
	 * user_launched_the_browser_for_automation() {
	 * System.out.println("User launch the browser"); }
	 * 
	 * @When("user enter the email address in the email address field") public void
	 * user_enter_the_email_address_in_the_email_address_field() {
	 * loginPage.enterEmailAddress(); }
	 * 
	 * @Then("user click on the next button") public void
	 * user_click_on_the_next_button() { loginPage.clickNextButton(); }
	 * 
	 * @Then("user click on the logout button") public void
	 * user_click_on_the_logout_button() { loginPage.clickLogOut(); }
	 */

}
