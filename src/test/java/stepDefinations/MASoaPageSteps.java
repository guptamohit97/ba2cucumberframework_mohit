package stepDefinations;

import java.util.List;
import java.util.Map;

import com.codoid.products.exception.FilloException;

import cucumberUtilities.TestContext;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.MASoaPage;

public class MASoaPageSteps {

	MASoaPage mASoaPage;
	TestContext testContext;
	

	public MASoaPageSteps(TestContext context) {
		testContext = context;
		mASoaPage = testContext.getPageObjectManager().getMASoaPage();
	}
	
	@When("user click on the Search Employee tab")
	public void user_click_on_the_Search_Employee_tab() {
		mASoaPage.clickSearchEmployeeLink();
	}

	
	@When("user enter the employee ID in the search box and verify SOA")
	public void user_enter_the_employee_ID_in_the_search_box_and_verify_SOA(DataTable dt) throws FilloException, InterruptedException {
		
		List<Map<String, String>> list = dt.asMaps();
		for (Map<String, String> form : list) 
		{
			String TestCase = form.get("TestCase");
		
		mASoaPage.enterSearchEmployeeIdTextBox(TestCase);
		mASoaPage.clickSearchEmployeeIdButton();
		mASoaPage.clickSearchEmployeeSoaButton();
		mASoaPage.clicksearchEmployeeSoaLink();
		mASoaPage.clickSoaSearchIconButton();
		Thread.sleep(5000);
		//mASoaPage.verifySoaBalancePoints(TestCase);
		mASoaPage.clickSearchEmployeeLink();
	}
	}
	/*
	@When("user select Employee ID radio button")
	public void user_select_Employee_ID_radio_button() {
		mASoaPage.clickSearchEmployeeIdButton();
	}

	@When("user click on search button")
	public void user_click_on_search_button() {
		mASoaPage.clickSearchEmployeeSoaButton();
	}

	@When("user click on the name hyperlink")
	public void user_click_on_the_name_hyperlink() {
		mASoaPage.clicksearchEmployeeSoaLink();
	}

	@When("user click on Statement of Account\\(claims) icon")
	public void user_click_on_Statement_of_Account_claims_icon() {
		mASoaPage.clickSoaSearchIconButton();
	}

	@Then("SOA page should be displayed with balance amount")
	public void soa_page_should_be_displayed_with_balance_amount() {
		mASoaPage.verifySoaBalancePoints();
	}*/


	
}
