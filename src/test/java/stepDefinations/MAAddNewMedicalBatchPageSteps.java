package stepDefinations;

import java.util.List;
import java.util.Map;

import com.codoid.products.exception.FilloException;

import cucumberUtilities.TestContext;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.MAAddNewMedicalBatchPage;

public class MAAddNewMedicalBatchPageSteps {

	MAAddNewMedicalBatchPage mAAddNewMedicalBatchPage;
	TestContext testContext;
	

	public MAAddNewMedicalBatchPageSteps(TestContext context) {
		testContext = context;
		mAAddNewMedicalBatchPage = testContext.getPageObjectManager().getMaAddNewMedicalBatchPage();
	}
	
	@When("user add claim to batch for medical claims")
	public void user_add_claim_to_batch_for_medical_claims(DataTable dt) throws FilloException {
		
		List<Map<String, String>> list = dt.asMaps();
		for(Map<String, String> form : list)
		{
			String TestCase = form.get("TestCase");
			mAAddNewMedicalBatchPage.clickClaimLink();
			mAAddNewMedicalBatchPage.clickSearchAClaimLink();
			mAAddNewMedicalBatchPage.enterSearchAClaimTextBox(TestCase);
			mAAddNewMedicalBatchPage.clickSearchAClaimButton();
			mAAddNewMedicalBatchPage.clickAddToBatchButton();
			mAAddNewMedicalBatchPage.clickAddBatchNewButton();
			mAAddNewMedicalBatchPage.enterNewBatchNameTextBox(TestCase);
			mAAddNewMedicalBatchPage.clickBatchSaveButton();
			mAAddNewMedicalBatchPage.clickAddDocumentToBatchLink();
			mAAddNewMedicalBatchPage.clickAddTrancationCodeToBatchButton();
			mAAddNewMedicalBatchPage.clickClaimLink();
	}
	}


	
	
	
}
