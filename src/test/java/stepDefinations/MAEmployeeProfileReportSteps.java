package stepDefinations;

import cucumberUtilities.TestContext;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.MAEmployeeProfileReportPage;

public class MAEmployeeProfileReportSteps {

	MAEmployeeProfileReportPage mAEmployeeProfileReportPage;
	TestContext testContext;

	public MAEmployeeProfileReportSteps(TestContext context) {
		testContext = context;
		mAEmployeeProfileReportPage = testContext.getPageObjectManager().getMaReportsPage();
	}

	// Employee profile Report

	@When("user click on the Employee profile report under Report Stat")
	public void user_click_on_the_Employee_profile_report_under_Report_Stat() {
		mAEmployeeProfileReportPage.clickEmployeeProfileReport();
	}

	@When("user click MA employee search button")
	public void user_click_MA_employee_search_button() {
		mAEmployeeProfileReportPage.clickEmployeeSearchButton();
	}

	@Then("verify report header as Employee Profile Report")
	public void verify_report_header_as_Employee_Profile_Report() {
		mAEmployeeProfileReportPage.validateEmployeeProfileReportName();
	}

}
