package stepDefinations;

import cucumberUtilities.TestContext;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.HRFlexReportPage;

@SuppressWarnings("deprecation")
public class HRFlexReportPageSteps {

	HRFlexReportPage hRFlexReportPage;
	TestContext testContext;

	public HRFlexReportPageSteps(TestContext context) {
		testContext = context;
		hRFlexReportPage = testContext.getPageObjectManager().getHrFlexReportPage();
	}

	@When("user click dsosg flex point detailed report link")
	public void user_click_dsosg_flex_point_detailed_report_link() {
		hRFlexReportPage.clickEmployeeFlexReport();

	}

	@When("user click employee flex status as Active")
	public void user_click_employee_flex_status_as_Active() {
		hRFlexReportPage.selectEmployeeFlexStatus();
	}

	@When("user click flex report search button")
	public void user_click_flex_report_search_button() {
		hRFlexReportPage.clickFlexReportSearchButton();

	}

	@Then("verify report header as Flex Balance & Usage Report")
	public void verify_report_header_as_Flex_Balance_Usage_Report() {
		hRFlexReportPage.verifyFlexReportHeader();
	}
}