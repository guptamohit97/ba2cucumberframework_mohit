package stepDefinations;


import java.util.List;
import java.util.Map;

import com.codoid.products.exception.FilloException;

import cucumberUtilities.TestContext;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import pageObjects.EESubmitClaimHealthScreeningPage;
import pageObjects.EESubmitClaimMedicalExpencesPage;

@SuppressWarnings("deprecation")
public class EESubmitMedicalExpensesClaimPageSteps {

	EESubmitClaimMedicalExpencesPage eESubmitClaimMedicalExpencesPage;
	TestContext testContext;

	public EESubmitMedicalExpensesClaimPageSteps(TestContext context) {
		testContext = context;
		eESubmitClaimMedicalExpencesPage = testContext.getPageObjectManager().getEeSubmitClaimMedicalExpencesPage();
	}

	/*@Given("user click enter button under employee section for medical claim")
	public void user_click_enter_button_under_employee_section_for_medical_claim() {
		eESubmitClaimMedicalExpencesPage.clickMedicalEeEmployeeButton();
	}*/

	@When("user select create new claim link for medical claim")
	public void user_select_create_new_claim_link_for_medical_claim() {
		eESubmitClaimMedicalExpencesPage.clickMedicalCreateClaimLink();

	}

	@When("user select claimant name dropdown on EE claim screen for medical claim")
	public void user_select_claimant_name_dropdown_on_EE_claim_screen_for_medical_claim(DataTable dt) throws FilloException, InterruptedException {
		
		List<Map<String, String>> list = dt.asMaps();
		for (Map<String, String> form : list) 
		{
			String TestCase = form.get("TestCase");
		
		eESubmitClaimMedicalExpencesPage.selectMedicalClaimantNameDropDown(TestCase);
		eESubmitClaimMedicalExpencesPage.selectMedicalClaimTypeDropdown(TestCase);
		eESubmitClaimMedicalExpencesPage.enterMedicalClinicProviderName(TestCase);
		eESubmitClaimMedicalExpencesPage.enterMedicalReceiptDate(TestCase);
		eESubmitClaimMedicalExpencesPage.enterMedicalReceiptNo(TestCase);
		eESubmitClaimMedicalExpencesPage.enterMedicalReceiptAmount(TestCase);
		eESubmitClaimMedicalExpencesPage.clickMedicalAddAttachment();
		eESubmitClaimMedicalExpencesPage.clickMedicalReadyToSubmitButton();
		eESubmitClaimMedicalExpencesPage.clickMedicalConfirmSubmitButton();
		eESubmitClaimMedicalExpencesPage.clickMedicalConfirmButton();
		Thread.sleep(10000);
		eESubmitClaimMedicalExpencesPage.clickCreateNewClaim();
	}
	}

	/*@When("user select claim type from dropdown list for medical claim")
	public void user_select_claim_type_from_dropdown_list_for_medical_claim() {
		eESubmitClaimMedicalExpencesPage.selectMedicalClaimTypeDropdown();
	}

	@When("user enter value for clinic provider name textbox for medical claim")
	public void user_enter_value_for_clinic_provider_name_textbox_for_medical_claim() {
		eESubmitClaimMedicalExpencesPage.enterMedicalClinicProviderName();
	}

	@When("user enter value for receipt date textbox for medical claim")
	public void user_enter_value_for_receipt_date_textbox_for_medical_claim() {
		eESubmitClaimMedicalExpencesPage.enterMedicalReceiptDate();
	}

	@When("user enter value for receipt no textbox for medical claim")
	public void user_enter_value_for_receipt_no_textbox_for_medical_claim() {
		eESubmitClaimMedicalExpencesPage.enterMedicalReceiptNo();
	}

	@When("user enter value for receipt amount textbox for medical claim")
	public void user_enter_value_for_receipt_amount_textbox_for_medical_claim() {
		eESubmitClaimMedicalExpencesPage.enterMedicalReceiptAmount();
	}
	

	@When("user upload document through attach scanned receipt link for medical claim")
	public void user_upload_document_through_attach_scanned_receipt_link_for_medical_claim() {

		eESubmitClaimMedicalExpencesPage.clickMedicalAddAttachment();
	}

	@When("user select ready to submit button for medical claim")
	public void user_select_ready_to_submit_button_for_medical_claim() {

		eESubmitClaimMedicalExpencesPage.clickMedicalReadyToSubmitButton();
	}

	@When("user select confirm claims button for medical claim")
	public void user_select_confirm_claims_button_for_medical_claim() {
		eESubmitClaimMedicalExpencesPage.clickMedicalConfirmSubmitButton();
	}

	@When("user select ok button for medical claim")
	public void user_select_ok_button_for_medical_claim() {
		eESubmitClaimMedicalExpencesPage.clickMedicalConfirmButton();
	}

	@Then("user click search claim button for medical claim")
	public void user_click_search_claim_button_for_medical_claim() {
		eESubmitClaimMedicalExpencesPage.clickMedicalSearchButton();
	}*/


}
