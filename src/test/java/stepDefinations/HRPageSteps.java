package stepDefinations;

import cucumberUtilities.TestContext;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.HRPage;
import pageObjects.ImplementationPage;

@SuppressWarnings("deprecation")
public class HRPageSteps {

	HRPage hRPage;
	ImplementationPage implementationPage;
	TestContext testContext;

	public HRPageSteps(TestContext context) {
		testContext = context;
		implementationPage = testContext.getPageObjectManager().getImplementationPage();
		hRPage = testContext.getPageObjectManager().getHrPage();
	}

	@When("user click on security access link and click on manage user link")
	public void user_click_on_security_access_link_and_click_on_manage_user_link() {
		hRPage.clickManageUserButton();
	}

	@And("user click on add client admin button")
	public void user_click_on_add_client_admin_button() {
		hRPage.clickAddClientAdminButton();
	}

	@And("user click on search for employee button")
	public void user_click_on_search_for_employee_button() {
		hRPage.clickSearchForEmployeeButton();
	}

	@And("user enter employee name and click on search button")
	public void user_enter_employee_name_and_click_on_search_button() {
		hRPage.enterEmployeeFullName();
		hRPage.clickSearchEnteredEmployeeButton();
	}

	@And("user click on employee name link")
	public void user_click_on_employee_name_link() {
		hRPage.clickSearchedEmployeeLinkButton();
	}

	@Then("user click on save button")
	public void user_click_on_save_button() {
		hRPage.clickFinalSavebutttonButton();
	}

}
