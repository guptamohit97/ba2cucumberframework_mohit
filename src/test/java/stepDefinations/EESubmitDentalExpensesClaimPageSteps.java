package stepDefinations;


import java.util.List;
import java.util.Map;

import com.codoid.products.exception.FilloException;

import cucumberUtilities.TestContext;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.EESubmitClaimDentalExpencesPage;

public class EESubmitDentalExpensesClaimPageSteps {

	EESubmitClaimDentalExpencesPage eESubmitClaimDentalExpencesPage;
	TestContext testContext;

	public EESubmitDentalExpensesClaimPageSteps(TestContext context) {
		testContext = context;
		eESubmitClaimDentalExpencesPage = testContext.getPageObjectManager().getEeSubmitClaimDentalExpencesPage();
	}

	/*@And("^user click enter button under employee section for dental claim$")
	public void user_click_enter_button_under_employee_section_for_dental_claim() {
		eESubmitClaimDentalExpencesPage.clickDentalEeEmployeeButton();
	}*/

	@When("user select create new claim link for dental claim")
	public void user_select_create_new_claim_link_for_dental_claim() {
		eESubmitClaimDentalExpencesPage.clickDentalCreateClaimLink();

	}

	@When("user select claimant name dropdown on EE claim screen for dental claim")
	public void user_select_claimant_name_dropdown_on_EE_claim_screen_for_dental_claim(DataTable dt) throws FilloException, InterruptedException {
		
		List<Map<String, String>> list = dt.asMaps();
		for(Map<String, String> form : list)
		{
			String TestCase = form.get("TestCase");
			eESubmitClaimDentalExpencesPage.selectDentalClaimantNameDropDown(TestCase);
			eESubmitClaimDentalExpencesPage.selectDentalClaimTypeDropdown(TestCase);
			eESubmitClaimDentalExpencesPage.enterDentalClinicProviderName(TestCase);
			eESubmitClaimDentalExpencesPage.enterDentalReceiptDate(TestCase);
			eESubmitClaimDentalExpencesPage.enterDentalReceiptNo(TestCase);
			eESubmitClaimDentalExpencesPage.enterDentalReceiptAmount(TestCase);
			eESubmitClaimDentalExpencesPage.clickDentalAddAttachment();
			eESubmitClaimDentalExpencesPage.clickDentalReadyToSubmitButton();
			eESubmitClaimDentalExpencesPage.clickDentalConfirmSubmitButton();
			eESubmitClaimDentalExpencesPage.clickDentalConfirmButton();
			Thread.sleep(10000);
		//	eESubmitClaimDentalExpencesPage.clickDentalCreateClaimLink();
			eESubmitClaimDentalExpencesPage.clickCreateNewClaim();
		}
	
		
	}

	/*@When("user select claim type from dropdown list for dental claim")
	public void user_select_claim_type_from_dropdown_list_for_dental_claim() {
		eESubmitClaimDentalExpencesPage.selectDentalClaimTypeDropdown();
	}

	@When("user enter value for clinic provider name textbox for dental claim")
	public void user_enter_value_for_clinic_provider_name_textbox_for_dental_claim() {
		eESubmitClaimDentalExpencesPage.enterDentalClinicProviderName();
	}

	@When("user enter value for receipt date textbox for dental claim")
	public void user_enter_value_for_receipt_date_textbox_for_dental_claim() {
		eESubmitClaimDentalExpencesPage.enterDentalReceiptDate();
	}

	@When("user enter value for receipt no textbox for dental claim")
	public void user_enter_value_for_receipt_no_textbox_for_dental_claim() {
		eESubmitClaimDentalExpencesPage.enterDentalReceiptNo();
	}

	@When("user enter value for receipt amount textbox for dental claim")
	public void user_enter_value_for_receipt_amount_textbox_for_dental_claim() {
		eESubmitClaimDentalExpencesPage.enterDentalReceiptAmount();
	}

	@When("user upload document through attach scanned receipt link for dental claim")
	public void user_upload_document_through_attach_scanned_receipt_link_for_dental_claim() {

		eESubmitClaimDentalExpencesPage.clickDentalAddAttachment();
	}

	@When("user select ready to submit button for dental claim")
	public void user_select_ready_to_submit_button_for_dental_claim() {

		eESubmitClaimDentalExpencesPage.clickDentalReadyToSubmitButton();
	}

	@When("user select confirm claims button for dental claim")
	public void user_select_confirm_claims_button_for_dental_claim() {
		eESubmitClaimDentalExpencesPage.clickDentalConfirmSubmitButton();
	}

	@When("user select ok button for dental claim")
	public void user_select_ok_button_for_dental_claim() {
		eESubmitClaimDentalExpencesPage.clickDentalConfirmButton();
	}

	@Then("user click search claim button for dental claim")
	public void user_click_search_claim_button_for_dental_claim() {
		eESubmitClaimDentalExpencesPage.clickDentalSearchButton();
	}*/
	
}
