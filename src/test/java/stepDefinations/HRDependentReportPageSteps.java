package stepDefinations;

import cucumberUtilities.TestContext;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.HRDependentReportPage;

@SuppressWarnings("deprecation")
public class HRDependentReportPageSteps {

	HRDependentReportPage hRDependentReportPage;
	TestContext testContext;

	public HRDependentReportPageSteps(TestContext context) {
		testContext = context;
		hRDependentReportPage = testContext.getPageObjectManager().getHrDependentReportPage();
	}

	@When("user click dependent profile report link")
	public void user_click_dependent_profile_report_link() {
		hRDependentReportPage.clickDependentProfileReport();
	}

	@When("user click dependent status as Active")
	public void user_click_dependent_status_as_Active() {
		hRDependentReportPage.selectDependentStatus();
	}

	@When("user click dependent search button")
	public void user_click_dependent_search_button() {
		hRDependentReportPage.clickSearchDependentReportButton();
	}

	@Then("verify  dependent report header as Dependent Profile Report")
	public void verify_dependent_report_header_as_Dependent_Profile_Report() {
		hRDependentReportPage.verifyDependentReportHeader();
	}
}