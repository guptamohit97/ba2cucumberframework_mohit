package stepDefinations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.codoid.products.exception.FilloException;

import cucumberUtilities.TestContext;
import cucumberUtilities.Xls_Reader;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import pageObjects.EnrolmentCompletePage;
import pageObjects.MADependentProfileReportPage;

public class EnrolmentCompletePageSteps {

	EnrolmentCompletePage enrolmentCompletePage;
	TestContext testContext;

	public EnrolmentCompletePageSteps(TestContext context) {
		testContext = context;
		enrolmentCompletePage = testContext.getPageObjectManager().getEnrolmentCompletePage();
	}

	@Then("user naviagte to View Enrollment Status page")
	public void user_naviagte_to_View_Enrollment_Status_page() {
		enrolmentCompletePage.clickViewEnrollmentStatusButton();
	}
	


	@And("user close the enrolment for the employee")
	public void user_close_the_enrolment_for_the_employee(DataTable dt) throws InterruptedException, FilloException {
		
		List<Map<String, String>> list = dt.asMaps();
		for(Map<String, String> form : list)
		{
			String TestCase = form.get("TestCase");
			enrolmentCompletePage.selectEmployeeIDRadioClose();
			enrolmentCompletePage.enterEmployeeIDTextbox(TestCase);
			enrolmentCompletePage.clickSearchButton();
			enrolmentCompletePage.clickEditPeriodButton();
			enrolmentCompletePage.enterEnrolmentStartDate();
			enrolmentCompletePage.enterEnrolmentEndDate();
			enrolmentCompletePage.enterEnrolmentCloseDate();
			enrolmentCompletePage.enterEnrolmentCompleteDate();
			enrolmentCompletePage.clickSubmitButton();
		}
		
		
		enrolmentCompletePage.clickCompleteEnrollmentButton();
		enrolmentCompletePage.selectEmployeeIDRadioComplete();
		enrolmentCompletePage.enterEmployeeIDTextboxComplete();
		enrolmentCompletePage.clickSearchButton();
		enrolmentCompletePage.clickBatchCompleteAll();
		enrolmentCompletePage.clickScheduleJob_btnOk();
		enrolmentCompletePage.clickJobProgress1_btnClose();
		

	}
	
	

}
