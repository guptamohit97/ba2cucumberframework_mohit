package stepDefinations;

import cucumberUtilities.TestContext;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.MAClaimReviewCommonPage;

public class MAClaimReviewCommonPageSteps {

	MAClaimReviewCommonPage mAClaimReviewCommonPage;
	TestContext testContext;
	

	public MAClaimReviewCommonPageSteps(TestContext context) {
		testContext = context;
		mAClaimReviewCommonPage = testContext.getPageObjectManager().getMAClaimReviewCommonPage();
	}
	
	@When("user click claims link")
	public void user_click_claims_link() {
		mAClaimReviewCommonPage.clickMaClaimLink();
	}

	@When("user click on Mass Review Claims and Exception Processing link")
	public void user_click_on_Mass_Review_Claims_and_Exception_Processing_link() {
		mAClaimReviewCommonPage.clickMassReviewClaimsLink();
	}

	@When("user click review all button")
	public void user_click_review_all_button() throws InterruptedException {
		mAClaimReviewCommonPage.clickClaimReviewAllButton();
		Thread.sleep(5000);
	}

	/*@When("user click on OK button on review all screen pop up")
	public void user_click_on_OK_button_on_review_all_screen_pop_up() {
		mAClaimReviewCommonPage.();
	}
*/
	@When("user click review all link")
	public void user_click_review_all_link() {
		mAClaimReviewCommonPage.clickClaimReviewAllLink();
	}

	/*@When("user click on ok button reviewed pop up")
	public void user_click_on_ok_button_reviewed_pop_up() {
		mAClaimReviewCommonPage.();
	}*/

	@When("user click on schedular close button")
	public void user_click_on_schedular_close_button() {
		mAClaimReviewCommonPage.clickCliamSchedularCloseButton();
	}

	/*@Then("verify mass review completed successfully is displayed on screen")
	public void verify_mass_review_completed_successfully_is_displayed_on_screen() {
		mAClaimReviewCommonPage.verifyReviewClaimSuccessfulMessage();
	}*/


	
	
}
