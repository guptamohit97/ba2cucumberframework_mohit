package stepDefinations;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFCell;

import com.codoid.products.exception.FilloException;

import cucumberUtilities.ExcelData;
import cucumberUtilities.TestContext;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.EESubmitClaimChildCarePage;

@SuppressWarnings("deprecation")
public class EESubmitChildcareServicesClaimPageSteps {

	EESubmitClaimChildCarePage eeSubmitClaimCommonPage;
	TestContext testContext;

	public EESubmitChildcareServicesClaimPageSteps(TestContext context) {
		testContext = context;
		eeSubmitClaimCommonPage = testContext.getPageObjectManager().getEeSubmitClaimCommonPage();
	}

	/*@Given("user click enter button under employee section for childcare claim")
	public void user_click_enter_button_under_employee_section_for_childcare_claim() {
		eeSubmitClaimCommonPage.clickChildcareEeEmployeeButton();
	}*/

	@When("user select create new claim link for childcare claim")
	public void user_select_create_new_claim_link_for_childcare_claim() {
		eeSubmitClaimCommonPage.clickChildcareCreateClaimLink();
	}

	@When("user select claimant name dropdown on EE claim screen for childcare claim")
	public void user_select_claimant_name_dropdown_on_EE_claim_screen_for_childcare_claim(DataTable dt) throws FilloException, InterruptedException {
		// Write code here that turns the phrase above into concrete actions
		
		List<Map<String, String>> list = dt.asMaps();
		for (Map<String, String> form : list) 
		{
			String TestCase = form.get("TestCase");
			
		eeSubmitClaimCommonPage.selectChildcareClaimantNameDropDown(TestCase);
		eeSubmitClaimCommonPage.selectChildcareClaimTypeDropdown(TestCase);
		eeSubmitClaimCommonPage.enterChildcareClinicProviderName(TestCase);
		eeSubmitClaimCommonPage.enterChildcareReceiptDate(TestCase);
		eeSubmitClaimCommonPage.enterChildcareReceiptNo(TestCase);
		eeSubmitClaimCommonPage.enterChildcareReceiptAmount(TestCase);
		eeSubmitClaimCommonPage.clickChildcareAddAttachment();
		eeSubmitClaimCommonPage.clickChildcareReadyToSubmitButton();
		eeSubmitClaimCommonPage.clickChildcareConfirmSubmitButton();
		eeSubmitClaimCommonPage.clickChildcareConfirmButton();
		Thread.sleep(10000);
		eeSubmitClaimCommonPage.clickCreateNewClaim();
	}
	}

	/*@When("user select claim type from dropdown list for childcare claim")
	public void user_select_claim_type_from_dropdown_list_for_childcare_claim() {
		eeSubmitClaimCommonPage.selectChildcareClaimTypeDropdown();
	}

	@SuppressWarnings("unchecked")
	@When("user enter value for clinic provider name textbox for childcare claim")
	public void user_enter_value_for_clinic_provider_name_textbox_for_childcare_claim() throws IOException {
		
		
		eeSubmitClaimCommonPage.enterChildcareClinicProviderName();
		
		
ExcelData d = new ExcelData("C:\\Users\\mohit-gupta\\Documents\\Eclipse_projects\\BA2_Cucumber_Automation_Practice1\\Fixture\\TestData.xlsx");

		ArrayList data = d.getData("ChildCareClaim", "Clinic/Provider Name", "TC06_EE_Submit_Search_ClaimChildcareservices.feature");
		
		System.out.println( ((ArrayList<String>) data.get(1)).get(2));
		
	}

	@When("user enter value for receipt date textbox for childcare claim")
	public void user_enter_value_for_receipt_date_textbox_for_childcare_claim() throws IOException {
		
		ExcelData d = new ExcelData("C:\\Users\\mohit-gupta\\Documents\\Eclipse_projects\\BA2_Cucumber_Automation_Practice1\\Fixture\\TestData.xlsx");

		ArrayList<String> data = d.getData("ChildCareClaim", "Receipt Date (DD/MM/YYYY)", "TC06_EE_Submit_Search_ClaimChildcareservices");
		eeSubmitClaimCommonPage.enterChildcareReceiptDate();
		
		//System.out.println(data.get(1));
	}

	@When("user enter value for receipt no textbox for childcare claim")
	public void user_enter_value_for_receipt_no_textbox_for_childcare_claim() {
		eeSubmitClaimCommonPage.enterChildcareReceiptNo();
	}

	@When("user enter value for receipt amount textbox for childcare claim")
	public void user_enter_value_for_receipt_amount_textbox_for_childcare_claim() {
		eeSubmitClaimCommonPage.enterChildcareReceiptAmount();
	}

	@When("user upload document through attach scanned receipt link for childcare claim")
	public void user_upload_document_through_attach_scanned_receipt_link_for_childcare_claim() {

		eeSubmitClaimCommonPage.clickChildcareAddAttachment();
	}

	@When("user select ready to submit button for childcare claim")
	public void user_select_ready_to_submit_button_for_childcare_claim() {

		eeSubmitClaimCommonPage.clickChildcareReadyToSubmitButton();
	}

	@When("user select confirm claims button for childcare claim")
	public void user_select_confirm_claims_button_for_childcare_claim() {
		eeSubmitClaimCommonPage.clickChildcareConfirmSubmitButton();
	}

	@When("user select ok button for childcare claim")
	public void user_select_ok_button_for_childcare_claim() {
		eeSubmitClaimCommonPage.clickChildcareConfirmButton();
	}

	@Then("user click search claim button for childcare claim")
	public void user_click_search_claim_button_for_childcare_claim() {
		eeSubmitClaimCommonPage.clickChildcareSearchButton();
	}
*/
}
