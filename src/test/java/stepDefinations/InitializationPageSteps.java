package stepDefinations;

import cucumberUtilities.TestContext;
import io.cucumber.java.en.Then;
import pageObjects.ImplementationPage;
import pageObjects.InitializationPage;

public class InitializationPageSteps {

	InitializationPage initializationPage;
	ImplementationPage implementationPage;
	TestContext testContext;

	public InitializationPageSteps(TestContext context) {
		testContext = context;
		initializationPage = testContext.getPageObjectManager().getInitializationPage();
		implementationPage = testContext.getPageObjectManager().getImplementationPage();
	}

	@Then("user intilized the uploaded employee")
	public void user_intilized_the_uploaded_employee() {
		implementationPage.clickInitializationButton();
		initializationPage.enterEmrolmentStartDate();
		initializationPage.enterEnrolmentEndDate();
		initializationPage.enterEmployeeName();
		initializationPage.searchEmployeeButton();
		initializationPage.enterEmployeeNameInSearchField();
		initializationPage.clickEmployeeRadioButton();
		initializationPage.clickSubmitButton();
		initializationPage.selectNoOFRecords();
		initializationPage.clickSubmitButtonForInitialization();
		initializationPage.clickScheduleJob_btnOk();
		initializationPage.clickJobProgress1_btnClose();

	}

	/*
	 * @Then("user enter enrolment from date in the field") public void
	 * user_enter_enrolment_from_date_in_the_field() {
	 * initializationPage.enterEmrolmentStartDate(); }
	 * 
	 * @Then("user enter enrolment to date in the field") public void
	 * user_enter_enrolment_to_date_in_the_field() {
	 * initializationPage.enterEnrolmentEndDate(); }
	 * 
	 * @Then("user enter the username in the employee field") public void
	 * user_enter_the_username_in_the_employee_field() {
	 * initializationPage.enterEmployeeName(); }
	 * 
	 * @Then("user click on the search empolyee button") public void
	 * user_click_on_the_search_empolyee_button() {
	 * initializationPage.searchEmployeeButton(); }
	 * 
	 * @Then("user enter the employee name in the search field") public void
	 * user_enter_the_employee_name_in_the_search_field() {
	 * initializationPage.enterEmployeeNameInSearchField(); }
	 * 
	 * @Then("user select employee radio button") public void
	 * user_select_employee_radio_button() {
	 * initializationPage.clickEmployeeRadioButton(); }
	 * 
	 * @Then("user click on the search button") public void
	 * user_click_on_the_search_button() { initializationPage.clickSubmitButton(); }
	 * 
	 * @Then("user select no of records from the dropdown") public void
	 * user_select_no_of_records_from_the_dropdown() {
	 * initializationPage.selectNoOFRecords(); }
	 * 
	 * @Then("user click on the intilization submit button") public void
	 * user_click_on_the_intilization_submit_button() {
	 * initializationPage.clickSubmitButtonForInitialization(); }
	 * 
	 * @Then("click on the ok button") public void click_on_the_ok_button() {
	 * initializationPage.clickScheduleJob_btnOk(); }
	 * 
	 * @Then("user click on the close button of job progress pop up") public void
	 * user_click_on_the_close_button_of_job_progress_pop_up() {
	 * initializationPage.clickJobProgress1_btnClose(); }
	 */

}
