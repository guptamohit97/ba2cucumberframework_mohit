package stepDefinations;


import java.util.List;
import java.util.Map;

import com.codoid.products.exception.FilloException;

import cucumberUtilities.TestContext;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.EESubmitClaimFamilyCarePage;
import pageObjects.EESubmitClaimHealthScreeningPage;
import pageObjects.EESubmitClaimMedicalExpencesPage;

@SuppressWarnings("deprecation")
public class EESubmitFamilyCareWellBeingClaimPageSteps {

	EESubmitClaimFamilyCarePage eESubmitClaimFamilyCarePage;
	TestContext testContext;

	public EESubmitFamilyCareWellBeingClaimPageSteps(TestContext context) {
		testContext = context;
		eESubmitClaimFamilyCarePage = testContext.getPageObjectManager().getEeSubmitClaimFamilyCarePage();
	}

	/*@Given("user click enter button under employee section for familycare claim")
	public void user_click_enter_button_under_employee_section_for_familycare_claim() {
		eESubmitClaimFamilyCarePage.clickFamilycareEeEmployeeButton();
	}*/

	@When("user select create new claim link for familycare claim")
	public void user_select_create_new_claim_link_for_familycare_claim() {
		eESubmitClaimFamilyCarePage.clickFamilycareCreateClaimLink();

	}

	@When("user select claimant name dropdown on EE claim screen for familycare claim")
	public void user_select_claimant_name_dropdown_on_EE_claim_screen_for_familycare_claim(DataTable dt) throws InterruptedException, FilloException {
		// Write code here that turns the phrase above into concrete actions
		
		List<Map<String, String>> list = dt.asMaps();
		for (Map<String, String> form : list) 
		{
			String TestCase = form.get("TestCase");
			
		eESubmitClaimFamilyCarePage.selectFamilycareClaimantNameDropDown(TestCase);
		eESubmitClaimFamilyCarePage.selectFamilycareClaimTypeDropdown(TestCase);
		eESubmitClaimFamilyCarePage.enterFamilycareClinicProviderName(TestCase);
		eESubmitClaimFamilyCarePage.enterFamilycareReceiptDate(TestCase);
		eESubmitClaimFamilyCarePage.enterFamilycareReceiptNo(TestCase);
		eESubmitClaimFamilyCarePage.enterFamilycareReceiptAmount(TestCase);
		eESubmitClaimFamilyCarePage.clickFamilycareAddAttachment();
		eESubmitClaimFamilyCarePage.clickFamilycareReadyToSubmitButton();
		eESubmitClaimFamilyCarePage.clickFamilycareConfirmSubmitButton();
		eESubmitClaimFamilyCarePage.clickFamilycareConfirmButton();
		Thread.sleep(10000);
		eESubmitClaimFamilyCarePage.clickCreateNewClaim();
	}
	}

	/*@When("user select claim type from dropdown list for familycare claim")
	public void user_select_claim_type_from_dropdown_list_for_familycare_claim() {
		eESubmitClaimFamilyCarePage.selectFamilycareClaimTypeDropdown();
	}

	@When("user enter value for clinic provider name textbox for familycare claim")
	public void user_enter_value_for_clinic_provider_name_textbox_for_familycare_claim() {
		eESubmitClaimFamilyCarePage.enterFamilycareClinicProviderName();
	}

	@When("user enter value for receipt date textbox for familycare claim")
	public void user_enter_value_for_receipt_date_textbox_for_familycare_claim() {
		eESubmitClaimFamilyCarePage.enterFamilycareReceiptDate();
	}

	@When("user enter value for receipt no textbox for familycare claim")
	public void user_enter_value_for_receipt_no_textbox_for_familycare_claim() {
		eESubmitClaimFamilyCarePage.enterFamilycareReceiptNo();
	}

	@When("user enter value for receipt amount textbox for familycare claim")
	public void user_enter_value_for_receipt_amount_textbox_for_familycare_claim() {
		eESubmitClaimFamilyCarePage.enterFamilycareReceiptAmount();
	}


	@When("user upload document through attach scanned receipt link for familycare claim")
	public void user_upload_document_through_attach_scanned_receipt_link_for_familycare_claim() {

		eESubmitClaimFamilyCarePage.clickFamilycareAddAttachment();
	}

	@When("user select ready to submit button for familycare claim")
	public void user_select_ready_to_submit_button_for_familycare_claim() {

		eESubmitClaimFamilyCarePage.clickFamilycareReadyToSubmitButton();
	}

	@When("user select confirm claims button for familycare claim")
	public void user_select_confirm_claims_button_for_familycare_claim() {
		eESubmitClaimFamilyCarePage.clickFamilycareConfirmSubmitButton();
	}

	@When("user select ok button for familycare claim")
	public void user_select_ok_button_for_familycare_claim() {
		eESubmitClaimFamilyCarePage.clickFamilycareConfirmButton();
	}

	@Then("user click search claim button for familycare claim")
	public void user_click_search_claim_button_for_familycare_claim() {
		eESubmitClaimFamilyCarePage.clickFamilycareSearchButton();
	}*/

}
